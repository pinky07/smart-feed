package com.click4design.core.ui;

import com.click4design.roboholo.RoboHoloListActivity;
import com.click4design.socialfeed.SocialFeedApplication;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.space.service.AggregatedFeedService;
import com.foxykeep.datadroid.requestmanager.RequestManager;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;

import javax.inject.Inject;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 16:32
 */
public class CommonListActivity extends RoboHoloListActivity {
    @Inject
    protected CommonRequestManager requestManager;

    @Override
    protected void onResume() {
        super.onResume();
        ActivityManager.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityManager.onDestroy(this);
    }

	protected SocialFeedApplication app() {
		return SocialFeedApplication.getInstance();
	}
}
