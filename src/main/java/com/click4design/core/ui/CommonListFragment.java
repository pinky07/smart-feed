package com.click4design.core.ui;

import com.click4design.roboholo.RoboHoloListFragment;
import com.click4design.socialfeed.SocialFeedApplication;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.foxykeep.datadroid.requestmanager.RequestManager;

import javax.inject.Inject;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 18:33
 */
public class CommonListFragment extends RoboHoloListFragment {
    @Inject
    protected CommonRequestManager requestManager;

	public SocialFeedApplication app() {
		return SocialFeedApplication.getInstance();
	}
}
