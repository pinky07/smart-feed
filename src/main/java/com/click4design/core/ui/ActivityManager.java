package com.click4design.core.ui;

import org.holoeverywhere.app.Activity;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 20:39
 */
public class ActivityManager {
    private static Activity mTopActivity;

    public static Activity getTopActivity() {
        return mTopActivity;
    }

    /**
     * Call it in onResume for of activities where you using VK SDK
     * @param activity Your activity
     */
    public static void onResume(Activity activity) {
        if (mTopActivity != activity)
            mTopActivity = activity;
    }
    /**
     * Call it in onDestroy for of activities where you using VK SDK
     * @param activity Your activity
     */
    public static void onDestroy(Activity activity) {
        if (mTopActivity == activity)
            mTopActivity = null;
    }
}
