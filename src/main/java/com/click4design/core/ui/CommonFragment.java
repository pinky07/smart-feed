package com.click4design.core.ui;

import com.click4design.roboholo.RoboHoloFragment;
import com.click4design.socialfeed.SocialFeedApplication;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.foxykeep.datadroid.requestmanager.RequestManager;

import javax.inject.Inject;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 16:35
 */
public class CommonFragment extends RoboHoloFragment {
    @Inject
    protected CommonRequestManager requestManager;

	protected SocialFeedApplication app() {
		return SocialFeedApplication.getInstance();
	}
}
