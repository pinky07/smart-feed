package com.click4design.core.ui;

import com.click4design.roboholo.RoboHoloDialogFragment;
import com.click4design.socialfeed.SocialFeedApplication;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.space.service.AggregatedFeedService;
import com.foxykeep.datadroid.requestmanager.RequestManager;
import com.octo.android.robospice.SpiceManager;

import javax.inject.Inject;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 16:36
 */
public class CommonDialogFragment extends RoboHoloDialogFragment {
    @Inject
    protected CommonRequestManager requestManager;

	protected SocialFeedApplication app() {
		return SocialFeedApplication.getInstance();
	}
}
