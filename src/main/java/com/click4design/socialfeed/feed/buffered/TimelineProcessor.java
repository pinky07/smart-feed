package com.click4design.socialfeed.feed.buffered;

import com.click4design.socialfeed.data.model.Feed;
import com.click4design.socialfeed.feed.FeedItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 08.03.14
 * Time: 18:01
 */
public class TimelineProcessor<T extends FeedItem<T>> implements BufferedFeedProcessor<T> {
	private List<T> mPostList = new ArrayList<T>();
	private boolean isBeginning = true;

	@Override
	public List<T> process(Feed<T> feed, BufferedFeedLoader.LoaderState<T> state) {
		List<T> dispatchItems = new ArrayList<T>();
		mPostList.addAll(feed.getItems());
		mPostList.addAll(state.getRemainingItems());
		Collections.sort(mPostList);

		if(!feed.isEmpty()) {
			T currentLastPost = state.getLastItem();
			if(currentLastPost != null) {
				final int lastPostIndex = mPostList.indexOf(currentLastPost);
				final int postListCount = mPostList.size();

				if(postListCount -1 > lastPostIndex) {
					List<T> remainingList 	= new LinkedList<T>(mPostList.subList(lastPostIndex+1, postListCount));
					state.setRemainingItems(remainingList);
					mPostList.removeAll(remainingList);
				}
			}

			if(!state.isBeginning()) {
				T firstUploadedItem = feed.getFirstItem();
				if(mPostList.contains(firstUploadedItem)) {
					dispatchItems.addAll(new ArrayList<T>(mPostList.subList(
							mPostList.indexOf(firstUploadedItem),
							mPostList.size()))
					);
				}
			}
		}

		return null;
	}
}
