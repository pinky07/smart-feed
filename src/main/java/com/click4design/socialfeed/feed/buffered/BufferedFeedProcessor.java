package com.click4design.socialfeed.feed.buffered;

import com.click4design.socialfeed.data.model.Feed;
import com.click4design.socialfeed.feed.FeedItem;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 08.03.14
 * Time: 17:47
 */
public interface BufferedFeedProcessor<T extends FeedItem<T>> {
	public List<T> process(Feed<T> feed, BufferedFeedLoader.LoaderState<T> state);
}
