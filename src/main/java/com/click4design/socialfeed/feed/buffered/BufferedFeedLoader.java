package com.click4design.socialfeed.feed.buffered;

import android.content.Context;
import android.os.Bundle;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.CommonResponseHandler;
import com.click4design.socialfeed.data.IRequestListener;
import com.click4design.socialfeed.data.RequestFactory;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Feed;
import com.click4design.socialfeed.data.model.Pagination;
import com.click4design.socialfeed.data.service.BatchRequest;
import com.click4design.socialfeed.feed.FeedItem;
import com.click4design.socialfeed.feed.FeedLoader;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.foxykeep.datadroid.requestmanager.Request;

import java.util.*;

/**
 * Author: Khamidullin Kamil
 * Date: 23.02.14
 * Time: 1:09
 */
public class BufferedFeedLoader<T extends FeedItem<T>> implements FeedLoader<T>,  IRequestListener {
	private List<SocialNetworkProviderInterface> providers;
	private List<T> mPostList = new ArrayList<T>();

	private LoaderState<T> state;

	private CommonRequestManager mRequestManager;

	private CommonResponseHandler mRequestHandler;
	private FeedLoaderListener<T> mFeedLoaderListener;

	private FeedLoaderMode mode;

	/**
	 * Количество загружаемых за раз постов
	 */
	final static int LOAD_POST_COUNT = 20;

	public BufferedFeedLoader(
			Context context,
		  	CommonRequestManager requestManager,
			List<SocialNetworkProviderInterface> providers)  {
		mRequestManager = requestManager;
		mRequestHandler =  new CommonResponseHandler(context, this) {
			private static final long serialVersionUID = -5405342106487764838L;
			@Override
			public void onConnectionError() {
				super.onConnectionError();
				if(mFeedLoaderListener != null) {
					mFeedLoaderListener.onConnectionError();
				}
			}
		};

		this.providers = providers;
		this.state = new LoaderState<T>(this.providers);
		this.setMode(FeedLoaderMode.HOME_TIMELINE);
	}

	public void setMode(FeedLoaderMode mode) {
		this.mode = mode;
	}

	public void setFeedLoaderListener(FeedLoaderListener<T> listener) {
		mFeedLoaderListener = listener;
	}

	public void removeFeedLoaderListener() {
		mFeedLoaderListener = null;
	}

	/**
	 * Начинает загрузку ленты с начала
	 */
	public void start() {
		state.reset();

		if(!mPostList.isEmpty()) {
			mPostList.clear();
		}

		if(!providers.isEmpty()) {
			BatchRequest batchRequest = new BatchRequest();
			for (SocialNetworkProviderInterface provider : providers) {
				Bundle params = new Bundle();
				params.putInt("count", LOAD_POST_COUNT);
				batchRequest.add(RequestFactory.create(mode.getRequestCode(), provider, params));
			}

			mRequestManager.execute(batchRequest, mRequestHandler);
		}
	}

	/**
	 * Загружает следующую часть ленты
	 */
	public void next() {
		if (hasNext() && !state.isLoading()) {
			T lastItem = state.getLastItem();
			Bundle params = state.getNextPagingParams();
			if(params != null) {
				Request request = RequestFactory.create(mode.getRequestCode(), lastItem.getProvider(), params);
				mRequestManager.execute(request, mRequestHandler);
			}
		}
	}

	@Override
	final public void success(Request request, Bundle resultData) {
		Feed<T> feed = resultData.getParcelable("news");
		this.state.dispatchLoadSuccess(getProviderFromRequest(request), feed);

		processResult(feed);
	}

	private synchronized void processResult(Feed<T> feed) {
		List<T> dispatchItems = new ArrayList<T>();
		mPostList.addAll(feed.getItems());
		mPostList.addAll(state.getRemainingItems());
		Collections.sort(mPostList);

		if(!feed.isEmpty()) {
			T currentLastPost = state.getLastItem();
			if(currentLastPost != null) {
				final int lastPostIndex = mPostList.indexOf(currentLastPost);
				final int postListCount = mPostList.size();

				if(postListCount -1 > lastPostIndex) {
					List<T> remainingList 	= new LinkedList<T>(mPostList.subList(lastPostIndex+1, postListCount));
					state.setRemainingItems(remainingList);
					mPostList.removeAll(remainingList);
				}
			}

			if(!state.isBeginning()) {
				T firstUploadedItem = feed.getFirstItem();
				if(mPostList.contains(firstUploadedItem)) {
					dispatchItems.addAll(new ArrayList<T>(mPostList.subList(
							mPostList.indexOf(firstUploadedItem),
							mPostList.size()))
					);
				}
			}
		}

		if(state.isBeginning() && state.isAllProvidersLoaded()) {
			state.setBeginning(false);
			dispatchOnFeedItemsLoaded(mPostList);
		} else if(!state.isBeginning()) {
			dispatchOnFeedItemsLoaded(dispatchItems);
		}
	}

	@Override
	final public void error(CommonSocialNetworkException exception, Request request) {
		this.state.dispatchLoadError(getProviderFromRequest(request));
		this.dispatchOnError(exception);

		if(state.isBeginning() && state.isAllProvidersLoaded()) {
			state.setBeginning(false);
			dispatchOnFeedItemsLoaded(mPostList);
		}
	}

	@Override
	final public void start(Request request) {
		this.state.dispatchLoadStart(getProviderFromRequest(request));
	}

	@Override
	final public void finish(Request request) {}

	protected void dispatchOnFeedItemsLoaded(List<T> feedItems) {
		if(mFeedLoaderListener != null) {
			mFeedLoaderListener.onFeedItemsLoaded(feedItems);
		}
	}

	protected void dispatchOnError(CommonSocialNetworkException exception) {
		if(mFeedLoaderListener != null) {
			mFeedLoaderListener.onError(exception);
		}
	}

	@Override
	public boolean hasNext() {
		return state.hasNext();
	}

	private SocialNetworkProviderInterface getProviderFromRequest(Request request) {
		return (SocialNetworkProviderInterface)request.getParcelable("provider");
	}

	public static class LoaderState<T extends FeedItem<T>> {
		/**
		 * Коллекция с последним постом из каждоый соц сети
		 */
		final private Map<SocialNetworkProviderInterface, T> mLastPosts = new HashMap<SocialNetworkProviderInterface, T>();
		/**
		 * Cодержит параметры для загрузки следующей партии постов,
		 * для конкретной соц сети
		 */
		final private Map<SocialNetworkProviderInterface, Bundle> mNextPagingParams = new HashMap<SocialNetworkProviderInterface, Bundle>();

		/**
		 * Статус загрузки постов из соц сетей
		 */
		final private Map<SocialNetworkProviderInterface, Boolean> mLoadingStatus = new HashMap<SocialNetworkProviderInterface, Boolean>();

		private List<T> mRemainingPostList = new ArrayList<T>();

		final private List<SocialNetworkProviderInterface> mProviders;

		private boolean isBeginning = true;

		public LoaderState(List<SocialNetworkProviderInterface> providers) {
			this.mProviders = providers;
		}

		public synchronized void reset() {
			setBeginning(true);

			if(!mRemainingPostList.isEmpty()) {
				mRemainingPostList.clear();
			}

			if(!mLastPosts.isEmpty()) {
				mLastPosts.clear();
			}

			if(!mNextPagingParams.isEmpty()) {
				mNextPagingParams.clear();
			}

			if(!mLoadingStatus.isEmpty()) {
				mLoadingStatus.clear();
			}
		}

		private synchronized void setLoadingStatus(SocialNetworkProviderInterface provider, boolean value) {
			mLoadingStatus.put(provider, value);
		}

		public synchronized void dispatchLoadStart(SocialNetworkProviderInterface provider) {
			setLoadingStatus(provider, true);
		}

		public synchronized void dispatchLoadError(SocialNetworkProviderInterface provider) {
			setLoadingStatus(provider, false);
			mLastPosts.remove(provider);
		}

		public synchronized void dispatchLoadSuccess(SocialNetworkProviderInterface provider, Feed<T> feed) {
			setLoadingStatus(provider, false);

			Pagination pagination = feed.getPagination();
			if(pagination.hasNext() && !feed.isEmpty()) {
				mLastPosts.put(provider, feed.getLastItem());
				mNextPagingParams.put(provider, pagination.getParams());
			} else if(!pagination.hasNext()) {
				mLastPosts.remove(provider);
				mNextPagingParams.remove(provider);
			}
		}

		public List<T> getRemainingItems() {
			return this.mRemainingPostList;
		}

		public synchronized void setRemainingItems(List<T> items) {
			mRemainingPostList = items;
		}

		public T getLastItem() {
			if(!mLastPosts.isEmpty()) {
				return Collections.min(mLastPosts.values());
			} else {
				return null;
			}
		}

		public Bundle getNextPagingParamsFor(SocialNetworkProviderInterface provider) {
			if(mNextPagingParams.containsKey(provider)) {
				return mNextPagingParams.get(provider);
			} else {
				return null;
			}
		}

		public Bundle getNextPagingParams() {
			T item = getLastItem();
			if(item != null) {
				return getNextPagingParamsFor(item.getProvider());
			} else {
				return null;
			}
		}

		public boolean hasLoadingRequests() {
			return mLoadingStatus.values().contains(true);
		}

		public boolean isLoading() {
			T item = getLastItem();
			return item != null && isLoading(item.getProvider());
		}

		public boolean isLoading(SocialNetworkProviderInterface provider) {
			return mLoadingStatus.get(provider);
		}

		public boolean isAllProvidersLoaded() {
			return (mLoadingStatus.keySet().containsAll(mProviders) && !hasLoadingRequests());
		}

		public boolean isBeginning() {
			return this.isBeginning;
		}

		public void setBeginning(boolean value) {
			this.isBeginning = value;
		}

		boolean hasNext() {
			return (isAllProvidersLoaded() && !mLastPosts.isEmpty());
		}
	}
}
