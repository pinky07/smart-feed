package com.click4design.socialfeed.feed.comment;

import android.content.Context;
import android.os.Bundle;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.CommonResponseHandler;
import com.click4design.socialfeed.data.IRequestListener;
import com.click4design.socialfeed.data.RequestFactory;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Comment;
import com.click4design.socialfeed.data.model.Feed;
import com.click4design.socialfeed.data.model.Pagination;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.feed.FeedLoader;
import com.foxykeep.datadroid.requestmanager.Request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 10.03.14
 * Time: 12:57
 */
public class CommentLoader implements FeedLoader<Comment>, IRequestListener {
	private Context context;
	private CommonRequestManager requestManager;
	private FeedLoaderListener<Comment> listener;
	private Post post;
	private final List<Comment> comments = new ArrayList<Comment>();
	private int currentPosition = 0;
	private Pagination nextPaginationParams;
	private final static int NETWORK_LOAD_COUNT = 20;
	private final static int START_COUNT = 2;
	private final static int PART_COUNT = 10;
	private CommonResponseHandler responseHandler;
	private boolean isLoading = false;

	public CommentLoader(Context context, CommonRequestManager
						  requestManager, Post post) {
		this.context = context;
		this.requestManager = requestManager;
		this.post = post;
		this.responseHandler = new CommonResponseHandler(context, this);
	}

	private Request createRequest() {
		Bundle params;
		if(nextPaginationParams != null) {
			params = nextPaginationParams.getParams();
		} else {
			params = new Bundle();
		}
		params.putString("owner_id", post.getAuthor().getId());
		params.putString("post_id", post.getId());
		params.putString("username", post.getAuthor().getUsername());
		params.putString("sort", "desc");

		return RequestFactory.create(RequestFactory.REQUEST_COMMENTS, post.getProvider(), params);
	}

	private void loadComments() {
		if(!isLoading) {
			requestManager.execute(createRequest(), responseHandler);
		}
	}

	@Override
	public void success(Request request, Bundle resultData) {
		Feed<Comment> feed = resultData.getParcelable("comments");
		comments.addAll(feed.getItems());
		nextPaginationParams = feed.getPagination();

		dispatchOnLoadedItems(getNextCommentsPart());
	}

	@Override
	public void error(CommonSocialNetworkException exception, Request request) {
		dispatchError(exception);
	}

	@Override
	public void start(Request request) {
		isLoading = true;
	}

	@Override
	public void finish(Request request) {
		isLoading = false;
	}

	@Override
	public void start() {
		if(!comments.isEmpty()) {
			comments.clear();
		}
		currentPosition = 0;
		nextPaginationParams = null;
		loadComments();
	}

	@Override
	public void next() {
		if(needLoad()) {
			loadComments();
		} else {
			dispatchOnLoadedItems(getNextCommentsPart());
		}
	}

	private boolean needLoad() {
		return hasNextExternal() && comments.size() < (currentPosition + PART_COUNT);
	}

	private List<Comment> getNextCommentsPart() {
		List<Comment> list = new ArrayList<Comment>();
		if(comments.size() > currentPosition) {
			if(comments.size() < PART_COUNT+currentPosition) {
				int endIndex = comments.size();
				list.addAll(new ArrayList<Comment>(comments.subList(currentPosition, endIndex)));
				currentPosition = endIndex;
			} else {
				list.addAll(new ArrayList<Comment>(comments.subList(currentPosition, PART_COUNT+currentPosition)));
				currentPosition += PART_COUNT;
			}


		}

		return list;
	}

	private void dispatchOnLoadedItems(List<Comment> items) {
		if(this.listener != null) {
			this.listener.onFeedItemsLoaded(items);
		}
	}

	private void dispatchError(CommonSocialNetworkException ex) {
		if(this.listener != null) {
			this.listener.onError(ex);
		}
	}

	@Override
	public void setFeedLoaderListener(FeedLoaderListener<Comment> listener) {
		this.listener = listener;
	}

	@Override
	public void removeFeedLoaderListener() {
		this.listener = null;
	}

	@Override
	public void setMode(FeedLoaderMode mode) {
		//pass implementation
	}

	private boolean hasNextExternal() {
		return nextPaginationParams == null || nextPaginationParams.hasNext();
	}

	@Override
	public boolean hasNext() {
		return hasNextExternal() || comments.size() > currentPosition;
	}
}
