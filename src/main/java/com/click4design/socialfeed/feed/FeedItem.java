package com.click4design.socialfeed.feed;

import android.os.Parcelable;
import com.click4design.socialfeed.data.model.User;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;

/**
 * Author: Khamidullin Kamil
 * Date: 24.02.14
 * Time: 0:04
 */
public interface FeedItem<T> extends Comparable<T>, Parcelable {
	public SocialNetworkProviderInterface getProvider();

	public long getDate();

	public User getAuthor();
}
