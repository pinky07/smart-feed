package com.click4design.socialfeed.feed;

import com.click4design.socialfeed.data.RequestFactory;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 08.03.14
 * Time: 17:40
 */
public interface FeedLoader<T extends FeedItem<T>> {

	/**
	 * Начинает загрузку ленты с начала
	 */
	public void start();

	/**
	 * Загружает следующую часть ленты
	 */
	public void next();

	public void setFeedLoaderListener(FeedLoaderListener<T> listener);

	public void removeFeedLoaderListener();

	public void setMode(FeedLoaderMode mode);

	public boolean hasNext();

	/**
	 * Интерфейс обработчика загрузчика ленты
	 * @param <T>
	 */
	public static interface FeedLoaderListener<T extends FeedItem<T>> {
		public void onFeedItemsLoaded(List<T> postList);

		public void onError(CommonSocialNetworkException exception);

		public void onConnectionError();
	}

	public static enum FeedLoaderMode {
		HOME_TIMELINE(RequestFactory.REQUEST_NEWS),
		USER_TIMELINE(RequestFactory.REQUEST_MY_FEED),
		FAVORITES(RequestFactory.REQUEST_FAVORITES);

		FeedLoaderMode(int requestCode) {
			this.requestCode = requestCode;
		}

		int requestCode;

		public int getRequestCode() {
			return this.requestCode;
		}
	}
}
