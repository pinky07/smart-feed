package com.click4design.socialfeed.ui;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.click4design.roboholo.RoboHoloDialogFragment;
import com.click4design.socialfeed.R;
import com.perm.kate.api.Auth;
import org.holoeverywhere.LayoutInflater;
import roboguice.inject.InjectView;

/**
 * Author: Khamidullin Kamil
 * Date: 28.06.13
 * Time: 19:00
 */
public class SocialNetworkAuthFragment extends RoboHoloDialogFragment {

    @InjectView(R.id.web)
    protected WebView mWebView;

    protected final static String API_ID = "3728229";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Title!");

        return inflater.inflate(R.layout.fragment_social_network_auth, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mWebView.getSettings().setJavaScriptEnabled(true);
        String url=Auth.getUrl(API_ID, Auth.getSettings());
        mWebView.loadUrl(url);
    }
}
