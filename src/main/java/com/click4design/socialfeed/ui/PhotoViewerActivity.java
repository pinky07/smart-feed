package com.click4design.socialfeed.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.click4design.roboholo.RoboHoloActivity;
import com.click4design.socialfeed.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import org.holoeverywhere.widget.ViewPager;
import uk.co.senab.photoview.PhotoView;
import android.view.ViewGroup.LayoutParams;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 13.02.14
 * Time: 3:56
 */
public class PhotoViewerActivity extends RoboHoloActivity {
	public final static String EXTRA_PHOTO_URLS = "photo_urls";
	public final static String EXTRA_CURRENT_ITEM = "current_item_index";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_viewer);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("Просмотр фото");
		actionBar.setDisplayUseLogoEnabled(false);


		ViewPager mViewPager = (HackyViewPager) findViewById(R.id.view_pager);
		setContentView(mViewPager);
		Intent intent = getIntent();

		List<String> urls = intent.getStringArrayListExtra(EXTRA_PHOTO_URLS);

		mViewPager.setAdapter(new SamplePagerAdapter(urls));
		if(intent.hasExtra(EXTRA_CURRENT_ITEM)) {
			mViewPager.setCurrentItem(intent.getIntExtra(EXTRA_CURRENT_ITEM, 0));
		}
	}

	static class SamplePagerAdapter extends PagerAdapter {
		protected List<String> urls;
		protected ImageLoader imageLoader;

		SamplePagerAdapter(List<String> urls) {
			this.urls = urls;
			imageLoader = ImageLoader.getInstance();
		}

		@Override
		public int getCount() {
			return urls.size();
		}

		@Override
		public View instantiateItem(ViewGroup container, int position) {
			final PhotoView photoView = new PhotoView(container.getContext());
			imageLoader.loadImage(urls.get(position), new SimpleImageLoadingListener() {
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					photoView.setImageBitmap(loadedImage);
				}

				@Override
				public void onLoadingStarted(String imageUri, View view) {
					super.onLoadingStarted(imageUri, view);    //To change body of overridden methods use File | Settings | File Templates.
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					super.onLoadingFailed(imageUri, view, failReason);    //To change body of overridden methods use File | Settings | File Templates.
				}

				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					super.onLoadingCancelled(imageUri, view);    //To change body of overridden methods use File | Settings | File Templates.
				}
			});

			// Now just add PhotoView to ViewPager and return it
			container.addView(photoView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

			return photoView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == object;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
