package com.click4design.socialfeed.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.click4design.socialfeed.R;
import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.widget.ArrayAdapter;
import org.holoeverywhere.widget.TextView;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 09.03.14
 * Time: 12:06
 */
public class FeedFragmentNavigationAdapter extends ArrayAdapter<String> {
	private CharSequence title;
	public FeedFragmentNavigationAdapter(Context context, CharSequence title, List<String> subtitles) {
		super(context, R.layout.simple_spinner_item, subtitles);
		this.title = title;
		setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.action_bar_navigation_layout, null);
		}
		TextView title = (TextView) convertView.findViewById(R.id.title);
		title.setText(this.title);

		TextView subtitle = (TextView) convertView.findViewById(R.id.subtitle);
		subtitle.setText(getItem(position));

		return convertView;
	}
}
