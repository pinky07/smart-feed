package com.click4design.socialfeed.ui;

import android.os.Bundle;
import com.click4design.core.ui.CommonActivity;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.SocialFeedApplication;
import com.click4design.socialfeed.data.RequestFactory;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.SocialNetworksManager;
import com.google.inject.Inject;
import org.holoeverywhere.addon.AddonSlider;
import org.holoeverywhere.addon.Addons;
import org.holoeverywhere.slider.SliderMenu;
import com.click4design.socialfeed.feed.FeedLoader.FeedLoaderMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 08.01.14
 * Time: 22:29
 */
@Addons(AddonSlider.class)
public class StartActivity extends CommonActivity {
	@Inject
	SocialNetworksManager socialNetworksManager;

    public AddonSlider.AddonSliderA addonSlider() {
        return addon(AddonSlider.class);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SliderMenu sliderMenu = addonSlider().obtainDefaultSliderMenu(R.layout.menu);

        List<SocialNetworkProviderInterface> providers = socialNetworksManager.getAuthorizedSocialNetworks();
        ArrayList<SocialNetworkProviderInterface> providersAcceptPhoto = new ArrayList<SocialNetworkProviderInterface>();
        for(SocialNetworkProviderInterface prov :providers) {
            if(prov.isAcceptPhoto()) {
                providersAcceptPhoto.add(prov);
            }
        }

        Bundle feedFragmentArgs = new Bundle();
        feedFragmentArgs.putSerializable(AFeedFragment.ARG_MODE, FeedLoaderMode.HOME_TIMELINE);
		feedFragmentArgs.putString(AFeedFragment.ARG_TITLE, "Лента");

		Bundle favoritesFeedFragmentArgs = new Bundle();
		favoritesFeedFragmentArgs.putSerializable(AFeedFragment.ARG_MODE, FeedLoaderMode.FAVORITES);
		favoritesFeedFragmentArgs.putString(AFeedFragment.ARG_TITLE, "Избранное");

		Bundle myFeedFragmentArgs = new Bundle();
		myFeedFragmentArgs.putSerializable(AFeedFragment.ARG_MODE, FeedLoaderMode.USER_TIMELINE);
		myFeedFragmentArgs.putString(AFeedFragment.ARG_TITLE, "Моя лента");

        Bundle photoFeedFragmentArgs = new Bundle();
        photoFeedFragmentArgs.putInt(FeedFragment.ARG_REQUEST_CODE, RequestFactory.REQUEST_PHOTOS);

        sliderMenu.add("Лента", AFeedFragment.class, feedFragmentArgs, SliderMenu.BLUE);
		sliderMenu.add("Избранное", AFeedFragment.class, favoritesFeedFragmentArgs, SliderMenu.BLUE);
		sliderMenu.add("Моя лента", AFeedFragment.class, myFeedFragmentArgs, SliderMenu.BLUE);
        //sliderMenu.add("Фото", FeedFragment.class, photoFeedFragmentArgs, SliderMenu.GREEN);
        sliderMenu.add("Аккаунты", AccountsFragment.class, SliderMenu.BLUE);

	   	if(providers.isEmpty()) {
			sliderMenu.setCurrentPage(3);
		}
    }

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
	}
}
