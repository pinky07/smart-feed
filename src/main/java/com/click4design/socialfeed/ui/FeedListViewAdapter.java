package com.click4design.socialfeed.ui;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.data.model.Photo;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.network.resource.Resources;
import com.click4design.socialfeed.network.resource.ResourcesManager;
import com.click4design.socialfeed.ui.actions.PostAction;
import com.jake.quiltview.QuiltView;
import com.nostra13.universalimageloader.core.ImageLoader;
import org.holoeverywhere.widget.ArrayAdapter;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.TextView;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.*;

/**
 * Author: Khamidullin Kamil
 * Date: 22.09.13
 * Time: 14:55
 */
public class FeedListViewAdapter extends ArrayAdapter<Post> {
	protected ResourcesManager mResourcesManager;
    protected Context mContext;
    protected LayoutInflater mInflater;
    protected ImageLoader mImageLoader = ImageLoader.getInstance();
    protected DateTimeFormatter mDateTimeFormat = DateTimeFormat.forPattern("dd MMMM HH:mm:ss");
    protected Map<Integer, QuiltView> mCache;
    protected boolean isScrolling = false;
	private PostActionsListener mPostActionsListener;

	protected View.OnClickListener photoOnClickHandler = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(mContext, PhotoViewerActivity.class);
			intent.putExtra(PhotoViewerActivity.EXTRA_PHOTO_URLS, (ArrayList<String>)v.getTag());
			intent.putExtra(PhotoViewerActivity.EXTRA_CURRENT_ITEM, v.getId());
			mContext.startActivity(intent);
		}
	};

    public FeedListViewAdapter(Context context, ResourcesManager resourcesManager, List<Post> news) {
        super(context, android.R.layout.simple_spinner_item, news);
        super.setNotifyOnChange(false);
        mContext  = context;
		mResourcesManager = resourcesManager;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //mNews     = news;
        mCache    = new HashMap<Integer, QuiltView>();
    }

    public FeedListViewAdapter(Context context, ResourcesManager resourcesManager) {
        this(context, resourcesManager, new ArrayList<Post>());
    }

	public void setPostActionsListener(PostActionsListener listener) {
		this.mPostActionsListener = listener;
	}

	public void setScrolling(boolean value) {
        this.isScrolling = value;
    }

    public boolean isScrolling() {
        return isScrolling;
    }

    public boolean contains(Post item) {
        return getPosition(item) != -1;
    }

    @Override
    public void addAll(Collection<? extends Post> collection) {
        List<Post> posts = new ArrayList<Post>();
        for(Post item : collection) {
            if(!contains(item)) {
                posts.add(item);
            }
        }
        if(android.os.Build.VERSION.SDK_INT >= 11) {
            super.addAll(posts);
        } else {
            setNotifyOnChange(false);
            for(Post p : posts) {
                super.add(p);
            }
        }
    }

    @Override
    public void addAll(Post... items) {
        List<Post> posts = new ArrayList<Post>();
        for(Post item : items) {
            if(!contains(item)) {
                posts.add(item);
            }
        }

        if(android.os.Build.VERSION.SDK_INT >= 11) {
            super.addAll(posts);
        } else {
            for(Post p : posts) {
                super.add(p);
            }
        }
    }

	public List<Post> subList(int start, int end) {
		List<Post> posts = new ArrayList<Post>();
		for (int i = start; i < end; i ++) {
			posts.add(getItem(i));
		}

		return posts;
	}

    @Override
    public void add(Post object) {
        if(!contains(object)) {
            super.add(object);
        }
    }

    public void sort() {
        super.sort(new Comparator<Post>() {
            @Override
            public int compare(Post lhs, Post rhs) {
                return (int)(rhs.getDate() - lhs.getDate());
            }
        });
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

	@Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        final Post post = getItem(i);
		Resources res = mResourcesManager.provide(post.getProvider());
        View view = convertView;
        final ViewHolder holder;

        if(view == null) {
            view = mInflater.inflate(R.layout.lv_news_item, viewGroup, false);
            view.setId(post.hashCode());
            holder = new ViewHolder();
            holder.id = post.hashCode();
			holder.fave = (LinearLayout)view.findViewById(R.id.fave);
			holder.comment = (LinearLayout)view.findViewById(R.id.add_comment);
			holder.repost = (LinearLayout)view.findViewById(R.id.repost);
            holder.authorAvatar = (ImageView)view.findViewById(R.id.avatar);
            holder.networkIcon = (ImageView)view.findViewById(R.id.network_icon);
			holder.faveIcon = (ImageView)view.findViewById(R.id.fave_icon);
            holder.author = (TextView)view.findViewById(R.id.author);
            holder.text   = (TextView)view.findViewById(R.id.text);
            holder.date   = (TextView)view.findViewById(R.id.date);
            holder.photos = (QuiltView)view.findViewById(R.id.photos);
            holder.photos.setBaseWidth(viewGroup.getWidth() - (viewGroup.getPaddingLeft() + viewGroup.getPaddingRight() + view.getPaddingLeft()+view.getPaddingRight()));
            holder.commentsCount = (TextView)view.findViewById(R.id.comments_count);
            holder.likesCount = (TextView)view.findViewById(R.id.likes_count);
            holder.repostsCount  = (TextView)view.findViewById(R.id.reposts_count);
			holder.postActions = (LinearLayout)view.findViewById(R.id.post_action_layout);

            view.setTag(holder);
        } else {
            holder = (ViewHolder)view.getTag();

            if(holder.id != post.hashCode()) {

                ViewGroup parent = (ViewGroup) holder.photos.getParent();
                int index = parent.indexOfChild(holder.photos);
                parent.removeView(holder.photos);
                holder.photos = new QuiltView(mContext, true, viewGroup.getWidth() - (viewGroup.getPaddingLeft() + viewGroup.getPaddingRight() + view.getPaddingLeft()+view.getPaddingRight()), 0);
                holder.photos.setId(R.id.photos);
                if(holder.photos.getParent() != null) {
                    ((ViewGroup) holder.photos.getParent()).removeView(holder.photos);
                }
                parent.addView(holder.photos, index);
                holder.id = post.hashCode();
            }
        }
        holder.networkIcon.setImageResource(res.getSocialNetworkIcon());
        holder.author.setText(post.getAuthor().getFullName());

        if(TextUtils.isEmpty(post.getText())) {
            holder.text.setVisibility(View.GONE);
        } else {
            holder.text.setText(post.getText());
            holder.text.setVisibility(View.VISIBLE);
        }

		if(post.isFaved()) {
			holder.faveIcon.setImageResource(res.getFavedIcon());
		} else {
			holder.faveIcon.setImageResource(res.getFaveIcon());
		}

        holder.commentsCount.setText(post.getCommentsCount() != -1 ? Integer.toString(post.getCommentsCount()) : "");
        holder.likesCount.setText(post.getFavesCount() != -1 ? Integer.toString(post.getFavesCount()) : "");
        holder.repostsCount.setText(post.getRepostsCount() != -1 ?Integer.toString(post.getRepostsCount()) : "");
        holder.date.setText(mDateTimeFormat.print(new DateTime(getItem(i).getDate() * 1000)));
        if(post.hasPhotos() && (holder.photos.quilt.views.isEmpty() || holder.photos.quilt.getTag() == null)) {
            if(/*!isScrolling() && */holder.photos.quilt.getTag() == null && !holder.photos.quilt.views.isEmpty()) {
                List<Photo> photos = post.getPhotos(6);
                for(int j = 0; j < photos.size(); j ++) {
					ImageView imageView = ((ImageView)((LinearLayout)holder.photos.quilt.views.get(j)).getChildAt(0));
					imageView.setId(j);
					imageView.setTag(post.getPhotoUrls());
					imageView.setOnClickListener(photoOnClickHandler);
					mImageLoader.displayImage(photos.get(j).getUrl(), imageView);
                    holder.photos.quilt.setTag(new Object());
                }

            } else if(holder.photos.quilt.views.isEmpty()) {
                ArrayList<ImageView> images = new ArrayList<ImageView>();
				List<Photo> photos = post.getPhotos(6);
                for(int j = 0; j < photos.size(); j ++) {
                    final ImageView image = new ImageView(mContext);
                    image.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                    image.setScaleType(ImageView.ScaleType.CENTER_CROP);
					image.setId(j);
					image.setTag(post.getPhotoUrls());
					image.setOnClickListener(photoOnClickHandler);

					mImageLoader.displayImage(photos.get(j).getUrl(), image);
					holder.photos.quilt.setTag(new Object());

                    images.add(image);
                }

                holder.photos.addPatchImages(images);
                holder.photos.setTag(post.getId());
            }
        }

		if(holder.authorAvatar.getTag() == null || !((String)holder.authorAvatar.getTag()).equals(post.getId())) {
			mImageLoader.displayImage(post.getAuthor().getAvatarUrl(), holder.authorAvatar);
			holder.authorAvatar.setTag(post.getId());
		}



		if(post.canComment() || !post.canFave() || post.canRepost()) {
			holder.postActions.setVisibility(View.VISIBLE);
			if(post.canComment()) {
				holder.comment.setVisibility(View.VISIBLE);
				holder.comment.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						mPostActionsListener.createCommentAction().doAction(post);
					}
				});
			} else {
				holder.comment.setVisibility(View.GONE);
			}

			if(post.canFave()) {
				holder.fave.setVisibility(View.VISIBLE);
				holder.fave.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						PostAction action = (post.isFaved()) ?
								mPostActionsListener.createUnFaveAction():
								mPostActionsListener.createFaveAction();

						action.doAction(post);
						notifyDataSetChanged();
					}
				});
			} else {
				holder.fave.setVisibility(View.GONE);
			}


			if(post.canRepost()) {
				holder.repost.setVisibility(View.VISIBLE);
				holder.repost.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						mPostActionsListener.createRepostAction().doAction(post);
					}
				});
			} else {
				holder.repost.setVisibility(View.GONE);
			}

		} else {
			holder.postActions.setVisibility(View.GONE);
		}

        return view;
    }

    public static class ViewHolder {
        int id;
        ImageView authorAvatar;
        ImageView networkIcon;
		ImageView faveIcon;
		LinearLayout fave;
		LinearLayout repost;
		LinearLayout comment;
		LinearLayout postActions;
        TextView author;
        TextView text;
        TextView date;
        TextView commentsCount;
        TextView repostsCount;
        TextView likesCount;
        QuiltView photos;
    }

	public static interface PostActionsListener {
		public PostAction createFaveAction();
		public PostAction createUnFaveAction();
		public PostAction createCommentAction();
		public PostAction createRepostAction();
	}
}
