package com.click4design.socialfeed.ui;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.*;
import android.widget.AbsListView;
import com.click4design.core.ui.CommonListFragment;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.CommonResponseHandler;
import com.click4design.socialfeed.data.IRequestListener;
import com.click4design.socialfeed.data.RequestFactory;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Feed;
import com.click4design.socialfeed.data.model.Pagination;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.data.service.BatchRequest;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.SocialNetworksManager;
import com.click4design.socialfeed.network.resource.ResourcesManager;
import com.click4design.socialfeed.utils.MetricsConverter;
import com.foxykeep.datadroid.requestmanager.Request;
import android.widget.AbsListView.OnScrollListener;
import com.google.inject.Inject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.PauseOnScrollListener;
import org.apache.commons.lang3.StringUtils;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.*;
import uk.co.senab.actionbarpulltorefresh.extras.actionbarcompat.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import uk.co.senab.actionbarpulltorefresh.library.viewdelegates.ViewDelegate;
import com.click4design.socialfeed.ui.FeedListViewAdapter.PostActionsListener;
import com.click4design.socialfeed.ui.FeedListViewAdapter.ViewHolder;

import java.util.*;

/**
 * Author: Khamidullin Kamil
 * Date: 27.06.13
 * Time: 23:50
 */
public class FeedFragment extends CommonListFragment/* RoboSherlockListFragment*/
		implements OnScrollListener, OnRefreshListener, IRequestListener, ActionBar.OnNavigationListener {

    @Inject
    protected CommonRequestManager requestManager;

	@Inject
	protected ResourcesManager resourcesManager;

	@Inject
	protected SocialNetworksManager socialNetworksManager;


    private List<SocialNetworkProviderInterface> providers;

	List<SocialNetworkProviderInterface> selectedProviders = new ArrayList<SocialNetworkProviderInterface>();

    /**
     * Коллекция с последним постом из каждоый соц сети
     */
    private Map<SocialNetworkProviderInterface, Post> lastPosts;

    /**
     * Cодержит параметры для загрузки следующей партии постов,
     * для конкретной соц сети
     */
    private Map<SocialNetworkProviderInterface, Bundle> lastPagingParams;

    /**
     * Статус загрузки постов из соц сетей
     */
    private Map<SocialNetworkProviderInterface, Boolean> loadingStatus;

    private transient volatile List<Post> remainPosts = new ArrayList<Post>();

    private FeedListViewAdapter adapter;

    private View loadingFooter;

    private int requestCode;

    public final static String ARG_REQUEST_CODE = "request_code";
    public final static String ARG_PROVIDERS = "providers";
    /**
     * Количество загружаемых за раз постов
     */
    final static int LOAD_POST_COUNT = 40;

    private PullToRefreshLayout mPullToRefreshLayout;

    protected CommonResponseHandler mRequestHandler;

	protected List<Post> mPosts = new ArrayList<Post>();

	private final static String LOG_TAG = "feed";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //create request manager handler
        mRequestHandler =  new CommonResponseHandler(getActivity(), this) {
            private static final long serialVersionUID = -5405342106487764838L;
            @Override
            public void onConnectionError() {
                super.onConnectionError();
                onNonInternetConnection();
            }
        };
        setHasOptionsMenu(true);
    }


	@Override
	public void onResume() {
		super.onResume();
		mRequestHandler.setRequestListener(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mRequestHandler.removeRequestListener();
	}

	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter        = new FeedListViewAdapter(getActivity(), resourcesManager, mPosts);
		//adapter.setPostActionsListener(this);
        requestCode    = getArguments().getInt(ARG_REQUEST_CODE);
        providers      = socialNetworksManager.getAuthorizedSocialNetworks();
		selectedProviders.addAll(providers);

        List<String> socialNetworkTitles  = new ArrayList<String>();
        socialNetworkTitles.add("Smart Tape");
        for(SocialNetworkProviderInterface p : providers) {
            socialNetworkTitles.add(StringUtils.capitalize(p.getCode().toLowerCase()));
        }

        configurationActionBar(getSupportActionBar(), socialNetworkTitles);
    }

    void configurationActionBar(ActionBar actionBar, List<String> socialNetworkTitles) {
        ArrayAdapter mSpinnerAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, socialNetworkTitles);
        mSpinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setListNavigationCallbacks(mSpinnerAdapter, this);
    }

	public void refreshFeed() {
        if(!remainPosts.isEmpty()) {
            remainPosts.clear();
        }

        if(!lastPosts.isEmpty()) {
            lastPosts.clear();
        }

        if(!lastPagingParams.isEmpty()) {
            lastPagingParams.clear();
        }

        if(!loadingStatus.isEmpty()) {
            loadingStatus.clear();
        }

		if(!adapter.isEmpty()) {
			adapter.clear();
		}

		if(getListAdapter() != null) {
			setListAdapter(null);
			setListShown(false);
		}

        if(selectedProviders.isEmpty()) {
            setEmptyText("Необходимо добавить аккаунты соц. сетей");
            setListAdapter(adapter);
        } else {
            BatchRequest batchRequest = new BatchRequest();
            for (SocialNetworkProviderInterface provider:selectedProviders) {
                Bundle params = new Bundle();
                params.putInt("count", LOAD_POST_COUNT);
                batchRequest.add(RequestFactory.create(requestCode, provider, params));
            }

            requestManager.execute(batchRequest, mRequestHandler);
        }
	}

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.feed, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_compose:
				startActivity(new Intent(getActivity(), AddPostActivity.class));
                return true;
        }
        return false;
    }

    protected void setLoadingStatus(Request request, boolean isLoading) {
        SocialNetworkProviderInterface provider = (SocialNetworkProviderInterface)request.getParcelable("provider");
        loadingStatus.put(provider, isLoading);
    }

    protected List<Post> getRemainPosts() {
        return this.remainPosts;
    }


    protected synchronized void setRemainPosts(List<Post> remainPosts) {
        this.remainPosts = remainPosts;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
		setEmptyText("Данные отсутствуют");
		((TextView)getListView().getEmptyView()).setTextSize(16);
        ViewGroup viewGroup = (ViewGroup) view;
        mPullToRefreshLayout = new PullToRefreshLayout(viewGroup.getContext());
        ActionBarPullToRefresh.from(getActivity())
                .insertLayoutInto(viewGroup)
                .theseChildrenArePullable(getListView(), getListView().getEmptyView())
                .listener(this)
				.useViewDelegate(TextView.class, new ViewDelegate() {
					@Override
					public boolean isReadyForPull(View view, float v, float v2) {
						return Boolean.TRUE;
					}
				})
                .setup(mPullToRefreshLayout);

        lastPosts = new HashMap<SocialNetworkProviderInterface, Post>();
        lastPagingParams = new HashMap<SocialNetworkProviderInterface, Bundle>();
        loadingStatus   = new HashMap<SocialNetworkProviderInterface, Boolean>();

        loadingFooter  = View.inflate(getActivity(), R.layout.lv_pull_more_loader, null);
        loadingFooter.setVisibility(View.GONE);

        getListView().setBackgroundResource(R.color.base_gray);
        getListView().setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        getListView().setDivider(new ColorDrawable(Color.TRANSPARENT));
        getListView().setDividerHeight(MetricsConverter.dp2px(getActivity(), 12));
        getListView().setDrawSelectorOnTop(true);

        int padding = MetricsConverter.dp2px(getActivity(), 10);
        getListView().setPadding(padding, 0, padding, 0);
        getListView().setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), false, true, this));
        getListView().addFooterView(loadingFooter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(getActivity(), PostActivity.class);
        intent.putExtra(PostActivity.EXTRA_POST,  adapter.getItem(position));
        startActivity(intent);
    }

    void loadPosts(SocialNetworkProviderInterface provider, Bundle params) {
		Request request = RequestFactory.create(requestCode, provider, params);
		requestManager.execute(request, mRequestHandler);
    }

    protected boolean isNeedAdapter() {
        if(loadingStatus.keySet().containsAll(selectedProviders) && getListAdapter() == null) {
			return !loadingStatus.containsValue(true);

            /*for(Map.Entry<SocialNetworkProviderInterface, Boolean> set : loadingStatus.entrySet()) {
                if(set.getValue()) {
                    return false;
                }
            }
            return true; */
        } else { return false; }
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {}

    protected void pullMore(SocialNetworkProviderInterface provider) {
		if(app().isOnline()) {
			loadingFooter.setVisibility(View.VISIBLE);
			loadPosts(provider, lastPagingParams.get(provider));
		} else {
			onNonInternetConnection();
		}
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        //Получаем последний видимый элемент
        int lastItem = (firstVisibleItem + visibleItemCount);
        int footerViewsCount = getListView().getFooterViewsCount();
        if(totalItemCount > visibleItemCount && lastItem == totalItemCount && (totalItemCount-footerViewsCount) > 0) {
            int lastItemIndex = (lastItem-footerViewsCount)-1;
			SocialNetworkProviderInterface provider = adapter.getItem(lastItemIndex).getProvider();
			if(!loadingStatus.get(provider) && lastPosts.containsKey(provider)) {
				pullMore(provider);
			}
        }
    }

    @Override
    public void onRefreshStarted(View view) {
        refreshFeed();
    }

    @Override
    public void success(Request request, Bundle resultData) {
		if (request.getRequestType() == requestCode) {
			processResult(resultData);
		}
    }

	private void processResult(Bundle resultData) {
		SocialNetworkProviderInterface provider = resultData.getParcelable("provider");
		Feed<Post> news = resultData.getParcelable("news");

		adapter.setNotifyOnChange(false);
		adapter.addAll(news.getItems());
		adapter.addAll(getRemainPosts());
		adapter.sort();

		if(!news.isEmpty()) {
			Pagination pagination = news.getPagination();
			if(pagination.hasNext()) {
				Post lastPost = news.getLastItem();
				lastPosts.put(provider, lastPost);
				Log.i(LOG_TAG, String.format("last post id:%s; time%s", lastPost.getId(), lastPost.getDate()));

				lastPagingParams.put(provider, pagination.getParams());
			} else {
				lastPosts.remove(provider);
				lastPagingParams.remove(provider);
			}
		}

		Post currentLastPost = getCurrentLastPost();
		if(currentLastPost != null) {
			int firstLastPostIndex = adapter.getPosition(currentLastPost);
			if(adapter.getCount()-1 > firstLastPostIndex) {
				synchronized (this) {
					List<Post> tempList = adapter.subList(0, firstLastPostIndex+1);
					setRemainPosts(adapter.subList(firstLastPostIndex+1, adapter.getCount()));
					adapter.clear();
					adapter.addAll(tempList);
				}
			}
		}
	}
	
	private Post getCurrentLastPost() {
		if(lastPosts != null && !lastPosts.isEmpty()) {
			return Collections.min(lastPosts.values());
		} else {
			return null;
		}
	}

	private void onNonInternetConnection() {
		setEmptyText("Отсутствует соединение с интернетом");
        setListAdapter(null);
		setListShown(true);
        if(mPullToRefreshLayout.isRefreshing()) {
            mPullToRefreshLayout.setRefreshComplete();
        }
	}

    @Override
    public void error(CommonSocialNetworkException exception, Request request) {
        SocialNetworkProviderInterface provider = (SocialNetworkProviderInterface)request.getParcelable("provider");
        lastPosts.remove(provider);
        if(!lastPosts.isEmpty()) {
            Post firstLastPost = Collections.min(lastPosts.values());
            pullMore(firstLastPost.getProvider());
        }
    }

    @Override
    public void start(Request request) {
        setLoadingStatus(request, true);
    }

    @Override
    public void finish(Request request) {
        setLoadingStatus(request, false);

        if(isNeedAdapter()) {
            setListAdapter(adapter);
		} else {
			if(getListAdapter() != null) {
				adapter.notifyDataSetChanged();
				setListShown(true);
			}
        }

        loadingFooter.setVisibility(View.GONE);
        mPullToRefreshLayout.setRefreshComplete();
    }

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		selectedProviders.clear();
		if(itemPosition == 0) {
			selectedProviders.addAll(providers);
		} else {
			selectedProviders.add(providers.get(itemPosition-1));
		}
		refresh(100);
		return true;
	}

	public void refresh(final int timeout) {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				try {
					Thread.sleep(timeout);
				} catch (InterruptedException iex) {/*pass*/}
				return null;
			}

			@Override
			protected void onPostExecute(Void aVoid) {
				refreshFeed();
			}
		}.execute();
	}

	private void doFave(ViewHolder viewHolder, Post post) {
		post.setFaved(true);
		post.setFavesCount(post.getFavesCount()+1);
		viewHolder.faveIcon.setImageResource(resourcesManager.provide(post.getProvider()).getFavedIcon());
		viewHolder.likesCount.setText(Integer.toString(post.getFavesCount()));
	}

	private void doUnFave(ViewHolder viewHolder, Post post) {
		post.setFaved(false);
		post.setFavesCount(post.getFavesCount()-1);
		viewHolder.faveIcon.setImageResource(resourcesManager.provide(post.getProvider()).getFaveIcon());
		viewHolder.likesCount.setText(Integer.toString(post.getFavesCount()));
	}

	//@Override
	public void onFaveAction(final ViewHolder viewHolder,final Post post) {
		doFave(viewHolder, post);
		Request request = new Request(RequestFactory.REQUEST_FAVE);
		request.put("provider", post.getProvider());
		Bundle params = new Bundle();
		params.putParcelable("post", post);
		request.put("params", params);

		requestManager.execute(request, new CommonResponseHandler(getActivity(), new IRequestListener() {
			@Override
			public void success(Request request, Bundle resultData) {}

			@Override
			public void error(CommonSocialNetworkException exception, Request request) {
				doUnFave(viewHolder, post);
			}

			@Override
			public void start(Request request) {}

			@Override
			public void finish(Request request) {}
		}) {
			private static final long serialVersionUID = -5405342432487764838L;
			@Override
			public void onConnectionError() {
				super.onConnectionError();
				onNonInternetConnection();
			}
		});
	}

	//@Override
	public void onAddCommentAction(ViewHolder viewHolder, Post post) {
		final EditText input = new EditText(getActivity());
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		input.setGravity(Gravity.TOP);
		input.setHint("Текст комментария...");
		input.setMinLines(2);
		input.setLayoutParams(lp);

		new AlertDialog.Builder(getActivity())
				.setTitle("Новый комментарий")
				.setView(input)
				.setPositiveButton(android.R.string.ok, null)
				.setNegativeButton(android.R.string.cancel, null)
				.create()
				.show();


		//alertDialog.setView(input);
	}

	//@Override
	public void onRepostAction(ViewHolder viewHolder, Post post) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	//@Override
	public void onUnFaveAction(final ViewHolder viewHolder, final Post post) {
		doUnFave(viewHolder, post);
		Request request = new Request(RequestFactory.REQUEST_UNFAVE);
		request.put("provider", post.getProvider());
		Bundle params = new Bundle();
		params.putParcelable("post", post);
		request.put("params", params);

		requestManager.execute(request, new CommonResponseHandler(getActivity(), new IRequestListener() {
			@Override
			public void success(Request request, Bundle resultData) {}

			@Override
			public void error(CommonSocialNetworkException exception, Request request) {
				doFave(viewHolder, post);
			}

			@Override
			public void start(Request request) {}

			@Override
			public void finish(Request request) {}
		}) {
			private static final long serialVersionUID = -5405342106487723438L;
			@Override
			public void onConnectionError() {
				super.onConnectionError();
				onNonInternetConnection();
			}
		});
	}
}
