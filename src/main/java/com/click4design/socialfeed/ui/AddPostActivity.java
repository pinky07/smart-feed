package com.click4design.socialfeed.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import com.click4design.core.ui.CommonActivity;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.SocialFeedApplication;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.CommonResponseHandler;
import com.click4design.socialfeed.data.IRequestListener;
import com.click4design.socialfeed.data.RequestFactory;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.data.service.BatchRequest;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.SocialNetworksManager;
import com.click4design.socialfeed.utils.BitmapUtils;
import com.foxykeep.datadroid.requestmanager.Request;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.*;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

import javax.inject.Inject;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 01.02.14
 * Time: 11:33
 */
@ContentView(R.layout.fragment_add_post)
public class AddPostActivity extends CommonActivity
		implements MultiSelectSocialNetworkDialogFragment.OnSelectSocialNetworksListener, IRequestListener {
    final int CAMERA_PIC_REQUEST = 1112;
    final int GALLERY_PIC_REQUEST = 1113;
    final int GET_GEO_POINT_REQUEST = 1114;

    //@InjectView(R.id.social_network_spinner)
    protected Button mSocialNetworkSpinner;

    @InjectView(R.id.add_photo_from_camera)
    ImageButton addPhotoFromCamera;

    @InjectView(R.id.add_photo_from_gallery)
    ImageButton addPhotoFromGallery;

    @InjectView(R.id.remove_photo)
    ImageButton removePhotoButton;

    @InjectView(R.id.photo)
    ImageView photoView;

    @InjectView(R.id.add_photo_layout)
    LinearLayout addPhotoLayout;

    @InjectView(R.id.remove_photo_layout)
    LinearLayout removePhotoLayout;

	@InjectView(R.id.message)
	EditText messageView;

	String photoToUpload;

    protected List<SocialNetworkProviderInterface> mSocialNetworks;

    protected List<SocialNetworkProviderInterface> mSelectedSocialNetworks;

	private String formErrorText;

	ProgressDialog progressDialog;

	@Inject
	protected CommonRequestManager requestManager;

	@Inject
	protected SocialNetworksManager socialNetworksManager;

	protected CommonResponseHandler mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.fragment_add_post);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("Добавить запись");
        actionBar.setDisplayUseLogoEnabled(false);
		mListener =  new CommonResponseHandler(this, this);

        mSocialNetworks = new ArrayList<SocialNetworkProviderInterface>(socialNetworksManager.getAuthorizedSocialNetworks());
		mSocialNetworks.remove(SocialNetworkProvider.INSTAGRAM); //todo

        mSelectedSocialNetworks  = mSocialNetworks;

        mSocialNetworkSpinner = (Button)findViewById(R.id.social_network_spinner);

        mSocialNetworkSpinner.setText("Все соц сети");
        mSocialNetworkSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MultiSelectSocialNetworkDialogFragment fragment = MultiSelectSocialNetworkDialogFragment.newInstance(mSocialNetworks, mSelectedSocialNetworks);
                fragment.setTitle("Выберите соц. сети");
                fragment.setOnSelectSocialNetworksListener(AddPostActivity.this);
                fragment.show(getSupportFragmentManager());
            }
        });

        addPhotoFromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				File photostorage = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
				File photofile = new File(photostorage, (System.currentTimeMillis()) + ".jpg");

				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); //intent to start camera
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photofile));

                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), CAMERA_PIC_REQUEST);
            }
        });

        addPhotoFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_PIC_REQUEST);
            }
        });

        removePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(AddPostActivity.this)
                        .setMessage("Вы уверены что хотите удалить фото?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removePhoto();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .create()
                        .show();

            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (null != data) {
			if(requestCode == CAMERA_PIC_REQUEST || requestCode== GALLERY_PIC_REQUEST) {
				setUploadingPhoto(data.getData());
			}
        }
    }

    private void setUploadingPhoto(Uri uri) {
		try {
			File f = BitmapUtils.scaleImage(this, uri);
			Bitmap b = BitmapFactory.decodeFile(f.getPath());
			//Bitmap b = BitmapUtils.scaleImage(this, uri);
			photoView.setImageBitmap(b);
			addPhotoLayout.setVisibility(View.GONE);
			removePhotoLayout.setVisibility(View.VISIBLE);
			photoToUpload = f.getPath();
		} catch (IOException fex) {/*pass*/}
    }

    private void removePhoto() {
        photoView.setImageResource(R.drawable.ic_content_new_picture_big);
        removePhotoLayout.setVisibility(View.GONE);
        addPhotoLayout.setVisibility(View.VISIBLE);
        photoToUpload = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_add:
				if(isValidForm()) {
					sendPost(messageView.getText().toString(), photoToUpload);
				} else {
					showErrorDialog();
				}
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_post, menu);
        return true;
    }

    @Override
    public void onSelectSocialNetworks(List<SocialNetworkProviderInterface> socialNetworks) {
        mSelectedSocialNetworks = socialNetworks;

        String networksTitles;
        if(mSelectedSocialNetworks.isEmpty()) {
            networksTitles = "Выбрать";
        } else {
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < mSelectedSocialNetworks.size(); i++) {
                sb.append(mSelectedSocialNetworks.get(i).getCode().toUpperCase());
                if(i < mSelectedSocialNetworks.size() - 1) {
                     sb.append(", ");
                }
            }
            networksTitles = sb.toString();
        }

        mSocialNetworkSpinner.setText(networksTitles);
    }

	public boolean isValidForm() {
		if(mSelectedSocialNetworks.isEmpty()) {
			formErrorText = "Необходимо выбрать хотя бы одну соц. сеть";
			return false;
		} else if(TextUtils.isEmpty(messageView.getText().toString()) && photoToUpload == null) {
			formErrorText = "Необходимо написать текст сообщения";
			return false;
		} else {
			return true;
		}
	}

	public void showErrorDialog() {
		new AlertDialog.Builder(this)
				.setMessage(formErrorText)
				.setPositiveButton(android.R.string.ok, null)
				.create()
				.show();
	}

    public void sendPost(String message, String uploadPhotoURI) {
		BatchRequest batchRequest = new BatchRequest();
		for (SocialNetworkProviderInterface provider: mSelectedSocialNetworks) {
			Bundle params = new Bundle();
			params.putString("text", message);
			params.putString("photo_uri", uploadPhotoURI);
			batchRequest.add(RequestFactory.create(RequestFactory.REQUEST_ADD_POST, provider, params));
		}
		requestManager.execute(batchRequest, mListener);

		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("Отравка собщения");
		progressDialog.setMessage(String.format("Отправлено 0 из %s", mSelectedSocialNetworks.size()));
		progressDialog.setMax(mSelectedSocialNetworks.size());
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setIndeterminate(false);
		progressDialog.show();
    }

	private void progressSend() {
		if(progressDialog != null) {
			progressDialog.incrementProgressBy(1);
			progressDialog.incrementSecondaryProgressBy(1);
			progressDialog.setMessage(String.format("Отправлено 0 из %s", progressDialog.getProgress()));

			if(progressDialog.getProgress() >= progressDialog.getMax()) {
				progressDialog.dismiss();
				Toast.makeText(this, "Сообщение успешно добавлено", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}

	@Override
	public void success(Request request, Bundle resultData) {
		progressSend();
	}

	@Override
	public void error(CommonSocialNetworkException exception, Request request) {
		progressSend();
	}

    @Override
    public void start(Request request) {}

    @Override
	public void finish(Request request) {}
}
