package com.click4design.socialfeed.ui;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.*;
import android.widget.AbsListView;
import com.click4design.core.ui.CommonListFragment;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Comment;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.feed.FeedLoader;
import com.click4design.socialfeed.feed.buffered.BufferedFeedLoader;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.SocialNetworksManager;
import com.click4design.socialfeed.network.resource.ResourcesManager;
import com.click4design.socialfeed.ui.actions.*;
import com.click4design.socialfeed.utils.MetricsConverter;
import com.google.inject.Inject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.PauseOnScrollListener;
import org.apache.commons.lang3.StringUtils;
import org.holoeverywhere.widget.*;
import uk.co.senab.actionbarpulltorefresh.extras.actionbarcompat.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import uk.co.senab.actionbarpulltorefresh.library.viewdelegates.ViewDelegate;
import com.click4design.socialfeed.feed.FeedLoader.FeedLoaderMode;
import com.click4design.socialfeed.ui.FeedListViewAdapter.PostActionsListener;

import java.util.*;

/**
 * Author: Khamidullin Kamil
 * Date: 05.03.14
 * Time: 0:30
 */
public class AFeedFragment extends CommonListFragment
		implements AbsListView.OnScrollListener, OnRefreshListener,
		ActionBar.OnNavigationListener, FeedLoader.FeedLoaderListener<Post>,
		PostActionsListener
{

	@Inject
	protected CommonRequestManager requestManager;

	@Inject
	protected ResourcesManager resourcesManager;

	@Inject
	protected SocialNetworksManager socialNetworksManager;


	private List<SocialNetworkProviderInterface> providers;
	private List<SocialNetworkProviderInterface> selectedProviders = new ArrayList<SocialNetworkProviderInterface>();

	private FeedListViewAdapter adapter;

	private View loadingFooter;

	public final static String ARG_MODE = "mode";
	public final static String ARG_TITLE = "title";

	private PullToRefreshLayout mPullToRefreshLayout;

	protected List<Post> mPosts = new ArrayList<Post>();

	private final static String LOG_TAG = "feed";

	private FeedLoaderMode mode;

	private FeedLoader<Post> feedLoader;

	private String title;

	private boolean needClearPosts = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onResume() {
		super.onResume();
		feedLoader.setFeedLoaderListener(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		feedLoader.removeFeedLoaderListener();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		providers      = socialNetworksManager.getAuthorizedSocialNetworks();
		selectedProviders.addAll(providers);
		title		   = (getArguments().containsKey(ARG_TITLE)) ? getArguments().getString(ARG_TITLE) : "";
		mode		   = (FeedLoaderMode)getArguments().getSerializable(ARG_MODE);

		feedLoader = new BufferedFeedLoader<Post>(getActivity(), requestManager, selectedProviders);
		feedLoader.setMode(mode);

		configurationActionBar(getSupportActionBar());
	}

	void configurationActionBar(ActionBar actionBar) {
		List<String> socialNetworkTitles  = new ArrayList<String>();
		socialNetworkTitles.add(getString(R.string.app_name));
		for(SocialNetworkProviderInterface p : providers) {
			socialNetworkTitles.add(StringUtils.capitalize(p.getCode().toLowerCase()));
		}

		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		actionBar.setListNavigationCallbacks(new FeedFragmentNavigationAdapter(getActivity(), title, socialNetworkTitles), this);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.feed, menu);
	}

	@Override
	public void onFeedItemsLoaded(List<Post> postList) {
		if(needClearPosts) {
			mPosts.clear();
			needClearPosts = false;
		}
		mPosts.addAll(postList);
		completeRequest();
	}

	@Override
	public void onError(CommonSocialNetworkException exception) {
		setEmptyText("Данные отсутствуют");
		completeRequest();
	}

	@Override
	public void onConnectionError() {
		setEmptyText("Отсутствует соединение с интернетом");
		completeRequest();
	}

	private void completeRequest() {
		if(this.adapter == null) {
			this.adapter = new FeedListViewAdapter(getActivity(), resourcesManager, mPosts);
			this.adapter.setPostActionsListener(this);
			setListAdapter(this.adapter);
		} else {
			setListShown(true);
			this.adapter.notifyDataSetChanged();
		}

		loadingFooter.setVisibility(View.GONE);

		if(mPullToRefreshLayout.isRefreshing()) {
			mPullToRefreshLayout.setRefreshComplete();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_compose:
				startActivity(new Intent(getActivity(), AddPostActivity.class));
				return true;
		}
		return false;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setEmptyText("Данные отсутствуют");

		ViewGroup viewGroup = (ViewGroup) view;
		mPullToRefreshLayout = new PullToRefreshLayout(viewGroup.getContext());
		ActionBarPullToRefresh.from(getActivity())
				.insertLayoutInto(viewGroup)
				.theseChildrenArePullable(getListView(), getListView().getEmptyView())
				.listener(this)
				.useViewDelegate(TextView.class, new ViewDelegate() {
					@Override
					public boolean isReadyForPull(View view, float v, float v2) {
						return Boolean.TRUE;
					}
				})
				.setup(mPullToRefreshLayout);

		configurationListView(getListView());
	}

	private void configurationListView(ListView listView) {
		((TextView)listView.getEmptyView()).setTextSize(16);

		listView.setBackgroundResource(R.color.base_gray);
		listView.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
		listView.setDivider(new ColorDrawable(Color.TRANSPARENT));
		listView.setDividerHeight(MetricsConverter.dp2px(getActivity(), 12));
		listView.setDrawSelectorOnTop(true);

		int padding = MetricsConverter.dp2px(getActivity(), 10);
		int headerViewHeight = MetricsConverter.dp2px(getActivity(), 2);

		TextView headerView = new TextView(getActivity());
		headerView.setHeight(headerViewHeight);

		listView.addHeaderView(headerView);
		listView.setPadding(padding, 0, padding, 0);
		listView.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), false, true, this));
		loadingFooter  = View.inflate(getActivity(), R.layout.lv_pull_more_loader, null);
		loadingFooter.setVisibility(View.GONE);
		listView.addFooterView(loadingFooter);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent(getActivity(), PostActivity.class);
		intent.putExtra(PostActivity.EXTRA_POST,  adapter.getItem(position - getListView().getHeaderViewsCount()));
		startActivity(intent);
	}

	@Override
	public void onScrollStateChanged(AbsListView absListView, int i) {}

	protected void pullMore() {
		if(app().isOnline()) {
			loadingFooter.setVisibility(View.VISIBLE);
			feedLoader.next();
		} else {
			onNonInternetConnection();
		}
	}

	@Override
	public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		//Получаем последний видимый элемент
		if (visibleItemCount > 0 && firstVisibleItem + visibleItemCount == totalItemCount && feedLoader.hasNext()) {
			pullMore();
		}
	}

	@Override
	public void onRefreshStarted(View view) {
		refresh();
	}

	private void refresh() {
		if(selectedProviders.isEmpty()) {
			setEmptyText("Необходимо добавить аккаунты соц. сетей");
			setListAdapter(this.adapter = null);
			setListShown(true);
			if(mPullToRefreshLayout.isRefreshing()) {
				mPullToRefreshLayout.setRefreshComplete();
			}
		} else {
			needClearPosts = true;
			feedLoader.start();
		}
	}

	private void onNonInternetConnection() {
		setEmptyText("Отсутствует соединение с интернетом");
		setListAdapter(this.adapter = null);
		setListShown(true);
		if(mPullToRefreshLayout.isRefreshing()) {
			mPullToRefreshLayout.setRefreshComplete();
		}
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		selectedProviders.clear();
		if(itemPosition == 0) {
			selectedProviders.addAll(providers);
		} else {
			selectedProviders.add(providers.get(itemPosition-1));
		}
		refresh(100);
		return true;
	}

	public void refresh(final int timeout) {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				try {
					Thread.sleep(timeout);
				} catch (InterruptedException iex) {/*pass*/}
				return null;
			}

			@Override
			protected void onPostExecute(Void aVoid) {
				setListShown(false);
				refresh();
			}
		}.execute();
	}

	@Override
	public PostAction createFaveAction() {
		return new FaveAction(getActivity(), requestManager, new PostAction.ActionCallback<Void>() {
			@Override
			public void success(Post post, Void pass) {
				adapter.notifyDataSetChanged();
			}

			@Override
			public void error(Post post, String error) {
				adapter.notifyDataSetChanged();
			}
		});
	}

	public PostAction createCommentAction() {
		return new DialogCommentAction(getActivity(), requestManager, new PostAction.ActionCallback<Comment>() {
			@Override
			public void success(Post post, Comment result) {
				adapter.notifyDataSetChanged();
			}

			@Override
			public void error(Post post, String error) {}
		});
	}

	public PostAction createRepostAction() {
		return new RepostAction(getSupportActivity(), socialNetworksManager, requestManager, new PostAction.ActionCallback<Void>() {
			@Override
			public void success(Post post, Void result) {
				//To change body of implemented methods use File | Settings | File Templates.
			}

			@Override
			public void error(Post post, String error) {
				//To change body of implemented methods use File | Settings | File Templates.
			}
		});
	}

	@Override
	public PostAction createUnFaveAction() {
		return new UnFaveAction(getActivity(), requestManager, new PostAction.ActionCallback<Void>() {
			@Override
			public void success(Post post, Void result) {
				adapter.notifyDataSetChanged();
			}

			@Override
			public void error(Post post, String error) {
				adapter.notifyDataSetChanged();
				Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
			}
		});
	}
}
