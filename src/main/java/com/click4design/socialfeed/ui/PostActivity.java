package com.click4design.socialfeed.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.click4design.core.ui.CommonActivity;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Comment;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.feed.FeedLoader;
import com.click4design.socialfeed.feed.comment.CommentLoader;
import com.click4design.socialfeed.network.resource.Resources;
import com.click4design.socialfeed.network.resource.ResourcesManager;
import com.click4design.socialfeed.ui.actions.CommentAction;
import com.click4design.socialfeed.ui.actions.FaveAction;
import com.click4design.socialfeed.ui.actions.PostAction;
import com.click4design.socialfeed.ui.actions.UnFaveAction;
import com.click4design.socialfeed.utils.Keyboard;
import com.click4design.socialfeed.utils.MetricsConverter;
import com.google.inject.Inject;
import com.jake.quiltview.QuiltView;
import com.nostra13.universalimageloader.core.ImageLoader;
import org.holoeverywhere.widget.*;
import roboguice.inject.ContentView;
import roboguice.inject.InjectExtra;
import roboguice.inject.InjectView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 08.01.14
 * Time: 23:10
 */
@ContentView(R.layout.activity_post)
public class PostActivity extends CommonActivity implements FeedLoader.FeedLoaderListener<Comment> {
    public static final String EXTRA_POST = "post";

	private List<Comment> comments = new ArrayList<Comment>();

	private CommentLoader commentsLoader;

	private CommentsListAdapter commentsListAdapter;

	private ImageLoader imageLoader;

	private TextView loadMore;

    @InjectExtra(EXTRA_POST)
    protected Post mPost;

	@InjectView(R.id.comments_list)
	protected ListView mListView;

	@InjectView(R.id.add_comment_layout)
	protected RelativeLayout mCommentLayout;

	@InjectView(R.id.push_comment)
	protected ImageButton pushComment;

	@InjectView(R.id.comment_text)
	protected EditText commentInput;

    protected ImageView avatar;

    protected ImageView socNetwork;

    protected TextView author;

    protected TextView date;

    protected TextView text;

    protected QuiltView photos;

	protected LinearLayout mPostView;
	TextView commentsCount;
	TextView repostsCount;
	TextView likesCount;
	LinearLayout fave;
	LinearLayout repost;
	LinearLayout comment;
	LinearLayout postActions;
	ImageView faveIcon;

	@Inject
	protected CommonRequestManager requestManager;
	@Inject
	protected ResourcesManager mResourcesManager;

	protected View.OnClickListener photoOnClickHandler = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(PostActivity.this, PhotoViewerActivity.class);
			intent.putExtra(PhotoViewerActivity.EXTRA_PHOTO_URLS, new ArrayList<String>(mPost.getPhotoUrls()));
			intent.putExtra(PhotoViewerActivity.EXTRA_CURRENT_ITEM, v.getId());
			startActivity(intent);
		}
	};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		configurationActionBar(getSupportActionBar());
		commentsLoader = new CommentLoader(this, requestManager, mPost);
		commentsLoader.setFeedLoaderListener(this);

       	imageLoader = ImageLoader.getInstance();

		mPostView = (LinearLayout)getLayoutInflater().inflate(R.layout.lv_news_item);
		avatar 		= (ImageView)mPostView.findViewById(R.id.avatar);
		socNetwork  = (ImageView)mPostView.findViewById(R.id.network_icon);
		author		= (TextView)mPostView.findViewById(R.id.author);
		date		= (TextView)mPostView.findViewById(R.id.date);
		text		= (TextView)mPostView.findViewById(R.id.text);
		photos		= (QuiltView)mPostView.findViewById(R.id.photos);

		commentsCount = (TextView)mPostView.findViewById(R.id.comments_count);
		likesCount = (TextView)mPostView.findViewById(R.id.likes_count);
		repostsCount  = (TextView)mPostView.findViewById(R.id.reposts_count);
		postActions = (LinearLayout)mPostView.findViewById(R.id.post_action_layout);
		fave = (LinearLayout)mPostView.findViewById(R.id.fave);
		comment = (LinearLayout)mPostView.findViewById(R.id.add_comment);
		repost = (LinearLayout)mPostView.findViewById(R.id.repost);
		faveIcon = (ImageView)mPostView.findViewById(R.id.fave_icon);

		imageLoader.displayImage(mPost.getAuthor().getAvatarUrl(), avatar);
		if(mPost.hasPhotos()) {
			ArrayList<ImageView> images = new ArrayList<ImageView>();
			for(int i = 0; i < mPost.getPhotoUrls().size(); i++) {
				String photoUrl = mPost.getPhotoUrls().get(i);
				imageLoader.displayImage(mPost.getAuthor().getAvatarUrl(), avatar);
				final ImageView image = new ImageView(this);
				image.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
				image.setId(i);
				image.setScaleType(ImageView.ScaleType.CENTER_CROP);
				image.setOnClickListener(photoOnClickHandler);
				imageLoader.displayImage(photoUrl, image);
				images.add(image);
			}
			photos.addPatchImages(images);
		}
		prepareView(mPost);


		ListView.LayoutParams lP = new ListView.LayoutParams(
				ListView.LayoutParams.MATCH_PARENT, ListView.LayoutParams.WRAP_CONTENT);
		loadMore = new TextView(this);
		loadMore.setLayoutParams(lP);
		loadMore.setGravity(Gravity.CENTER);
		int padd = MetricsConverter.dp2px(this, 12);
		loadMore.setPadding(padd, padd, padd, padd);
		loadMore.setText("Загрузить ещё");
		loadMore.setVisibility(View.VISIBLE);
		loadMore.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				loadComments(false);
			}
		});

		mListView.addHeaderView(mPostView);
		commentsListAdapter = new CommentsListAdapter(this, comments);
		mListView.setAdapter(commentsListAdapter);

		if(mPost.canComment()) {
			pushComment.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					addComment(commentInput.getText().toString());
					commentInput.getText().clear();
					Keyboard.hide(PostActivity.this);
					commentInput.clearFocus();
				}
			});
			loadComments(true);
		} else {
			mCommentLayout.setVisibility(View.GONE);
		}

	}

	protected void prepareView(final Post post) {
		Resources res = mResourcesManager.provide(post.getProvider());

		if(post.canComment() || !post.canFave() || post.canRepost()) {
			postActions.setVisibility(View.VISIBLE);
			if(post.canComment()) {
				comment.setVisibility(View.VISIBLE);
				comment.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						commentInput.setFocusableInTouchMode(true);
						commentInput.requestFocus();
						final InputMethodManager inputMethodManager = (InputMethodManager) PostActivity.this
								.getSystemService(Context.INPUT_METHOD_SERVICE);
						inputMethodManager.showSoftInput(commentInput, InputMethodManager.SHOW_IMPLICIT);
					}
				});
			} else {
				comment.setVisibility(View.GONE);
			}

			if(post.isFaved()) {
				faveIcon.setImageResource(res.getFavedIcon());
			} else {
				faveIcon.setImageResource(res.getFaveIcon());
			}

			if(post.canFave()) {
				fave.setVisibility(View.VISIBLE);
				fave.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if(post.isFaved()) {
							unFave(post);
						} else {
							fave(post);
						}
					}
				});
			} else {
				fave.setVisibility(View.GONE);
			}


			if(post.canRepost()) {
				repost.setVisibility(View.VISIBLE);
				repost.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						//dispatchOnRepost(holder, post);
					}
				});
			} else {
				repost.setVisibility(View.GONE);
			}

		} else {
			postActions.setVisibility(View.GONE);
		}


		socNetwork.setImageResource(post.getProvider().getIconRes());
		author.setText(post.getAuthor().getFullName());

		commentsCount.setText(post.getCommentsCount() != -1 ? Integer.toString(post.getCommentsCount()) : "");
		likesCount.setText(post.getFavesCount() != -1 ? Integer.toString(post.getFavesCount()) : "");
		repostsCount.setText(post.getRepostsCount() != -1 ?Integer.toString(post.getRepostsCount()) : "");

		if(TextUtils.isEmpty(post.getText())) {
			text.setVisibility(View.GONE);
		} else {
			text.setMaxLines(Integer.MAX_VALUE);
			text.setText(post.getText());
			text.setVisibility(View.VISIBLE);
		}
	}

	private void configurationActionBar(ActionBar actionBar) {
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("Просмотр записи");
		actionBar.setDisplayUseLogoEnabled(false);
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

	private void loadComments(boolean start) {
		loadMore.setText("Загрузка...");

		mListView.removeHeaderView(loadMore);
		mListView.addHeaderView(loadMore);
		if(start) {
			if(!comments.isEmpty()) {
				comments.clear();
			}
			commentsLoader.start();
		} else {
			commentsLoader.next();
		}
	}

	@Override
	public void onFeedItemsLoaded(List<Comment> postList) {
		Collections.reverse(postList);
		postList.addAll(new ArrayList<Comment>(comments));
		comments.clear();
		comments.addAll(postList);

		commentsListAdapter.notifyDataSetChanged();

		mListView.removeHeaderView(loadMore);
		if(commentsLoader.hasNext()) {
			loadMore.setText("Загрузить ещё");
			mListView.addHeaderView(loadMore);
		}
	}

	@Override
	public void onError(CommonSocialNetworkException exception) {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void onConnectionError() {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	private void addComment(String text) {
		new CommentAction(this, requestManager, text, new PostAction.ActionCallback<Comment>() {
			@Override
			public void success(Post post, Comment comment) {
				prepareView(post);
				if(comment != null) {
					comments.add(comment);
					commentsListAdapter.notifyDataSetChanged();
				} else {
					loadComments(true);
				}
				scrollToBottom();
			}

			@Override
			public void error(Post post, String error) {
				//To change body of implemented methods use File | Settings | File Templates.
			}
		}).doAction(mPost);
	}

	private void fave(Post post) {
		new FaveAction(this, requestManager, new PostAction.ActionCallback<Void>() {
			@Override
			public void success(Post post, Void result) {
				prepareView(post);
			}

			@Override
			public void error(Post post, String error) {
				prepareView(post);
			}
		}).doAction(post);
	}

	private void unFave(Post post) {
		new UnFaveAction(this, requestManager, new PostAction.ActionCallback<Void>() {
			@Override
			public void success(Post post, Void result) {
				prepareView(post);
			}

			@Override
			public void error(Post post, String error) {
				prepareView(post);
			}
		}).doAction(post);
	}

	private void scrollToBottom() {
		mListView.post(new Runnable() {
			@Override
			public void run() {
				// Select the last row so it will scroll into view...
				mListView.setSelection(commentsListAdapter.getCount() - 1);
			}
		});
	}
}
