package com.click4design.socialfeed.ui.actions;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.CommonResponseHandler;
import com.click4design.socialfeed.data.IRequestListener;
import com.click4design.socialfeed.data.RequestFactory;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Comment;
import com.click4design.socialfeed.data.model.Post;
import com.foxykeep.datadroid.requestmanager.Request;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.Toast;

/**
 * Author: Khamidullin Kamil
 * Date: 10.03.14
 * Time: 18:45
 */
public class CommentAction implements PostAction, IRequestListener {
	private Activity activity;
	private CommonRequestManager requestManager;
	private ActionCallback<Comment> callback;
	private Post post;
	private String text;
	private ProgressDialog progress;
	private boolean isProgress = false;
	private CommonResponseHandler responseHandler;

	public CommentAction(Activity context, CommonRequestManager requestManager, String text, ActionCallback callback) {
		this.activity = context;
		this.requestManager = requestManager;
		this.callback = callback;
		this.text = text;
		this.responseHandler = new CommonResponseHandler(context, this);
	}

	public CommentAction(Activity context, CommonRequestManager requestManager, ActionCallback callback) {
		this.activity = context;
		this.requestManager = requestManager;
		this.callback = callback;
		this.text = "";
		this.responseHandler = new CommonResponseHandler(context, this);
	}

	public void setText(String text) {
		this.text = text;
	}

	public Request createRequest(Post post, String text) {
		Request request = new Request(RequestFactory.REQUEST_ADD_COMMENT);
		request.put("provider", post.getProvider());
		Bundle params = new Bundle();
		params.putString("post_id", post.getId());
		params.putString("username", post.getAuthor().getUsername());
		params.putString("author_id", post.getAuthor().getId());
		params.putString("text", text);
		request.put("params", params);

		return request;
	}

	@Override
	public void doAction(Post post) {
		this.post = post;
		if(!isProgress) {
			if(!TextUtils.isEmpty(text)) {
				requestManager.execute(createRequest(post, text),
						responseHandler
				);
			} else {
				Toast.makeText(activity, "Текст комментария не может быть пустым", Toast.LENGTH_SHORT).show();
			}
		}
	}


	@Override
	public void success(Request request, Bundle resultData) {
		post.setCommentsCount(post.getCommentsCount()+1);
		callback.success(post, resultData.containsKey("comment") ? (Comment)resultData.getParcelable("comment") : null);
	}

	@Override
	public void error(CommonSocialNetworkException exception, Request request) {
		callback.error(post, exception.getMessage());
	}

	@Override
	public void start(Request request) {
		isProgress = true;

		progress = new ProgressDialog(activity);
		progress.setMessage("Подождите");
		progress.setCancelable(false);
		progress.show();
	}

	@Override
	public void finish(Request request) {
		isProgress = false;

		if(progress != null) {
			progress.dismiss();
			progress = null;
		}
	}
}
