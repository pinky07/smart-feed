package com.click4design.socialfeed.ui.actions;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.CommonResponseHandler;
import com.click4design.socialfeed.data.IRequestListener;
import com.click4design.socialfeed.data.RequestFactory;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Post;
import com.foxykeep.datadroid.requestmanager.Request;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.Toast;

/**
 * Author: Khamidullin Kamil
 * Date: 10.03.14
 * Time: 1:09
 */
public class DialogCommentAction extends CommentAction{
	private Activity activity;

	public DialogCommentAction(Activity context, CommonRequestManager requestManager, ActionCallback callback) {
		super(context, requestManager, callback);
		this.activity = context;
	}

	@Override
	public void doAction(final Post post) {
		final EditText input = new EditText(activity);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT);
		input.setGravity(Gravity.TOP);
		input.setHint("Текст комментария...");
		input.setMinLines(2);
		input.setLayoutParams(lp);

		new AlertDialog.Builder(activity)
				.setTitle("Новый комментарий")
				.setView(input)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String text = input.getText().toString();
						setText(text);
						DialogCommentAction.super.doAction(post);
					}
				})
				.setNegativeButton(android.R.string.cancel, null)
				.create()
				.show();
	}
}
