package com.click4design.socialfeed.ui.actions;

import android.app.AlertDialog;
import android.content.Context;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.SocialNetworksManager;
import com.click4design.socialfeed.ui.MultiSelectSocialNetworkDialogFragment;
import org.holoeverywhere.app.Activity;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 10.03.14
 * Time: 11:51
 */
public class RepostAction implements PostAction, MultiSelectSocialNetworkDialogFragment.OnSelectSocialNetworksListener {
	private Activity activity;
	private CommonRequestManager requestManager;
	private Post post;
	private ActionCallback<Void> callback;
	protected SocialNetworksManager socialNetworksManager;

	public RepostAction(Activity context, SocialNetworksManager socialNetworksManager, CommonRequestManager requestManager, ActionCallback callback) {
		this.activity = context;
		this.socialNetworksManager = socialNetworksManager;
		this.requestManager = requestManager;
		this.callback = callback;
	}

	@Override
	public void doAction(Post post) {
		this.post = post;
		List<SocialNetworkProviderInterface> networks = new ArrayList<SocialNetworkProviderInterface>(socialNetworksManager.getAuthorizedSocialNetworks());
		networks.remove(SocialNetworkProvider.INSTAGRAM);

		MultiSelectSocialNetworkDialogFragment fragment = MultiSelectSocialNetworkDialogFragment.newInstance(
				networks,
				new ArrayList<SocialNetworkProviderInterface>()
		);
		fragment.setTitle("Репост сообщения");
		fragment.setOnSelectSocialNetworksListener(this);
		fragment.show(activity.getSupportFragmentManager());
	}

	@Override
	public void onSelectSocialNetworks(List<SocialNetworkProviderInterface> socialNetworks) {
		//To change body of implemented methods use File | Settings | File Templates.
	}
}
