package com.click4design.socialfeed.ui.actions;

import android.content.Context;
import android.os.Bundle;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.CommonResponseHandler;
import com.click4design.socialfeed.data.IRequestListener;
import com.click4design.socialfeed.data.RequestFactory;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Post;
import com.foxykeep.datadroid.requestmanager.Request;

/**
 * Author: Khamidullin Kamil
 * Date: 09.03.14
 * Time: 22:44
 */
public class FaveAction implements PostAction, IRequestListener {
	private Context context;
	private CommonRequestManager requestManager;
	private ActionCallback<Void> callback;
	private Post post;
	private boolean isFaving;

	public FaveAction(Context context, CommonRequestManager requestManager, ActionCallback callback) {
		this.context = context;
		this.requestManager = requestManager;
		this.callback = callback;
	}

	public Request createRequest(Post post) {
		Request request = new Request(RequestFactory.REQUEST_FAVE);
		request.put("provider", post.getProvider());
		Bundle params = new Bundle();
		params.putParcelable("post", post);
		request.put("params", params);

		return request;
	}

	@Override
	public void doAction(Post post) {
		this.post = post;
		if(!isFaving) {
			requestManager.execute(createRequest(post), new CommonResponseHandler(context, this));
		}

		post.doFave();
	}

	@Override
	public void success(Request request, Bundle resultData) {
		if(callback != null) {
			callback.success(post, null);
		}
	}

	@Override
	public void error(CommonSocialNetworkException exception, Request request) {
		post.doUnFave();
		if(callback != null) {
			callback.error(post, exception.getMessage());
		}
	}

	@Override
	public void start(Request request) {
		this.isFaving = true;
	}

	@Override
	public void finish(Request request) {
		this.isFaving = false;
	}
}
