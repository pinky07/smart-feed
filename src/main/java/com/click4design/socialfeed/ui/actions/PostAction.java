package com.click4design.socialfeed.ui.actions;

import com.click4design.socialfeed.data.model.Post;
import com.foxykeep.datadroid.requestmanager.Request;
import com.foxykeep.datadroid.requestmanager.RequestManager;

/**
 * Author: Khamidullin Kamil
 * Date: 09.03.14
 * Time: 22:51
 */
public interface PostAction {
	public void doAction(Post post);

	public interface ActionCallback<T> {
		public void success(Post post, T result);

		public void error(Post post, String error);
	}
}
