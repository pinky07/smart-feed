package com.click4design.socialfeed.ui.actions;

import android.content.Context;
import android.os.Bundle;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.CommonResponseHandler;
import com.click4design.socialfeed.data.IRequestListener;
import com.click4design.socialfeed.data.RequestFactory;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Post;
import com.foxykeep.datadroid.requestmanager.Request;

/**
 * Author: Khamidullin Kamil
 * Date: 10.03.14
 * Time: 0:50
 */
public class UnFaveAction implements PostAction, IRequestListener {
	private Context context;
	private CommonRequestManager requestManager;
	private ActionCallback<Void> callback;
	private Post post;
	private boolean isUnFaving;

	public UnFaveAction(Context context, CommonRequestManager requestManager, ActionCallback callback) {
		this.context = context;
		this.requestManager = requestManager;
		this.callback = callback;
	}

	public Request createRequest(Post post) {
		Request request = new Request(RequestFactory.REQUEST_UNFAVE);
		request.put("provider", post.getProvider());
		Bundle params = new Bundle();
		params.putParcelable("post", post);
		request.put("params", params);

		return request;
	}

	@Override
	public void doAction(Post post) {
		this.post = post;
		if(!isUnFaving) {
			requestManager.execute(createRequest(post), new CommonResponseHandler(context, this));
		}
		post.doUnFave();
	}

	@Override
	public void success(Request request, Bundle resultData) {
		if(callback != null) {
			callback.success(post, null);
		}
	}

	@Override
	public void error(CommonSocialNetworkException exception, Request request) {
		post.doFave();
		if(callback != null) {
			callback.error(post, exception.getMessage());
		}
	}

	@Override
	public void start(Request request) {
		isUnFaving = true;
	}

	@Override
	public void finish(Request request) {
		isUnFaving = false;
	}
}
