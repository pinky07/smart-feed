package com.click4design.socialfeed.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import org.holoeverywhere.widget.ViewPager;

/**
 * Author: Khamidullin Kamil
 * Date: 13.02.14
 * Time: 4:04
 */
public class HackyViewPager extends ViewPager {
	public HackyViewPager(Context context) {
		super(context);
	}

	public HackyViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		try {
			return super.onInterceptTouchEvent(ev);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return false;
		}
	}
}
