package com.click4design.socialfeed.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.data.model.Comment;
import com.click4design.socialfeed.data.model.User;
import com.nostra13.universalimageloader.core.ImageLoader;
import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.widget.TextView;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 23.02.14
 * Time: 23:49
 */
public class CommentsListAdapter extends BaseAdapter {
	private List<Comment> comments;
	private LayoutInflater inflater;
	private ImageLoader imageLoader;
	private DateTimeFormatter dateFormat = DateTimeFormat.forPattern("dd MMM HH:mm:ss");

	public CommentsListAdapter(Context context, List<Comment> comments) {
		this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.comments = comments;
		this.imageLoader = ImageLoader.getInstance();
	}


	@Override
	public int getCount() {
		return comments.size();
	}

	@Override
	public Comment getItem(int position) {
		return comments.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null) {
			convertView = inflater.inflate(R.layout.comment_item);
		}

		Comment comment = getItem(position);

		ImageView avatar = (ImageView)convertView.findViewById(R.id.author_avatar);
		TextView text = (TextView)convertView.findViewById(R.id.text);
		TextView author = (TextView)convertView.findViewById(R.id.author_name);
		TextView date = (TextView)convertView.findViewById(R.id.date);

		User user = comment.getAuthor();
		if(user != null)  {
			imageLoader.displayImage(comment.getAuthor().getAvatarUrl(), avatar);
			author.setText(comment.getAuthor().getFullName());
		}

		text.setText(comment.getText());
		date.setText(dateFormat.print(new DateTime(comment.getDate() * 1000)));

		return convertView;
	}
}
