package com.click4design.socialfeed.ui;

import android.R;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import com.click4design.roboholo.RoboHoloDialogFragment;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 01.02.14
 * Time: 12:21
 */
public class MultiSelectSocialNetworkDialogFragment extends RoboHoloDialogFragment implements DialogInterface.OnMultiChoiceClickListener {
    List<SocialNetworkProviderInterface> socialNetworks = new ArrayList<SocialNetworkProviderInterface>();
    boolean [] checkboxStates;
    String[] titles;
    OnSelectSocialNetworksListener listener;

    public static final String ARG_SOC_NETWORKS = "soc_networks";
    public static final String ARG_SELECTED_SOC_NETWORKS = "selected_soc_networks";

    public static MultiSelectSocialNetworkDialogFragment newInstance(List<SocialNetworkProviderInterface> socialNetworks, List<SocialNetworkProviderInterface> selectedSocialNetworks) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_SOC_NETWORKS, new ArrayList<Parcelable>(socialNetworks));
        args.putParcelableArrayList(ARG_SELECTED_SOC_NETWORKS, new ArrayList<Parcelable>(selectedSocialNetworks));
        MultiSelectSocialNetworkDialogFragment fragment = new MultiSelectSocialNetworkDialogFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<Parcelable> socNetwParcelables = getArguments().getParcelableArrayList(ARG_SOC_NETWORKS);
        List<Parcelable> selSocNetwParcelables = getArguments().getParcelableArrayList(ARG_SELECTED_SOC_NETWORKS);
        checkboxStates = new boolean[socNetwParcelables.size()];
        titles         = new String[socNetwParcelables.size()];
        for(int i = 0; i < socNetwParcelables.size(); i++) {
            socialNetworks.add((SocialNetworkProviderInterface)socNetwParcelables.get(i));
            checkboxStates[i]   = selSocNetwParcelables.contains(socNetwParcelables.get(i));
            titles[i] = socialNetworks.get(i).getCode().toUpperCase();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setMultiChoiceItems(titles, checkboxStates, this)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        List<SocialNetworkProviderInterface> socNetworks = new ArrayList<SocialNetworkProviderInterface>();
                        for(int j = 0; j < checkboxStates.length; j++) {
                            if(checkboxStates[j]) {
                                socNetworks.add(socialNetworks.get(j));
                            }
                        }

                        if(listener != null) {
                            listener.onSelectSocialNetworks(socNetworks);
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
        checkboxStates[which]  = isChecked;
    }

    public void setOnSelectSocialNetworksListener(OnSelectSocialNetworksListener listener) {
        this.listener = listener;
    }

    public interface OnSelectSocialNetworksListener {
        public void onSelectSocialNetworks(List<SocialNetworkProviderInterface> socialNetworks);
    }
}
