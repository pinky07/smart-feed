package com.click4design.socialfeed.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import com.click4design.core.ui.CommonListFragment;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.SocialFeedApplication;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.SocialNetworksManager;
import com.click4design.socialfeed.network.account.Account;
import com.click4design.socialfeed.network.account.AccountStorage;
import com.click4design.socialfeed.network.auth.AuthDialog;
import com.click4design.socialfeed.network.auth.AuthListener;
import com.google.inject.Inject;
import org.holoeverywhere.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 28.06.13
 * Time: 18:59
 */
public class AccountsFragment extends CommonListFragment implements AuthListener {

    @Inject
    AccountStorage accountStorage;
	@Inject
	SocialNetworksManager socialNetworksManager;

	ArrayAdapter<SocialNetworkProviderInterface> adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
		getSupportActivity().getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActivity().getSupportActionBar().setTitle("Аккаунты");
		setEmptyText("Не добавлено ни одного аккаунта");
		adapter = new ArrayAdapter<SocialNetworkProviderInterface>(getActivity(), android.R.layout.simple_list_item_1, socialNetworksManager.getAuthorizedSocialNetworks());
        setListAdapter(adapter);
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
				showAcceptRemoveDialog(socialNetworksManager.getAuthorizedSocialNetworks().get(position));
				return true;
			}
		});
    }

    @Override
    public void onAuthenticated(Account account, SocialNetworkProviderInterface provider) {
        accountStorage.save(provider, account);
		adapter.notifyDataSetChanged();
    }

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.accounts, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_add:
				showSocialNetworksDialog();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void showAcceptRemoveDialog(final SocialNetworkProviderInterface provider) {
		new AlertDialog.Builder(getSupportActivity())
				.setMessage("Вы уверены что хотите удалить этот аккаунт?")
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						accountStorage.remove(provider);
						adapter.notifyDataSetChanged();
					}
				})
				.setNegativeButton(android.R.string.no, null)
				.create()
				.show();
	}

	public void showSocialNetworksDialog() {
		final ArrayAdapter<SocialNetworkProviderInterface> arrayAdapter = new ArrayAdapter<SocialNetworkProviderInterface>(
				getActivity(), R.layout.simple_list_item_1, socialNetworksManager.getNotAuthorizedSocialNetworks());
		new AlertDialog.Builder(getSupportActivity())
				.setTitle("Выберите соц. сеть")
				//.setNegativeButton("cancel", null)
				.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						AuthDialog authFragment =  arrayAdapter.getItem(which).createAuthFragment();
						authFragment.show(getChildFragmentManager(), "frag", AccountsFragment.this);
					}
				})
				.create()
				.show();
	}
}
