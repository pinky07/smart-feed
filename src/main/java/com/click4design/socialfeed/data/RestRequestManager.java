package com.click4design.socialfeed.data;

import android.content.Context;
import com.click4design.socialfeed.data.service.RestService;
import com.foxykeep.datadroid.requestmanager.RequestManager;

/**
 * Author: Khamidullin Kamil
 * Date: 22.09.13
 * Time: 1:30
 */
public class RestRequestManager extends RequestManager {
    private RestRequestManager(Context context) {
        super(context, RestService.class);
    }

    private static RestRequestManager sInstance;

    public static RestRequestManager from(Context context) {
        if (sInstance == null) {
            sInstance = new RestRequestManager(context);
        }
        return sInstance;
    }
}