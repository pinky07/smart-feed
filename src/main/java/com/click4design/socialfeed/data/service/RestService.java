package com.click4design.socialfeed.data.service;

import android.os.Bundle;
import com.click4design.socialfeed.data.RequestFactory;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.SocialNetworkError;
import com.click4design.socialfeed.data.operation.*;
import com.foxykeep.datadroid.exception.CustomRequestException;
import com.foxykeep.datadroid.requestmanager.Request;
import com.foxykeep.datadroid.service.RequestService;

/**
 * Author: Khamidullin Kamil
 * Date: 22.09.13
 * Time: 1:31
 */
public class RestService extends RequestService {

    @Override
    public Operation getOperationForType(int requestType) {
        switch (requestType) {
            case RequestFactory.REQUEST_NEWS:
                return new LoadNewsFeedOperation();
            case RequestFactory.REQUEST_PHOTOS:
                return new LoadPhotosFeedOperation();
            case RequestFactory.REQUEST_ADD_POST:
                return new AddPostOperation();
			case RequestFactory.REQUEST_FAVORITES:
				return new LoadFavoritePostsOperation();
			case RequestFactory.REQUEST_FAVE:
				return new FaveOperation();
			case RequestFactory.REQUEST_UNFAVE:
				return new UnFaveOperation();
			case RequestFactory.REQUEST_MY_FEED:
				return new LoadMyPostsOperation();
			case RequestFactory.REQUEST_COMMENTS:
				return new LoadCommentsOperation();
			case RequestFactory.REQUEST_ADD_COMMENT:
				return new AddCommentOperation();
            default:
                return null;
        }
    }

    @Override
    protected int getMaximumNumberOfThreads() {
        return 3;
    }

    @Override
    protected Bundle onCustomRequestException(Request request, CustomRequestException exception) {
        Bundle data = new Bundle();
        data.putSerializable("exception", exception);

        return data;
    }
}