package com.click4design.socialfeed.data.service;

import com.foxykeep.datadroid.requestmanager.Request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 13.02.14
 * Time: 16:41
 */
public class BatchRequest {
    ArrayList<Request> requests = new ArrayList<Request>();

    public BatchRequest(Request... requests) {
        this.requests.addAll(Arrays.asList(requests));
    }

    public BatchRequest() {}

    public void add(Request request) {
        this.requests.add(request);
    }

    public ArrayList<Request> getRequests() {
        return requests;
    }
}
