package com.click4design.socialfeed.data.operation;

import android.content.Context;
import android.os.Bundle;
import com.click4design.socialfeed.SocialFeedApplication;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.exception.InternalExceptionFactory;
import com.click4design.socialfeed.network.error.adapter.InstagramExceptionAdapter;
import com.click4design.socialfeed.network.error.adapter.TwitterExceptionAdapter;
import com.click4design.socialfeed.network.error.adapter.VKExceptionAdapter;
import com.foxykeep.datadroid.exception.ConnectionException;
import com.foxykeep.datadroid.exception.CustomRequestException;
import com.foxykeep.datadroid.exception.DataException;
import com.foxykeep.datadroid.requestmanager.Request;
import com.foxykeep.datadroid.service.RequestService.*;
import com.perm.kate.api.KException;
import retrofit.RetrofitError;
import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;
import twitter4j.TwitterException;

/**
 * Author: Khamidullin Kamil
 * Date: 23.09.13
 * Time: 17:20
 */
abstract public class RoboOperation implements Operation {

    abstract public Bundle exec(Context context, Request request) throws Exception;

    @Override
    final public Bundle execute(Context context, Request request) throws CommonSocialNetworkException {
        final RoboInjector injector = RoboGuice.getInjector(context);
        injector.injectMembersWithoutViews(this);
        try {
            return exec(context, request);
        } catch (Exception ex) {
            throw InternalExceptionFactory.reThrow(ex);
        }
    }
}
