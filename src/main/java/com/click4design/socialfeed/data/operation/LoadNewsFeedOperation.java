package com.click4design.socialfeed.data.operation;

import android.content.Context;
import android.os.Bundle;
import com.click4design.socialfeed.data.model.Feed;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.api.*;
import com.foxykeep.datadroid.requestmanager.Request;
import com.google.inject.Inject;

/**
 * Author: Khamidullin Kamil
 * Date: 22.09.13
 * Time: 1:32
 */
public class LoadNewsFeedOperation extends RoboOperation {
    @Inject
    ApiClientFactoryInterface mApiFactory;

    @Override
    public Bundle exec(Context context, Request request) throws Exception {
        SocialNetworkProvider provider = (SocialNetworkProvider) request.getParcelable("provider");
        Bundle params = (Bundle)request.getParcelable("params");
        ApiClientInterface client = mApiFactory.getInstance(provider);

        Feed news = client.getFeed(params);
        Bundle bundle = new Bundle();
        bundle.putParcelable("news", news);
        bundle.putParcelable("provider", provider);

        return bundle;
    }
}
