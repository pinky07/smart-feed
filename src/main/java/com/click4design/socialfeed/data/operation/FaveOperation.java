package com.click4design.socialfeed.data.operation;

import android.content.Context;
import android.os.Bundle;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.api.ApiClientFactoryInterface;
import com.click4design.socialfeed.network.api.ApiClientInterface;
import com.foxykeep.datadroid.requestmanager.Request;
import com.google.inject.Inject;

/**
 * Author: Khamidullin Kamil
 * Date: 20.02.14
 * Time: 1:23
 */
public class FaveOperation extends RoboOperation {
	@Inject
	ApiClientFactoryInterface mApiFactory;

	@Override
	public Bundle exec(Context context, Request request) throws Exception {
		SocialNetworkProvider provider = (SocialNetworkProvider) request.getParcelable("provider");
		Bundle params = (Bundle)request.getParcelable("params");
		Post post = (Post)params.getParcelable("post");
		ApiClientInterface client = mApiFactory.getInstance(provider);
		client.fave(post);

		Bundle bundle = new Bundle();
		bundle.putParcelable("provider", provider);
		return new Bundle();
	}
}