package com.click4design.socialfeed.data.operation;

import android.content.Context;
import android.os.Bundle;
import com.click4design.socialfeed.data.model.Comment;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.api.ApiClientFactoryInterface;
import com.click4design.socialfeed.network.api.ApiClientInterface;
import com.foxykeep.datadroid.requestmanager.Request;
import com.google.inject.Inject;

/**
 * Author: Khamidullin Kamil
 * Date: 02.03.14
 * Time: 18:38
 */
public class AddCommentOperation extends RoboOperation {
	@Inject
	ApiClientFactoryInterface mApiFactory;

	@Override
	public Bundle exec(Context context, Request request) throws Exception {
		SocialNetworkProvider provider = (SocialNetworkProvider) request.getParcelable("provider");
		Bundle params = (Bundle)request.getParcelable("params");
		ApiClientInterface client = mApiFactory.getInstance(provider);
		Comment comment = client.addComment(params);

		Bundle bundle = new Bundle();
		bundle.putParcelable("comment", comment);


		return bundle;
	}
}