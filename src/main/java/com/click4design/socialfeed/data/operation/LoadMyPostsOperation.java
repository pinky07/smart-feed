package com.click4design.socialfeed.data.operation;

import android.content.Context;
import android.os.Bundle;
import com.click4design.socialfeed.data.model.Feed;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.api.ApiClientFactoryInterface;
import com.click4design.socialfeed.network.api.ApiClientInterface;
import com.foxykeep.datadroid.requestmanager.Request;
import com.google.inject.Inject;

/**
 * Author: Khamidullin Kamil
 * Date: 22.02.14
 * Time: 20:15
 */
public class LoadMyPostsOperation extends RoboOperation {
	@Inject
	ApiClientFactoryInterface mApiFactory;

	@Override
	public Bundle exec(Context context, Request request) throws Exception {
		SocialNetworkProvider provider = (SocialNetworkProvider) request.getParcelable("provider");
		Bundle params = (Bundle)request.getParcelable("params");
		ApiClientInterface client = mApiFactory.getInstance(provider);

		Feed news = client.getMyFeed(params);
		Bundle bundle = new Bundle();
		bundle.putParcelable("news", news);
		bundle.putParcelable("provider", provider);

		return bundle;
	}
}