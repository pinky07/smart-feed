package com.click4design.socialfeed.data;

import android.os.Bundle;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.foxykeep.datadroid.requestmanager.Request;

/**
 * Author: Khamidullin Kamil
 * Date: 22.09.13
 * Time: 1:31
 */
public final class RequestFactory {
    public static final int REQUEST_NEWS = 1;
	public static final int REQUEST_FAVORITES = 4;
    public static final int REQUEST_PHOTOS = 2;
    public static final int REQUEST_ADD_POST = 3;
	public static final int REQUEST_FAVE = 5;
	public static final int REQUEST_UNFAVE = 6;
	public static final int REQUEST_MY_FEED = 7;
	public static final int REQUEST_COMMENTS = 8;
	public static final int REQUEST_ADD_COMMENT = 9;

    public static Request create(int requestCode, SocialNetworkProviderInterface provider, Bundle params) {
        Request request = new Request(requestCode);
        request.put("provider", provider);
        request.put("params", params);

        return request;
    }

    private RequestFactory() {
    }
}