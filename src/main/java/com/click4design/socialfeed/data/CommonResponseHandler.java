package com.click4design.socialfeed.data;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Gravity;
import com.click4design.core.ui.ActivityManager;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.service.BatchRequest;
import com.click4design.socialfeed.network.error.handler.ExceptionHandler;
import com.foxykeep.datadroid.exception.CustomRequestException;
import com.foxykeep.datadroid.requestmanager.Request;
import com.foxykeep.datadroid.requestmanager.RequestManager;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.TextView;

import java.io.Serializable;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 18:46
 */
public class CommonResponseHandler implements RequestManager.RequestListener, Serializable {

    private static final long serialVersionUID = -5405342106487444838L;
    Context context;
    IRequestListener listener;
    ExceptionHandler exceptionHandler;

    public CommonResponseHandler(Context context, IRequestListener listener) {
        this.context = context;
        this.listener = listener;
        this.exceptionHandler = new ExceptionHandler(context);
    }

	public void setRequestListener(IRequestListener listener) {
		this.listener = listener;
	}

	public void removeRequestListener() {
		this.listener = null;
	}

    @Override
    final public void onRequestConnectionError(Request request, int statusCode) {/*doesn't not calling*/}

    @Override
    final public void onRequestDataError(Request request) {/*doesn't not calling*/}


    /**
     * Called when a successful request
     * @param request The {@link Request} defining the request.
     * @param resultData The result of the service execution.
     */
    @Override
    final public void onRequestFinished(Request request, Bundle resultData) {
        dispatchSuccess(request, resultData);
        dispatchFinish(request);
    }

    /**
     * Called when a fail request
     * @param request The {@link Request} defining the request.
     * @param resultData The result of the service execution.
     */
    @Override
    final public void onRequestCustomError(Request request, Bundle resultData) {
        CommonSocialNetworkException exception = (CommonSocialNetworkException)resultData.getSerializable("exception");
        exceptionHandler.handleException(exception, request, this);
        dispatchFinish(request);
    }

    /**
     * Called if there is no internet connection, called before execute
     */
    public void onConnectionError() {
        showConnectionErrorDialog();
    }

    public void dispatchConnectionError() {
        onConnectionError();
    }

    private void dispatchSuccess(Request request, Bundle resultData) {
        if(listener != null) {
            listener.success(request, resultData);
        }
    }

    public void dispatchError(CommonSocialNetworkException exception, Request request) {
        if(listener != null) {
            listener.error(exception, request);
        }
    }

    public void dispatchFinish(Request request) {
        if(listener != null) {
            listener.finish(request);
        }
    }

    public void dispatchStart(Request request) {
        if(listener != null) {
            listener.start(request);
        }
    }

    public static void showConnectionErrorDialog() {
        final Activity activity = ActivityManager.getTopActivity();
        if(activity != null) {
            final TextView msg = new TextView(activity);
            msg.setText("Отсутствует соединение с интернетом, настроить подключение?");
            msg.setPadding(10, 10, 10, 10);
            msg.setTextSize(18);
            msg.setGravity(Gravity.CENTER_HORIZONTAL);

            new AlertDialog.Builder(activity)
                    .setView(msg)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            activity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                            //startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), 1);
                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .create()
                    .show();
        }
    }
}
