package com.click4design.socialfeed.data.exception;

import com.click4design.socialfeed.network.error.adapter.InstagramExceptionAdapter;
import com.click4design.socialfeed.network.error.adapter.TwitterExceptionAdapter;
import com.click4design.socialfeed.network.error.adapter.VKExceptionAdapter;
import com.foxykeep.datadroid.exception.ConnectionException;
import com.perm.kate.api.KException;
import retrofit.RetrofitError;
import twitter4j.TwitterException;

import java.net.UnknownHostException;

/**
 * Author: Khamidullin Kamil
 * Date: 09.02.14
 * Time: 22:06
 */
public class InternalExceptionFactory {
    public static CommonSocialNetworkException reThrow(Exception exeption) {
        try {
            throw exeption;
        } catch (KException ex) {
            return new VKExceptionAdapter(ex);
        } catch (TwitterException ex) {
            return new TwitterExceptionAdapter(ex);
        } catch (RetrofitError ex) {
            if(isInstagramException(ex)) {
                return new InstagramExceptionAdapter(ex);
            } else {
                return new CommonSocialNetworkException(ex);
            }
        } catch (Exception ex) {
            return new CommonSocialNetworkException(ex);
        }
    }

    private static boolean isInstagramException(RetrofitError exception) {
        return exception.getUrl().contains("api.instagram.com");
    }
}
