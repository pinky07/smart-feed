package com.click4design.socialfeed.data.exception;

import com.click4design.socialfeed.data.model.SocialNetworkError;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.error.ErrorType;
import com.foxykeep.datadroid.exception.ConnectionException;
import com.foxykeep.datadroid.exception.CustomRequestException;

import java.net.UnknownHostException;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 18:20
 */
public class CommonSocialNetworkException extends CustomRequestException {
    private static final long serialVersionUID = 1535146375838402239L;
    protected int code = ErrorType.UNKNOWN;
    protected String message;
    protected SocialNetworkProvider provider;

    public CommonSocialNetworkException(Throwable throwable) {
        super(throwable);
        setMessage(throwable.getMessage());
        /*if(throwable instanceof ConnectionException || throwable instanceof UnknownHostException) {
            setCode(ErrorType.CONNECTION_ERROR);
        }*/
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SocialNetworkProvider getProvider() {
        return provider;
    }

    public void setProvider(SocialNetworkProvider provider) {
        this.provider = provider;
    }
}
