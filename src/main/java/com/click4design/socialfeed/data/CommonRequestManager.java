package com.click4design.socialfeed.data;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import com.click4design.core.ui.ActivityManager;
import com.click4design.socialfeed.SocialFeedApplication;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.service.BatchRequest;
import com.click4design.socialfeed.data.service.RestService;
import com.foxykeep.datadroid.requestmanager.Request;
import com.foxykeep.datadroid.requestmanager.RequestManager;
import com.google.inject.Singleton;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 19:01
 */
@Singleton
public class CommonRequestManager {
    RequestManager requestManager;
    private static CommonRequestManager sInstance;

    private CommonRequestManager(Context context) {
        requestManager = RestRequestManager.from(context);
    }

    public static CommonRequestManager from(Context context) {
        if (sInstance == null) {
            sInstance = new CommonRequestManager(context);
        }
        return sInstance;
    }

    public void execute(Request request, CommonResponseHandler listener) {
        if(SocialFeedApplication.getInstance().isOnline()) {
            listener.dispatchStart(request);
            requestManager.execute(request, listener);
        } else {
            listener.dispatchConnectionError();
        }
    }

    public void execute(BatchRequest batchRequest, CommonResponseHandler listener) {
        if(!batchRequest.getRequests().isEmpty()) {
            if(SocialFeedApplication.getInstance().isOnline()) {
                for(Request request : batchRequest.getRequests()) {
                    listener.dispatchStart(request);
                    requestManager.execute(request, listener);
                }
            } else {
                listener.dispatchConnectionError();
            }
        }
    }
}
