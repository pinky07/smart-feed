package com.click4design.socialfeed.data;

import android.os.Bundle;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.foxykeep.datadroid.requestmanager.Request;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 19:16
 */
public interface IRequestListener {
    public void success(Request request, Bundle resultData);

    public void error(CommonSocialNetworkException exception, Request request);

    public void start(Request request);

    public void finish(Request request);
}
