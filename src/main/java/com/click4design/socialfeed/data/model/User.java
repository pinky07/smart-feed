package com.click4design.socialfeed.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Author: Khamidullin Kamil
 * Date: 27.02.14
 * Time: 23:25
 */
public class User implements Parcelable {
	private String id;
	private String username;
	private String fullName;
	private String avatarUrl;

	public User(UserDataProvider dataProvider) {
		setId(dataProvider.getId());
		setUsername(dataProvider.getUsername());
		setFullName(dataProvider.getFullName());
		setAvatarUrl(dataProvider.getAvatarUrl());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(getId());
		parcel.writeString(getUsername());
		parcel.writeString(getFullName());
		parcel.writeString(getAvatarUrl());
	}

	public static final Parcelable.Creator<User> CREATOR
			= new Parcelable.Creator<User>() {
		public User createFromParcel(Parcel in) {
			return new User(in);
		}

		public User[] newArray(int size) {
			return new User[size];
		}
	};

	private User(Parcel in) {
		setId(in.readString());
		setUsername(in.readString());
		setFullName(in.readString());
		setAvatarUrl(in.readString());
	}

	public static interface UserDataProvider {
		public String getId();
		public String getUsername();
		public String getFullName();
		public String getAvatarUrl();
	}
}
