package com.click4design.socialfeed.data.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 19:30
 */
public class SocialNetworkError implements Parcelable {
    protected int code;
    protected String message;
    protected Bundle data;
    protected SocialNetworkProviderInterface provider;

    public SocialNetworkError() {
    }

    public SocialNetworkError(int type) {
        this.code = type;
    }

    public SocialNetworkError(int type, SocialNetworkProviderInterface provider) {
        this.code = type;
        this.provider = provider;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public SocialNetworkProviderInterface getProvider() {
        return provider;
    }

    public void setProvider(SocialNetworkProvider provider) {
        this.provider = provider;
    }

    public Bundle getData() {
        return data;
    }

    public void setData(Bundle data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    protected SocialNetworkError(Parcel parcel) {
        code = parcel.readInt();
        data = parcel.readParcelable(Bundle.class.getClassLoader());
        provider = (SocialNetworkProviderInterface)parcel.readSerializable();
        message = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(getCode());
        parcel.writeParcelable(getData(), i);
        parcel.writeSerializable(getProvider());
        parcel.writeString(message);
    }

    public static final Parcelable.Creator<SocialNetworkError> CREATOR
            = new Parcelable.Creator<SocialNetworkError>() {
        public SocialNetworkError createFromParcel(Parcel in) {
            return new SocialNetworkError(in);
        }

        public SocialNetworkError[] newArray(int size) {
            return new SocialNetworkError[size];
        }
    };
}
