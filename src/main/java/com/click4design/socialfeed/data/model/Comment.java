package com.click4design.socialfeed.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.click4design.socialfeed.feed.FeedItem;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;

/**
 * Author: Khamidullin Kamil
 * Date: 23.02.14
 * Time: 23:49
 */
public class Comment implements FeedItem<Comment> {
	private SocialNetworkProviderInterface provider;
	private String id;
	private String text;
	private long date;
	private User author;

	public Comment(CommentDataProvider commentData) {
		setProvider(commentData.getProvider());
		setId(commentData.getId());
		setAuthor(commentData.getAuthor());
		setText(commentData.getText());
		setDate(commentData.getDate());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public SocialNetworkProviderInterface getProvider() {
		return provider;
	}

	private void setProvider(SocialNetworkProviderInterface provider) {
		this.provider = provider;
	}

	@Override
	public int compareTo(Comment another) {
		return (int)((another).getDate() - getDate());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeSerializable(getProvider());
		dest.writeParcelable(getAuthor(), flags);
		dest.writeString(getId());
		dest.writeString(getText());
		dest.writeLong(getDate());
	}

	public static final Parcelable.Creator<Comment> CREATOR
			= new Parcelable.Creator<Comment>() {
		public Comment createFromParcel(Parcel in) {
			return new Comment(in);
		}

		public Comment[] newArray(int size) {
			return new Comment[size];
		}
	};

	private Comment(Parcel in) {
		setProvider((SocialNetworkProviderInterface)in.readSerializable());
		setAuthor((User)in.readParcelable(User.class.getClassLoader()));
		setId(in.readString());
		setText(in.readString());
		setDate(in.readLong());
	}

	public interface CommentDataProvider {
		public SocialNetworkProviderInterface getProvider();
		public User getAuthor();
		public String getId();
		public String getText();
		public long getDate();
	}
}
