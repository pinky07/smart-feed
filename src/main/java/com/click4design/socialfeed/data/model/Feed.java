package com.click4design.socialfeed.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.click4design.socialfeed.feed.FeedItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 24.09.13
 * Time: 0:25
 */
public class Feed<Item extends FeedItem<Item>> implements Parcelable {
    protected ArrayList<Item> items;
    protected Pagination pagination;

    public Feed(){
        items = new ArrayList<Item>();
    }

    public List<Item> getItems() {
        return items;
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

	public int size() {
		return items.size();
	}

    public Item getLastItem() {
        return Collections.max(items);
    }

	public Item getFirstItem() {
		return Collections.min(items);
	}

    public void addItem(Item post) {
        this.items.add(post);
    }

	public void sort() {
		Collections.sort(getItems());
	}

    public Pagination getPagination() {
		if(pagination == null)
			setPagination(new Pagination());

        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(getItems());
        parcel.writeParcelable(getPagination(), i);
    }

    public static final Parcelable.Creator<Feed> CREATOR
            = new Parcelable.Creator<Feed>() {
        public Feed createFromParcel(Parcel in) {
            return new Feed(in);
        }

        public Feed[] newArray(int size) {
            return new Feed[size];
        }
    };

    private Feed(Parcel in) {
        items = new ArrayList<Item>();
        in.readList(items, Post.class.getClassLoader());
        setPagination((Pagination) in.readParcelable(Pagination.class.getClassLoader()));
    }
}
