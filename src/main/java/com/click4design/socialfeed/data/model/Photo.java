package com.click4design.socialfeed.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Author: Khamidullin Kamil
 * Date: 11.10.13
 * Time: 2:25
 */
public class Photo implements Parcelable {
    protected String id;
    protected String previewUrl;
    protected String url;

	public Photo(PhotoDataProvider dataProvider){
		setId(dataProvider.getId());
		setPreviewUrl(dataProvider.getPreviewUrl());
		setUrl(dataProvider.getUrl());
	}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getPreviewUrl() {
		return previewUrl;
	}

	public void setPreviewUrl(String previewUrl) {
		this.previewUrl = previewUrl;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(getId());
		parcel.writeString(getPreviewUrl());
		parcel.writeString(getUrl());
	}

	public static final Parcelable.Creator<Photo> CREATOR
			= new Parcelable.Creator<Photo>() {
		public Photo createFromParcel(Parcel in) {
			return new Photo(in);
		}

		public Photo[] newArray(int size) {
			return new Photo[size];
		}
	};

	private Photo(Parcel in) {
		setId(in.readString());
		setPreviewUrl(in.readString());
		setUrl(in.readString());
	}

	public static interface PhotoDataProvider {
		public String getId();
		public String getPreviewUrl();
		public String getUrl();
	}
}
