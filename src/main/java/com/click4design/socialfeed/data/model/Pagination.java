package com.click4design.socialfeed.data.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Author: Khamidullin Kamil
 * Date: 22.02.14
 * Time: 11:06
 */
public class Pagination implements Parcelable {
	private Bundle params = new Bundle();
	private boolean hasNext = false;

	public Pagination() {}

	public boolean hasNext() {
		return hasNext;
	}

	public void setHasNext(boolean value) {
		this.hasNext = value;
	}

	public void setParams(Bundle bundle) {
		this.params = bundle;
	}

	public void setParams(String key, String value) {
		this.params.putString(key, value);
	}

	public void setParams(String key, int value) {
		this.params.putInt(key, value);
	}

	public void setParams(String key, long value) {
		this.params.putLong(key, value);
	}

	public Bundle getParams() {
		return params;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeParcelable(getParams(), i);
		parcel.writeInt(hasNext()? 1 : 0);
	}

	public static final Parcelable.Creator<Pagination> CREATOR
			= new Parcelable.Creator<Pagination>() {
		public Pagination createFromParcel(Parcel in) {
			return new Pagination(in);
		}

		public Pagination[] newArray(int size) {
			return new Pagination[size];
		}
	};

	private Pagination(Parcel in) {
		setParams((Bundle) in.readParcelable(Bundle.class.getClassLoader()));
		setHasNext(in.readInt() == 1);
	}
}
