package com.click4design.socialfeed.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.click4design.socialfeed.feed.FeedItem;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 22.09.13
 * Time: 0:32
 */
public class Post implements FeedItem<Post> {
    protected String id;
    protected long date;
	protected String text;
	protected User author;
	protected boolean isRepost;
	protected Post repostedPost;
    protected List<Photo> photos = new ArrayList<Photo>();
    protected SocialNetworkProviderInterface provider;

	protected String photoUri;

	//faves
	protected int favesCount = -1;
	protected boolean canFave = false;
	protected boolean isFaved;

	//reposts
	protected int repostsCount = -1;
	protected boolean canRepost = false;

	//comments
	protected int commentsCount = -1;
	protected boolean canComment = false;

	public Post(PostDataProvider dataProvider) {
		setProvider(dataProvider.getProvider());
		setId(dataProvider.getId());
		setDate(dataProvider.getDate());
		setText(dataProvider.getText());
		setFaved(dataProvider.isFaved());
		setFavesCount(dataProvider.getFavesCount());
		setCanFave(dataProvider.canFave());
		setCommentsCount(dataProvider.getCommentsCount());
		setCanComment(dataProvider.canComment());
		setRepostsCount(dataProvider.getRepostsCount());
		setCanRepost(dataProvider.canRepost());
		setAuthor(dataProvider.getAuthor());
		setPhotos(dataProvider.getPhotos());
	}

	public void doFave() {
		if(!isFaved()) {
			setFaved(true);
			setFavesCount(getFavesCount()+1);
		}
	}

	public void doUnFave() {
		if(isFaved()) {
			setFaved(false);
			setFavesCount(getFavesCount()-1);
		}
	}

	public void setFaved(boolean value) {
		isFaved = value;
	}

	public boolean isFaved() {
		return isFaved;
	}

	public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getId() {
        return id;
    }

    private void setId(String id) {
        this.id = id;
    }

	private void setPhotos(List<Photo> photos){
		this.photos = photos;
	}

    public void addPhoto(Photo photo) {
        this.photos.add(photo);
    }

    public List<Photo> getPhotos() {
        return this.photos;
    }

    public List<Photo> getPhotos(int count) {
        if(photos.size() > count) {
            return photos.subList(0, count);
        } else {
            return photos;
        }
    }

    public boolean hasPhotos() {
        return !getPhotos().isEmpty();
    }

    public int getFavesCount() {
        return this.favesCount;
    }

    public void setFavesCount(int likesCount) {
        this.favesCount = likesCount;
    }

    public int getRepostsCount() {
        return repostsCount;
    }

    public void setRepostsCount(int repostsCount) {
        this.repostsCount = repostsCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

	private void setProvider(SocialNetworkProviderInterface provider) {
		this.provider = provider;
	}

    public SocialNetworkProviderInterface getProvider() {
        return provider;
    }

	public String getPhotoUri() {
		return photoUri;
	}

	public void setPhotoUri(String photoUri) {
		this.photoUri = photoUri;
	}

	public void setCanFave(boolean canFave) {
		this.canFave = canFave;
	}

	public void setCanComment(boolean canComment) {
		this.canComment = canComment;
	}

	public void setCanRepost(boolean canRepost) {
		this.canRepost = canRepost;
	}

	public boolean canFave() {
		return this.canFave;
	}

	public boolean canComment() {
		return this.canComment;
	}

	public boolean canRepost() {
		return this.canRepost;
	}

	public boolean isRepost() {
		return isRepost;
	}

	public void setRepost(boolean value) {
		this.isRepost = value;
	}

	public Post getRepostedPost() {
		return repostedPost;
	}

	public void setRepostedPost(Post post) {
		this.repostedPost = post;
	}

	public List<String> getPhotoUrls()  {
		List<String> photoUrls = new ArrayList<String>();
		for(Photo p : getPhotos()) {
			photoUrls.add(p.getUrl());
		}

		return photoUrls;
	}

	@Override
    public int compareTo(Post another) {
        return (int)((another).getDate() - getDate());
    }

    @Override
    public boolean equals(Object o) {
        if((o instanceof Post) && !TextUtils.isEmpty(getId()) && getProvider() != null) {
            return getId().equals(((Post) o).getId()) && getProvider().equals(((Post) o).getProvider());
        } else {
            return super.equals(o);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeSerializable(getProvider());
        parcel.writeString(getId());
        parcel.writeString(getText());
        parcel.writeLong(getDate());
        parcel.writeInt(getCommentsCount());
        parcel.writeInt(getFavesCount());
        parcel.writeInt(getRepostsCount());
		parcel.writeString(getPhotoUri());
		parcel.writeInt(isFaved()? 1: 0);
		parcel.writeInt(canFave ? 1: 0);
		parcel.writeInt(canComment ? 1 : 0);
		parcel.writeInt(canRepost ? 1: 0);
		parcel.writeParcelable(getAuthor(), i);
		parcel.writeList(getPhotos());
		parcel.writeInt(isRepost ? 1 : 0);
		parcel.writeParcelable(getRepostedPost(), i);
    }

    public static final Parcelable.Creator<Post> CREATOR
            = new Parcelable.Creator<Post>() {
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }

        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    private Post(Parcel in) {
        provider = (SocialNetworkProviderInterface)in.readSerializable();
        setId(in.readString());
        setText(in.readString());
        setDate(in.readLong());
        setCommentsCount(in.readInt());
        setFavesCount(in.readInt());
        setRepostsCount(in.readInt());
		setPhotoUri(in.readString());
		setFaved(in.readInt() == 1);
		setCanFave(in.readInt() == 1);
		setCanComment(in.readInt() == 1);
		setCanRepost(in.readInt() == 1);
		setAuthor((User)in.readParcelable(Photo.class.getClassLoader()));
		in.readList(photos, Photo.class.getClassLoader());
		setRepost(in.readInt() == 1);
		setRepostedPost((Post)in.readParcelable(Post.class.getClassLoader()));
    }

    @Override
    public String toString() {
		if(getProvider() != null && !TextUtils.isEmpty(getId())) {
			return String.format("%s@%s",getProvider().getCode(), getId());
		} else {
			return super.toString();
		}
    }

	public interface PostDataProvider {
		public SocialNetworkProviderInterface getProvider();
		public String getId();
		public String getText();
		public long getDate();
		public int getCommentsCount();
		public int getFavesCount();
		public int getRepostsCount();
		public List<Photo> getPhotos();
		public boolean canFave();
		public boolean canComment();
		public boolean canRepost();
		public boolean isFaved();
		public User getAuthor();
		public boolean isRepost();
		public Post getRepostedPost();
	}
}
