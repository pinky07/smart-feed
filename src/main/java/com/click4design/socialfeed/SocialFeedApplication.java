
package com.click4design.socialfeed;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.click4design.socialfeed.injection.CommonModule;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.account.Account;
import com.click4design.socialfeed.network.account.AccountStorage;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.utils.StorageUtils;
import org.holoeverywhere.app.Application;
import roboguice.RoboGuice;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

//@ReportsCrashes(formKey = "YOUR_FORM_KEY")
public class SocialFeedApplication
    extends Application
{
    AccountStorage accountStorage;

    private static SocialFeedApplication mInstance;

    public static SocialFeedApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        //ACRA.init(this);
        super.onCreate();
        mInstance = this;
        RoboGuice.setBaseApplicationInjector(this,
                RoboGuice.DEFAULT_STAGE,
                RoboGuice.newDefaultRoboModule(this),
                new CommonModule()
        );

        accountStorage = RoboGuice.getInjector(this).getInstance(AccountStorage.class);

        File cacheDir = StorageUtils.getCacheDirectory(this);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .discCacheExtraOptions(480, 800, Bitmap.CompressFormat.JPEG, 75, null)
                .memoryCacheExtraOptions(480, 800)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(50)
                .defaultDisplayImageOptions(new DisplayImageOptions.Builder()
                        .displayer(new FadeInBitmapDisplayer(500))
                        .showImageOnLoading(R.drawable.shape)
                        .showImageForEmptyUri(R.drawable.shape)
                        .showImageOnFail(R.drawable.shape)
                        .cacheOnDisc(true)
                        .cacheInMemory(true)
                        .build())

                .discCache(new UnlimitedDiscCache(cacheDir))
                .build();

        ImageLoader.getInstance().init(config);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
