package com.click4design.socialfeed.space.service;

import android.app.Application;
import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.persistence.CacheManager;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 16:18
 */
public class AggregatedFeedService extends SpiceService {

    @Override
    public CacheManager createCacheManager(Application application ) {
        CacheManager cacheManager = new CacheManager();
        //JacksonObjectPersisterFactory jacksonObjectPersisterFactory = new JacksonObjectPersisterFactory( application );
        //cacheManager.addPersister( jacksonObjectPersisterFactory );
        return cacheManager;
    }


}
