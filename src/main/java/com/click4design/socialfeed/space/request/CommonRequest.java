package com.click4design.socialfeed.space.request;

import android.content.Context;
import android.os.Bundle;
import com.octo.android.robospice.request.SpiceRequest;
import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 17:13
 */
public abstract class CommonRequest<T> extends SpiceRequest<T> {
    protected Bundle params;

    protected CommonRequest(Context context, Class<T> clazz) {
        super(clazz);
        final RoboInjector injector = RoboGuice.getInjector(context);
        injector.injectMembersWithoutViews(this);
    }

    final public void attachParams(Bundle params) {
        this.params = params;
    }

    @Override
    final public T loadDataFromNetwork() throws Exception {
        return execute((params != null) ? params : new Bundle());
    }

    public abstract T execute(Bundle params) throws Exception;
}
