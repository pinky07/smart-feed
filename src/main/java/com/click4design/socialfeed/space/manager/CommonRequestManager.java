package com.click4design.socialfeed.space.manager;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.request.CachedSpiceRequest;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 17:48
 */
public class CommonRequestManager extends SpiceManager {
    public CommonRequestManager(Class<? extends SpiceService> spiceServiceClass) {
        super(spiceServiceClass);
    }

    @Override
    public <T> void execute(CachedSpiceRequest<T> cachedSpiceRequest, RequestListener<T> requestListener) {
        super.execute(cachedSpiceRequest, requestListener);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public <T> void execute(SpiceRequest<T> request, Object requestCacheKey, long cacheExpiryDuration, RequestListener<T> requestListener) {
        super.execute(request, requestCacheKey, cacheExpiryDuration, requestListener);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public <T> void execute(SpiceRequest<T> request, RequestListener<T> requestListener) {
        super.execute(request, requestListener);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
