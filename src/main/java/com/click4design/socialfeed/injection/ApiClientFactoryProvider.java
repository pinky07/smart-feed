package com.click4design.socialfeed.injection;

import com.click4design.socialfeed.network.account.AccountStorage;
import com.click4design.socialfeed.network.api.ApiClientFactory;
import com.click4design.socialfeed.network.api.ApiClientFactoryInterface;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Author: Khamidullin Kamil
 * Date: 23.09.13
 * Time: 17:50
 */
public class ApiClientFactoryProvider implements Provider<ApiClientFactoryInterface> {
    @Inject
    protected AccountStorage sp;

    @Override
    public ApiClientFactoryInterface get() {
        return new ApiClientFactory(sp);
    }
}