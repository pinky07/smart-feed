package com.click4design.socialfeed.injection;

import com.click4design.socialfeed.network.DefaultSocialNetworksManager;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.SocialNetworksManager;
import com.click4design.socialfeed.network.account.AccountStorage;
import com.google.inject.Inject;
import com.google.inject.Provider;

import java.util.Arrays;
import java.util.List;


/**
 * Author: Khamidullin Kamil
 * Date: 19.02.14
 * Time: 22:56
 */
public class SocialNetworksManagerProvider implements Provider<SocialNetworksManager> {
	final private SocialNetworkProviderInterface[] providers = new SocialNetworkProviderInterface[] {
			SocialNetworkProvider.TWITTER,
			SocialNetworkProvider.VK,
			SocialNetworkProvider.INSTAGRAM,
			SocialNetworkProvider.FACEBOOK
	};
	@Inject
	protected AccountStorage storage;

	@Override
	public SocialNetworksManager get() {
		return new DefaultSocialNetworksManager(storage, Arrays.asList(providers));
	}
}