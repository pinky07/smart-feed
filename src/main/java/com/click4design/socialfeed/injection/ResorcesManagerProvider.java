package com.click4design.socialfeed.injection;

import com.click4design.socialfeed.network.resource.DefaultResourcesManager;
import com.click4design.socialfeed.network.resource.ResourcesManager;
import com.google.inject.Provider;
import com.google.inject.Singleton;

/**
 * Author: Khamidullin Kamil
 * Date: 20.02.14
 * Time: 23:32
 */
@Singleton
public class ResorcesManagerProvider implements Provider<ResourcesManager> {
	@Override
	public ResourcesManager get() {
		return new DefaultResourcesManager();
	}
}
