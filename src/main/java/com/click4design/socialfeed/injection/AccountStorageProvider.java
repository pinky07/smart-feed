package com.click4design.socialfeed.injection;

import android.content.SharedPreferences;
import com.click4design.socialfeed.network.account.AccountStorage;
import com.click4design.socialfeed.network.account.SharedPreferenceAccountStorage;
import com.google.inject.Inject;
import com.google.inject.Provider;

/**
 * Author: Khamidullin Kamil
 * Date: 30.06.13
 * Time: 0:03
 */
public class AccountStorageProvider implements Provider<AccountStorage> {
    @Inject
    protected SharedPreferences sp;

    @Override
    public AccountStorage get() {
        return new SharedPreferenceAccountStorage(sp);
    }
}
