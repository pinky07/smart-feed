package com.click4design.socialfeed.injection;

import android.content.Context;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.RestRequestManager;
import com.foxykeep.datadroid.requestmanager.RequestManager;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 18:29
 */
public class RequestManagerProvider implements Provider<CommonRequestManager> {
    @Inject
    protected Context context;

    @Override
    public CommonRequestManager get() {
        return CommonRequestManager.from(context);
    }
}
