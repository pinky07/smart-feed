package com.click4design.socialfeed.injection;

import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.feed.FeedLoader;
import com.click4design.socialfeed.network.SocialNetworksManager;
import com.click4design.socialfeed.network.account.AccountStorage;
import com.click4design.socialfeed.network.api.ApiClientFactoryInterface;
import com.click4design.socialfeed.network.resource.ResourcesManager;
import com.foxykeep.datadroid.requestmanager.RequestManager;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Singleton;

/**
 * Author: Khamidullin Kamil
 * Date: 29.06.13
 * Time: 20:16
 */
    public class CommonModule implements Module {
    @Override
    public void configure(Binder binder) {
		binder.requestStaticInjection();
        binder.bind(AccountStorage.class).toProvider(AccountStorageProvider.class).in(Singleton.class);
        binder.bind(ApiClientFactoryInterface.class).toProvider(ApiClientFactoryProvider.class).in(Singleton.class);
        binder.bind(CommonRequestManager.class).toProvider(RequestManagerProvider.class).in(Singleton.class);
		binder.bind(SocialNetworksManager.class).toProvider(SocialNetworksManagerProvider.class).in(Singleton.class);
		binder.bind(ResourcesManager.class).toProvider(ResorcesManagerProvider.class).in(Singleton.class);
        //binder.bind(AccountStorage.class).toProvider(AccountStorageProvider.class);
    }
}
