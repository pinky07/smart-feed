package com.click4design.socialfeed.utils;

import android.content.Context;

/**
 * Author: Khamidullin Kamil
 * Date: 10.02.14
 * Time: 19:53
 */
public class MetricsConverter {
    public static int dp2px(Context context, int dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return  (int) (dp*scale + 0.5f);
    }
}
