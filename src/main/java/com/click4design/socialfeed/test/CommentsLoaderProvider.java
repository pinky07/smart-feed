package com.click4design.socialfeed.test;

import android.content.Context;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.model.Photo;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.data.model.User;
import com.click4design.socialfeed.feed.FeedItem;
import com.click4design.socialfeed.feed.FeedLoader;
import com.click4design.socialfeed.feed.buffered.BufferedFeedLoader;
import com.click4design.socialfeed.feed.comment.CommentLoader;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.SocialNetworksManager;
import com.google.inject.Inject;
import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 10.03.14
 * Time: 14:12
 */
public class CommentsLoaderProvider extends LoaderProvider {
	@Inject
	protected CommonRequestManager requestManager;
	@Inject
	protected SocialNetworksManager socialNetworksManager;

	public CommentsLoaderProvider(Context context) {
		super(context);
	}

	public FeedLoader<? extends FeedItem> provide() {
		return new CommentLoader(context, requestManager, createTestPost());
	}

	private Post createTestPost() {
		final SocialNetworkProvider provider = SocialNetworkProvider.VK;

		String instagramPostId = "672964811428534799_260955581";
		String instagramAuthorId = "260955581";
		String instagramAuthorUsername = "kadyrov_95";

		String vkPostId = "298958";
		String vkAuthorId = "-28261334";

		String twitterPostId = "";

		final String postId = vkPostId;
		final String authorId = vkAuthorId;
		final String authorUsername = "";

		return new Post(new Post.PostDataProvider() {
			@Override
			public SocialNetworkProviderInterface getProvider() {
				return provider;
			}

			@Override
			public String getId() {
				return postId;
			}

			@Override
			public String getText() {
				return null;
			}

			@Override
			public long getDate() {
				return 0;
			}

			@Override
			public int getCommentsCount() {
				return 0;
			}

			@Override
			public int getFavesCount() {
				return 0;
			}

			@Override
			public int getRepostsCount() {
				return 0;
			}

			@Override
			public List<Photo> getPhotos() {
				return null;
			}

			@Override
			public boolean canFave() {
				return false;
			}

			@Override
			public boolean canComment() {
				return false;
			}

			@Override
			public boolean canRepost() {
				return false;
			}

			@Override
			public boolean isFaved() {
				return false;
			}

			@Override
			public User getAuthor() {
				return new User(new User.UserDataProvider() {
					@Override
					public String getId() {
						return authorId;  //To change body of implemented methods use File | Settings | File Templates.
					}

					@Override
					public String getUsername() {
						return authorUsername;  //To change body of implemented methods use File | Settings | File Templates.
					}

					@Override
					public String getFullName() {
						return null;  //To change body of implemented methods use File | Settings | File Templates.
					}

					@Override
					public String getAvatarUrl() {
						return null;  //To change body of implemented methods use File | Settings | File Templates.
					}
				});  //To change body of implemented methods use File | Settings | File Templates.
			}

			@Override
			public boolean isRepost() {
				return false;  //To change body of implemented methods use File | Settings | File Templates.
			}

			@Override
			public Post getRepostedPost() {
				return null;  //To change body of implemented methods use File | Settings | File Templates.
			}
		});
	}
}
