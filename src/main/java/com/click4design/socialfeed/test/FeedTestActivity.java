package com.click4design.socialfeed.test;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.click4design.core.ui.CommonActivity;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.feed.FeedItem;
import com.click4design.socialfeed.feed.FeedLoader;
import com.click4design.socialfeed.feed.comment.CommentLoader;
import com.click4design.socialfeed.network.SocialNetworksManager;
import com.google.inject.Inject;

import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 10.03.14
 * Time: 13:58
 */
public class FeedTestActivity<T extends FeedItem<T>> extends CommonActivity implements FeedLoader.FeedLoaderListener<T> {
	final String LOG_TAG = "test_log";
	private FeedLoader<T> feedLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);

		feedLoader = LoaderProvider.create(this, LoaderProvider.Loader.COMMENT).provide();
		feedLoader.setFeedLoaderListener(this);

		Button start = (Button)findViewById(R.id.start);
		start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				feedLoader.start();
			}
		});

		Button next = (Button)findViewById(R.id.next);
		next.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				feedLoader.next();
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		feedLoader.removeFeedLoaderListener();
	}

	@Override
	public void onFeedItemsLoaded(List<T> postList) {
		for(T p : postList) {
			Log.d(LOG_TAG, String.format("%tT %s : %s\n", new Date(p.getDate() * 1000), p.getProvider().getCode(), p.getAuthor().getFullName()));
		}
	}

	@Override
	public void onError(CommonSocialNetworkException exception) {
		Log.d(LOG_TAG, String.format("error : %s\n", exception.getMessage()));
	}

	@Override
	public void onConnectionError() {
		Log.d(LOG_TAG, "connection error\n");
	}
}