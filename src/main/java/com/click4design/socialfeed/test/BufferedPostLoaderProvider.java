package com.click4design.socialfeed.test;

import android.content.Context;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.feed.FeedLoader;
import com.click4design.socialfeed.feed.buffered.BufferedFeedLoader;
import com.click4design.socialfeed.network.SocialNetworksManager;
import com.google.inject.Inject;
import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;

/**
 * Author: Khamidullin Kamil
 * Date: 10.03.14
 * Time: 13:59
 */
public class BufferedPostLoaderProvider extends LoaderProvider {
	@Inject
	protected CommonRequestManager requestManager;
	@Inject
	protected SocialNetworksManager socialNetworksManager;

	public BufferedPostLoaderProvider(Context context) {
		super(context);
	}

	public FeedLoader provide() {
		return new BufferedFeedLoader<Post>(context, requestManager, socialNetworksManager.getAuthorizedSocialNetworks());
	}
}
