package com.click4design.socialfeed.test;

import android.content.Context;
import com.click4design.socialfeed.feed.FeedItem;
import com.click4design.socialfeed.feed.FeedLoader;
import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;

/**
 * Author: Khamidullin Kamil
 * Date: 10.03.14
 * Time: 14:15
 */
abstract public class LoaderProvider<T extends FeedItem<T>> {
	protected Context context;

	public LoaderProvider(Context context) {
		final RoboInjector injector = RoboGuice.getInjector(context);
		injector.injectMembersWithoutViews(this);
		this.context = context;
	}

	abstract FeedLoader<T> provide();

	public static LoaderProvider create(Context context, Loader type) {
		switch (type)  {
			case POST:
				return new BufferedPostLoaderProvider(context);
			case COMMENT:
				return new CommentsLoaderProvider(context);
			default:
				return new BufferedPostLoaderProvider(context);
		}
	}


	public enum Loader {
		POST,
		COMMENT
	}
}
