package com.click4design.socialfeed.network;

import com.click4design.socialfeed.network.account.Account;
import com.click4design.socialfeed.network.account.AccountStorage;

import java.util.ArrayList;
import java.util.List;
import com.click4design.socialfeed.network.account.AccountStorage.AccountStorageListener;
import com.google.inject.Singleton;

/**
 * default
 * Author: Khamidullin Kamil
 * Date: 19.02.14
 * Time: 22:45
 */
@Singleton
public class DefaultSocialNetworksManager implements SocialNetworksManager, AccountStorageListener {
	AccountStorage accountStorage;
	List<SocialNetworkProviderInterface> allProviders;
	List<SocialNetworkProviderInterface> authProviders;
	List<SocialNetworkProviderInterface> notAuthProviders;


	public DefaultSocialNetworksManager(AccountStorage accountStorage, List<SocialNetworkProviderInterface> allProviders) {
		this.accountStorage 	= accountStorage;
		this.accountStorage.addAccountStorageListener(this);
		this.allProviders  		= allProviders;
		this.authProviders 		= new ArrayList<SocialNetworkProviderInterface>();
		this.notAuthProviders 	= new ArrayList<SocialNetworkProviderInterface>();
		init();
	}

	private void init() {
		for(SocialNetworkProviderInterface provider : allProviders) {
			for(Account account : accountStorage.getAll()) {
				if(account.getClass().equals(provider.getAccountClass())) {
					authProviders.add(provider);
				}
			}
		}

		for(SocialNetworkProviderInterface provider : allProviders) {
			if(!authProviders.contains(provider)) {
				notAuthProviders.add(provider);
			}
		}
	}

	@Override
	public List<SocialNetworkProviderInterface> getSocialNetworks() {
		return this.allProviders;
	}

	@Override
	public List<SocialNetworkProviderInterface> getAuthorizedSocialNetworks() {
		return this.authProviders;
	}

	@Override
	public List<SocialNetworkProviderInterface> getNotAuthorizedSocialNetworks() {
		return this.notAuthProviders;
	}

	@Override
	public void accountSaved(SocialNetworkProviderInterface socialNetworkProvider, Account account) {
		if(notAuthProviders.contains(socialNetworkProvider)) {
			notAuthProviders.remove(socialNetworkProvider);
		}
		if(!authProviders.contains(socialNetworkProvider)) {
			authProviders.add(socialNetworkProvider);
		}
	}

	@Override
	public void accountRemoved(SocialNetworkProviderInterface socialNetworkProvider, Account account) {
		if(!notAuthProviders.contains(socialNetworkProvider)) {
			notAuthProviders.add(socialNetworkProvider);
		}

		if(authProviders.contains(socialNetworkProvider)) {
			authProviders.remove(socialNetworkProvider);
		}
	}
}
