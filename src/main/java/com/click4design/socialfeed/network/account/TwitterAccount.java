package com.click4design.socialfeed.network.account;

import twitter4j.auth.AccessToken;

/**
 * Author: Khamidullin Kamil
 * Date: 30.06.13
 * Time: 0:43
 */
public class TwitterAccount implements Account {
    protected String token;
    protected String tokenSecret;
    protected transient AccessToken accessToken;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    public AccessToken getAccessToken() {
        if(accessToken == null)
            accessToken = new AccessToken(getToken(), getTokenSecret());

        return accessToken;
    }
}
