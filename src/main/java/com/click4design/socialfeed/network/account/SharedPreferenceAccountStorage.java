package com.click4design.socialfeed.network.account;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.google.gson.Gson;
import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 29.06.13
 * Time: 20:24
 */
@Singleton
public class SharedPreferenceAccountStorage implements AccountStorage {
    protected SharedPreferences mSharedPref;
	protected List<AccountStorageListener> listeners = new ArrayList<AccountStorageListener>();

    public SharedPreferenceAccountStorage(SharedPreferences sp) {
        mSharedPref = sp;
    }

    @Override
    public void save(SocialNetworkProviderInterface socialNetworkProvider, Account account) {
        Editor editor = mSharedPref.edit();
        editor.putString(
                socialNetworkProvider.getCode(),
                new Gson().toJson(account)
        );
        editor.commit();

		dispatchAccountSaved(socialNetworkProvider, account);
    }

    @Override
    public Account get(SocialNetworkProviderInterface socialNetworkProvider) {
        String json = mSharedPref.getString(socialNetworkProvider.getCode(), "");
        try {
            return new Gson().
                    fromJson(
                            json,
                            socialNetworkProvider.getAccountClass()
                    );
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public void remove(SocialNetworkProviderInterface socialNetworkProvider) {
		Account account = get(socialNetworkProvider);
        Editor editor = mSharedPref.edit();
        editor.remove(socialNetworkProvider.getCode());
        editor.commit();

		dispatchAccountRemoved(socialNetworkProvider, account);
    }

    @Override
    public boolean has(SocialNetworkProviderInterface socialNetworkProvider) {
        return get(socialNetworkProvider) != null;
    }

    @Override
    public Account[] getAll() {
        List<Account> accounts = new ArrayList<Account>();
        for(SocialNetworkProvider provider : SocialNetworkProvider.class.getEnumConstants()) {
            Account account = get(provider);
            if(account != null) {
                accounts.add(account);
            }
        }

        return accounts.toArray(new Account[accounts.size()]);
    }

	private void dispatchAccountSaved(SocialNetworkProviderInterface provider, Account account) {
		if(listeners != null) {
			for(AccountStorageListener listener : listeners) {
				listener.accountSaved(provider, account);
			}
		}
	}

	private void dispatchAccountRemoved(SocialNetworkProviderInterface provider, Account account) {
		if(listeners != null) {
			for(AccountStorageListener listener : listeners) {
				listener.accountRemoved(provider, account);
			}
		}
	}

	@Override
	public void addAccountStorageListener(AccountStorageListener listener) {
		if(listeners == null) {
			listeners = new ArrayList<AccountStorageListener>();
		}

		listeners.add(listener);
	}

	@Override
	public void removeAccountStorageListener(AccountStorageListener listener) {
		if(listeners != null && listeners.contains(listener)) {
			listeners.remove(listener);
		}
	}
}
