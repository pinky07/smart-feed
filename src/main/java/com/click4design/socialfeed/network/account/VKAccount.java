package com.click4design.socialfeed.network.account;

/**
 * Author: Khamidullin Kamil
 * Date: 30.06.13
 * Time: 0:44
 */
public class VKAccount implements Account {
    protected String token;
    protected String userId;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
