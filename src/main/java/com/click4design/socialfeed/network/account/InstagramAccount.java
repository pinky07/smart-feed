package com.click4design.socialfeed.network.account;

/**
 * Author: Khamidullin Kamil
 * Date: 16.11.13
 * Time: 19:54
 */
public class InstagramAccount implements Account{
    protected String accessToken;

    public String getToken() {
        return accessToken;
    }

    public void setToken(String token) {
        this.accessToken = token;
    }
}
