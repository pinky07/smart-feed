package com.click4design.socialfeed.network.account;

import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.google.inject.Singleton;

/**
 * Author: Khamidullin Kamil
 * Date: 30.06.13
 * Time: 0:36
 */

public interface AccountStorage {
    public void save(SocialNetworkProviderInterface socialNetworkProvider, Account account);

    public Account get(SocialNetworkProviderInterface socialNetworkProvider);

    public void remove(SocialNetworkProviderInterface socialNetworkProvider);

    public boolean has(SocialNetworkProviderInterface socialNetworkProvider);

	public void addAccountStorageListener(AccountStorageListener listener);

	public void removeAccountStorageListener(AccountStorageListener listener);

    public Account[] getAll();

	public static interface AccountStorageListener {
		public void accountSaved(SocialNetworkProviderInterface socialNetworkProvider, Account account);
		public void accountRemoved(SocialNetworkProviderInterface socialNetworkProvider, Account account);
	}
}
