package com.click4design.socialfeed.network.api;

import android.os.Bundle;
import com.click4design.socialfeed.data.model.*;
import com.click4design.socialfeed.data.model.Comment;
import com.click4design.socialfeed.data.model.Photo;
import com.click4design.socialfeed.data.model.User;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.account.VKAccount;
import com.perm.kate.api.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 22.09.13
 * Time: 2:07
 */
public class VKApiClientAdapter implements ApiClientInterface {
    protected Api vkClient;
    protected static VKApiClientAdapter instance;

    public VKApiClientAdapter(VKAccount account) {
        vkClient = new Api(account.getToken(), "3890987");
        instance = this;
    }

    @Override
    public Feed<Post> getPhotoFeed() throws Exception {
        Bundle params = new Bundle();
        params.putString("filter", "photo");
        return getFeed(params);
    }

    @Override
    public Feed<Post> getPhotoFeed(Bundle params) throws Exception {
        params.putString("filter", "photo");
        return getFeed(params);
    }

    @Override
    public Feed<Post> getFeed() throws Exception {
        return getFeed(new Bundle());
    }

    @Override
    public Feed<Post> getFeed(Bundle params) throws Exception {
        Feed<Post> news = new Feed<Post>();
        int count  		= params.getInt("count", 0);
		int offset 		= params.getInt("offset", 0);
		String from     = params.containsKey("from")? params.getString("from") : "";
		String filter 	= params.containsKey("filter")? params.getString("filter") : "";;

        Newsfeed newsfeed = vkClient.getNews(0, count, 0, offset, from, "", "", filter);

        for(NewsItem item : newsfeed.items) {
			news.addItem(new Post(new VKPostDataProvider(item, newsfeed)));
		}

		Pagination pagination = new Pagination();
		pagination.setHasNext(news.size() == count && count > 0);
		pagination.setParams("count", count);
		pagination.setParams("offset", newsfeed.newOffset);
		pagination.setParams("from", newsfeed.newFrom);

        news.setPagination(pagination);

        return news;
    }

	@Override
	public Feed<Post> getFavoritesFeed(Bundle params) throws Exception {
		int count  = params.getInt("count", -1);
		int offset = params.getInt("offset", -1);

		WallFeed messages = vkClient.getFavePosts(count, offset);
		Feed<Post> feed = createFeedByWallFeed(messages);

		Pagination pagination = new Pagination();
		pagination.setHasNext((count + offset) < messages.count);
		pagination.setParams("count", count);
		pagination.setParams("offset", offset + count);

		feed.setPagination(pagination);

		return feed;
	}

	public void sendPost(Bundle params) throws Exception{
		String text = params.getString("text");
		String photoUri = params.getString("photo_uri");

        vkClient.createWallPost(text, photoUri);
    }

	@Override
	public void fave(Post post) throws Exception {
		vkClient.addLike(Long.parseLong(post.getAuthor().getId()), Long.parseLong(post.getId()), "post", null, null);
	}

	@Override
	public void unFave(Post post) throws Exception {
		vkClient.deleteLike(Long.parseLong(post.getAuthor().getId()), "post", Long.parseLong(post.getId()), null, null);
	}

	@Override
	public Feed<Post> getMyFeed(Bundle params) throws Exception {
		int count  		= params.getInt("count", -1);
		int offset 		= params.getInt("offset", -1);

		WallFeed wallFeed 	= vkClient.getWallMessages(0L, count, offset);
		Feed<Post> feed 			= createFeedByWallFeed(wallFeed);

		Pagination pagination = new Pagination();
		pagination.setHasNext((count + offset) < wallFeed.count);
		pagination.setParams("count", count);
		pagination.setParams("offset", offset + count);

		feed.setPagination(pagination);

		return feed;
	}

	private Feed<Post> createFeedByWallFeed(WallFeed wallFeed) {
		Feed<Post> news = new Feed<Post>();
		for (WallMessage wallMessage: wallFeed.items) {
			news.addItem(new Post(new VKWallMessageDataProvider(wallMessage, wallFeed)));
		}

		return news;
	}

	private List<User> getUsersByIds(Long...ids) throws Exception {
		List<User> users = new ArrayList<User>();
		List<com.perm.kate.api.User> profiles = vkClient.getProfiles(Arrays.asList(ids), null, null, "nom", null, null);
		for(com.perm.kate.api.User profile : profiles) {
			users.add(new User(new VKUserDataProvider(profile)));
		}

		return users;
	}

	@Override
	public Feed<Comment> getComments(Bundle params) throws Exception {
		long ownerId = Long.parseLong(params.getString("owner_id"));
		long postId  = Long.parseLong(params.getString("post_id"));
		int offset   = params.getInt("offset", 0);
		int count    = params.getInt("offset", 20);
		String sort  = (params.containsKey("sort")) ? params.getString("sort") : "desc";

		Feed<Comment> commentFeed = new Feed<Comment>();
		CommentList commentList = vkClient.getWallComments(ownerId, postId, sort, offset, count, "5.11");
		List<com.perm.kate.api.User> users = vkClient.getProfiles(Arrays.asList(commentList.getProfilesIds()), null, null, "nom", null, null);
		List<Group> groups = vkClient.getGroups(Arrays.asList(commentList.getGroupsIds()), null, null);
		for (com.perm.kate.api.Comment comment : commentList.comments){
			commentFeed.addItem(new Comment(new VKCommentDataProvider(comment, users, groups)));
		}

		Pagination pagination = new Pagination();
		pagination.setHasNext((count + offset) < commentList.count);
		pagination.setParams("count", count);
		pagination.setParams("sort", sort);
		pagination.setParams("offset", offset + count);

		commentFeed.setPagination(pagination);

		return commentFeed;
	}

	@Override
	public Comment addComment(Bundle params) throws Exception {
		String text 	= params.getString("text");
		long authorId 	= Long.parseLong(params.getString("author_id"));
		long postId 	= Long.parseLong(params.getString("post_id"));

		vkClient.createWallComment(authorId, postId, text, null, null, null, null);


		return null;
	}

	public static class VKPhotoDataProvider implements Photo.PhotoDataProvider {
		private com.perm.kate.api.Photo photo;

		public VKPhotoDataProvider(com.perm.kate.api.Photo photo) {
			this.photo = photo;
		}

		@Override
		public String getId() {
			return Long.toString(photo.pid);
		}

		@Override
		public String getPreviewUrl() {
			return photo.src_small;
		}

		@Override
		public String getUrl() {
			return photo.src_big;
		}
	}

	public static class VKUserDataProvider implements User.UserDataProvider {
		private com.perm.kate.api.User user;

		public VKUserDataProvider(com.perm.kate.api.User user) {
			this.user = user;
		}

		@Override
		public String getId() {
			return Long.toString(user.uid);
		}

		@Override
		public String getUsername() {
			return user.nickname;
		}

		@Override
		public String getFullName() {
			return String.format("%s %s", user.first_name, user.last_name);
		}

		@Override
		public String getAvatarUrl() {
			return user.photo_medium_rec;
		}
	}

	public static class VKGroupDataProvider implements User.UserDataProvider {
		private Group group;
		public VKGroupDataProvider(Group group) {
			this.group = group;
		}

		@Override
		public String getId() {
			return Long.toString(group.gid*(-1));
		}

		@Override
		public String getUsername() {
			return group.name;
		}

		@Override
		public String getFullName() {
			return group.name;
		}

		@Override
		public String getAvatarUrl() {
			return group.photo_big;
		}
	}

	public static class VKPostDataProvider implements Post.PostDataProvider {
		private NewsItem newsItem;
		private Newsfeed newsfeed;

		public VKPostDataProvider(NewsItem item, Newsfeed newsfeed) {
			this.newsItem = item;
			this.newsfeed = newsfeed;
		}

		@Override
		public SocialNetworkProviderInterface getProvider() {
			return SocialNetworkProvider.VK;
		}

		@Override
		public String getId() {
			if(newsItem.post_id == 0) {
				return String.format("%d%d", newsItem.source_id, newsItem.date);
			} else {
				return Long.toString(newsItem.post_id);
			}
		}

		@Override
		public String getText() {
			return newsItem.text;
		}

		@Override
		public long getDate() {
			return newsItem.date;
		}

		@Override
		public int getCommentsCount() {
			return newsItem.comment_count;
		}

		@Override
		public int getFavesCount() {
			return newsItem.like_count;
		}

		@Override
		public int getRepostsCount() {
			return newsItem.repost_count;
		}

		@Override
		public List<Photo> getPhotos() {
			List<Photo> photos = new ArrayList<Photo>();

			if(newsItem.type.equals("photo")) {
				for(com.perm.kate.api.Photo photo : newsItem.photos) {
					photos.add(new Photo(new VKPhotoDataProvider(photo)));
				}
			} else {
				for(Attachment attachment : newsItem.attachments) {
					if(attachment.photo != null) {
						photos.add(new Photo(new VKPhotoDataProvider(attachment.photo)));
					}
				}
			}

			return photos;
		}

		@Override
		public boolean canFave() {
			return true;
		}

		@Override
		public boolean canComment() {
			return newsItem.comment_can_post;
		}

		@Override
		public boolean canRepost() {
			return true;
		}

		@Override
		public boolean isFaved() {
			return newsItem.user_like;
		}

		@Override
		public User getAuthor() {
			if(newsItem.source_id > 0) {
				for (com.perm.kate.api.User user : newsfeed.profiles) {
					if(user.uid == newsItem.source_id) {
						return new User(new VKUserDataProvider(user));
					}
				}
			} else {
				for (Group group : newsfeed.groups) {
					if(group.gid == newsItem.source_id*(-1)) {
						return new User(new VKGroupDataProvider(group));
					}
				}
			}

			return null;
		}

		@Override
		public boolean isRepost() {
			return false; //todo распорсить репосты
		}

		@Override
		public Post getRepostedPost() {
			return null;  //todo распорсить репосты
		}
	}

	public static class VKWallMessageDataProvider implements Post.PostDataProvider {
		private WallMessage wallMessage;
		private WallFeed wallFeed;

		public VKWallMessageDataProvider(WallMessage wallMessage, WallFeed wallFeed) {
			this.wallMessage = wallMessage;
			this.wallFeed	 = wallFeed;
		}

		@Override
		public SocialNetworkProviderInterface getProvider() {
			return SocialNetworkProvider.VK;
		}

		@Override
		public String getId() {
			if(wallMessage.id == 0) {
				return String.format("%d%d", wallMessage.from_id, wallMessage.date);
			} else {
				return Long.toString(wallMessage.id);
			}
		}

		@Override
		public String getText() {
			return wallMessage.text;
		}

		@Override
		public long getDate() {
			return wallMessage.date;
		}

		@Override
		public int getCommentsCount() {
			return wallMessage.comment_count;
		}

		@Override
		public int getFavesCount() {
			return wallMessage.like_count;
		}

		@Override
		public int getRepostsCount() {
			return wallMessage.repost_count;
		}

		@Override
		public List<Photo> getPhotos() {
			List<Photo> photos = new ArrayList<Photo>();
			for (Attachment attachment: wallMessage.attachments) {
				if(attachment.photo != null) {
					photos.add(new Photo(new VKPhotoDataProvider(attachment.photo)));
				}
			}

			return photos;
		}

		@Override
		public boolean canFave() {
			return wallMessage.can_like;
		}

		@Override
		public boolean canComment() {
			return wallMessage.comment_can_post;
		}

		@Override
		public boolean canRepost() {
			return wallMessage.like_can_publish;
		}

		@Override
		public boolean isFaved() {
			return wallMessage.user_like;
		}

		@Override
		public User getAuthor() {
			if(wallMessage.from_id > 0) {
				for (com.perm.kate.api.User user : wallFeed.profiles) {
					if(user.uid == wallMessage.from_id) {
						return new User(new VKUserDataProvider(user));
					}
				}
			} else {
				for (Group group : wallFeed.groups) {
					if(group.gid == wallMessage.from_id*(-1)) {
						return new User(new VKGroupDataProvider(group));
					}
				}
			}

			return null;
		}

		@Override
		public boolean isRepost() {
			return false;  //todo распарсить репосты
		}

		@Override
		public Post getRepostedPost() {
			return null;  //todo распарсить репосты
		}
	}

	private class VKCommentDataProvider implements Comment.CommentDataProvider {
		private com.perm.kate.api.Comment comment;
		private List<com.perm.kate.api.User> users;
		private List<Group> groups;

		public VKCommentDataProvider(com.perm.kate.api.Comment comment, List<com.perm.kate.api.User> users, List<Group> groups) {
			this.comment = comment;
			this.users = users;
			this.groups = groups;
		}

		@Override
		public SocialNetworkProviderInterface getProvider() {
			return SocialNetworkProvider.VK;
		}

		@Override
		public User getAuthor() {
			if(comment.from_id > 0) {
				for(com.perm.kate.api.User user : users) {
					if (user.uid == comment.from_id) {
						return new User(new VKUserDataProvider(user));
					}
				}
			} else {
				for(Group group : groups) {
					if (group.gid == comment.from_id*-1) {
						return new User(new VKGroupDataProvider(group));
					}
				}
			}


			return null;
		}

		@Override
		public String getId() {
			return Long.toString(comment.cid);
		}

		@Override
		public String getText() {
			return comment.message;
		}

		@Override
		public long getDate() {
			return comment.date;
		}
	}
}
