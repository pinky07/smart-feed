package com.click4design.socialfeed.network.api;

import android.os.Bundle;
import com.click4design.socialfeed.data.model.Comment;
import com.click4design.socialfeed.data.model.Feed;
import com.click4design.socialfeed.data.model.Post;

/**
 * Author: Khamidullin Kamil
 * Date: 22.09.13
 * Time: 2:05
 */
public interface ApiClientInterface {

    public Feed<Post> getFeed() throws Exception;

    public Feed<Post> getPhotoFeed() throws Exception;

    public Feed<Post> getPhotoFeed(Bundle params) throws Exception;

    public Feed<Post> getFeed(Bundle params) throws Exception;

	public Feed<Post> getFavoritesFeed(Bundle params) throws Exception;

	public void fave(Post post) throws Exception;

	public void unFave(Post post) throws Exception;

	public void sendPost(Bundle params) throws Exception;

	public Feed<Post> getMyFeed(Bundle params) throws Exception;

	public Feed<Comment> getComments(Bundle params) throws Exception;

	public Comment addComment(Bundle params) throws Exception;
}
