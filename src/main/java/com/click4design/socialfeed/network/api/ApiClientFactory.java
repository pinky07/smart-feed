package com.click4design.socialfeed.network.api;

import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.account.*;
import com.click4design.socialfeed.network.account.AccountStorage.AccountStorageListener;
import com.google.inject.Singleton;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: Khamidullin Kamil
 * Date: 23.09.13
 * Time: 1:43
 */
@Singleton
public class ApiClientFactory implements ApiClientFactoryInterface, AccountStorageListener {
    protected AccountStorage accountStorage;
    protected Map<SocialNetworkProviderInterface, ApiClientInterface>clients = new HashMap<SocialNetworkProviderInterface, ApiClientInterface>();

    public ApiClientFactory(AccountStorage storage) {
        accountStorage = storage;
		accountStorage.addAccountStorageListener(this);
    }

    public ApiClientInterface getInstance(SocialNetworkProviderInterface provider) {
        if(!clients.containsKey(provider)) {
			ApiClientInterface client = create(provider);
			if(client != null) {
				clients.put(provider, create(provider));
			} else {
				return null;
			}
        }

        return clients.get(provider);
    }

	@Override
	public void accountSaved(SocialNetworkProviderInterface socialNetworkProvider, Account account) {
		ApiClientInterface client = create(socialNetworkProvider);
		if(client != null) {
			clients.put(socialNetworkProvider, create(socialNetworkProvider));
		}
	}

	@Override
	public void accountRemoved(SocialNetworkProviderInterface socialNetworkProvider, Account account) {
		if(clients.containsKey(socialNetworkProvider)) {
			clients.remove(socialNetworkProvider);
		}
	}

	private ApiClientInterface create(SocialNetworkProviderInterface provider) {
		switch ((SocialNetworkProvider)provider) {
			case TWITTER:
				return new TwitterApiClientAdapter((TwitterAccount)accountStorage.get(provider));
			case VK:
				return new VKApiClientAdapter((VKAccount)accountStorage.get(provider));
			case INSTAGRAM:
				return new InstagramClientAdapter((InstagramAccount)accountStorage.get(provider));
			default:
				return null;
		}
	}
}
