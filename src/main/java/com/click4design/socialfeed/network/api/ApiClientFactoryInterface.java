package com.click4design.socialfeed.network.api;

import com.click4design.socialfeed.network.SocialNetworkProviderInterface;

/**
 * Author: Khamidullin Kamil
 * Date: 23.09.13
 * Time: 17:39
 */
public interface ApiClientFactoryInterface {
    public ApiClientInterface getInstance(SocialNetworkProviderInterface provider);
}
