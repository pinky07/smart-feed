package com.click4design.socialfeed.network.api;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.click4design.socialfeed.data.model.*;
import com.click4design.socialfeed.data.model.Comment;
import com.click4design.socialfeed.data.model.Feed;
import com.click4design.socialfeed.data.model.Pagination;
import com.click4design.socialfeed.data.model.User;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.account.InstagramAccount;
import com.getinch.retrogram.Instagram;
import com.getinch.retrogram.endpoints.UsersEndpoint;
import com.getinch.retrogram.model.*;
import retrofit.RetrofitError;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Khamidullin Kamil
 * Date: 06.01.14
 * Time: 20:15
 */
public class InstagramClientAdapter implements ApiClientInterface {
    protected final Instagram client;

    public InstagramClientAdapter(InstagramAccount account) {
        client = new Instagram(account.getToken());
    }

    @Override
    public Feed getPhotoFeed() throws Exception {
        return getFeed();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Feed getPhotoFeed(Bundle params) throws Exception {
        return getFeed(params);
    }

    @Override
    public Feed getFeed() throws Exception {
        return getFeed(new Bundle());
    }

    @Override
    public Feed<Post> getFeed(Bundle params) throws RetrofitError {
        UsersEndpoint endpoint = client.getUsersEndpoint();
        Integer count = (params.containsKey("count")) ? params.getInt("count") : null;
        String maxId = (params.containsKey("maxId")) ? params.getString("maxId") : null;

		Log.i("inst_client", "request maxId = " + maxId);
		com.getinch.retrogram.model.Feed feed = endpoint.getFeed(count, null, maxId);
        Feed<Post> news = createFeedByMediaList(feed.getMediaList());

		Log.i("inst_client", "response maxId = " + feed.getPagination().getNextMaxId());

		Pagination pagination = new Pagination();
		pagination.setParams("maxId", feed.getPagination().getNextMaxId());
		pagination.setHasNext(feed.getPagination().getNextMaxId() != null);
		if(count != null) {
			pagination.setParams("count", count);
		}

        news.setPagination(pagination);

        return news;
    }

	@Override
	public Feed<Post> getFavoritesFeed(Bundle params) throws Exception {
		UsersEndpoint endpoint = client.getUsersEndpoint();
		Integer count = (params.containsKey("count")) ? params.getInt("count") : null;
		String maxId = (params.containsKey("maxId")) ? params.getString("maxId") : null;
		Log.i("inst_client", "request maxId = " + maxId);
		Liked feed = endpoint.getLiked(count, maxId);

		Feed<Post> news = createFeedByMediaList(feed.getMediaList());

		Pagination pagination = new Pagination();
		maxId = null;
		try {
			URL url = new URL(feed.getPagination().getNextUrl());
			Map<String, String> query = splitQuery(url);
			if(query.containsKey("max_like_id")) {
				maxId = query.get("max_like_id");
			}
		} catch (MalformedURLException mex) {/**/}

		if(!TextUtils.isEmpty(maxId)) {
			pagination.setParams("maxId", maxId);
			pagination.setHasNext(true);
		}

		if(count != null) {
			pagination.setParams("count", count);
		}

		news.setPagination(pagination);

		return news;
	}

	@Override
	public void sendPost(Bundle params) throws Exception {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void fave(Post post) throws Exception {
		client.getLikesEndpoint().like(post.getId());
	}

	public static Map<String, String> splitQuery(URL url) throws UnsupportedEncodingException {
		Map<String, String> query_pairs = new LinkedHashMap<String, String>();
		String query = url.getQuery();
		String[] pairs = query.split("&");
		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}
		return query_pairs;
	}

	@Override
	public void unFave(Post post) throws Exception {
		client.getLikesEndpoint().unlike(post.getId());
	}

	@Override
	public Feed<Post> getMyFeed(Bundle params) throws Exception {
		Integer count = (params.containsKey("count")) ? params.getInt("count") : null;
		String maxId = (params.containsKey("maxId")) ? params.getString("maxId") : null;

		Recent recent = client.getUsersEndpoint().getRecent(count, null, maxId, null, null);
		Feed<Post> feed = createFeedByMediaList(recent.getMediaList());

		Log.i("inst_client", "response maxId = " + recent.getPagination().getNextMaxId());

		Pagination pagination = new Pagination();
		pagination.setParams("maxId", recent.getPagination().getNextMaxId());
		pagination.setHasNext(recent.getPagination().getNextMaxId() != null);
		if(count != null) {
			pagination.setParams("count", count);
		}

		feed.setPagination(pagination);

		return feed;
	}

	private Feed<Post> createFeedByMediaList(List<Media> mediaList) {
		Feed<Post> news = new Feed<Post>();
		for(Media media : mediaList) {
			if(media.getType().equals("image")) {
				news.addItem(new Post(new InstagramMediaDataProvider(media)));
			}
		}

		return news;
	}

	@Override
	public Feed<Comment> getComments(Bundle params) throws Exception {
		String postId  = params.getString("post_id");
		Feed<Comment> feed = new Feed<Comment>();

		Comments comments = client.getCommentsEndpoint().getComments(postId);
		for(com.getinch.retrogram.model.Comment comment : comments.getComments()) {
			feed.addItem(new Comment(new InstagramCommentAdapter(comment)));
		}

		if(params.containsKey("sort") && params.getString("sort").equals("desc")) {
			feed.sort();
		}

		Pagination pagination = new Pagination();
		pagination.setHasNext(false);
		feed.setPagination(pagination);

		return feed;
	}

	@Override
	public Comment addComment(Bundle params) throws Exception {
		String text = params.getString("text");
		String postId = params.getString("post_id");

		client.getCommentsEndpoint().comment(postId, text);

		return null;
	}

	private class InstagramUserDataProvider implements User.UserDataProvider {
		private com.getinch.retrogram.model.User user;

		public InstagramUserDataProvider(com.getinch.retrogram.model.User user) {
			this.user = user;
		}

		@Override
		public String getId() {
			return user.getId();
		}

		@Override
		public String getUsername() {
			return user.getUsername();
		}

		@Override
		public String getFullName() {
			return user.getUsername();
		}

		@Override
		public String getAvatarUrl() {
			return user.getProfilePicture();
		}
	}

	private class InstagramPhotoDataProvider implements Photo.PhotoDataProvider {
		private Images images;

		public InstagramPhotoDataProvider(Images images) {
			this.images = images;
		}

		@Override
		public String getId() {
			return Integer.toString(images.hashCode());
		}

		@Override
		public String getPreviewUrl() {
			return images.getThumbnail().getUrl();
		}

		@Override
		public String getUrl() {
			return images.getStandardResolution().getUrl();
		}
	}

	private class InstagramMediaDataProvider implements Post.PostDataProvider {
		private Media media;

		public InstagramMediaDataProvider(Media media) {
			this.media = media;
		}

		@Override
		public SocialNetworkProviderInterface getProvider() {
			return SocialNetworkProvider.INSTAGRAM;
		}

		@Override
		public String getId() {
			return media.getId();
		}

		@Override
		public String getText() {
			return "";
		}

		@Override
		public long getDate() {
			return media.getCreatedTime();
		}

		@Override
		public int getCommentsCount() {
			return media.getComments().getCount();
		}

		@Override
		public int getFavesCount() {
			return media.getLikes().getCount();
		}

		@Override
		public int getRepostsCount() {
			return -1;
		}

		@Override
		public List<Photo> getPhotos() {
			List<Photo> photos = new ArrayList<Photo>();
			photos.add(new Photo(new InstagramPhotoDataProvider(media.getImages())));
			return photos;
		}

		@Override
		public boolean canFave() {
			return true;
		}

		@Override
		public boolean canComment() {
			return true;
		}

		@Override
		public boolean canRepost() {
			return true;
		}

		@Override
		public boolean isFaved() {
			return media.userHasLiked();
		}

		@Override
		public User getAuthor() {
			return new User(new InstagramUserDataProvider(media.getUser()));
		}

		@Override
		public boolean isRepost() {
			return false;
		}

		@Override
		public Post getRepostedPost() {
			return null;
		}
	}

	private class InstagramCommentAdapter implements Comment.CommentDataProvider {
		private com.getinch.retrogram.model.Comment comment;

		public InstagramCommentAdapter(com.getinch.retrogram.model.Comment comment) {
			this.comment = comment;
		}

		@Override
		public SocialNetworkProviderInterface getProvider() {
			return SocialNetworkProvider.INSTAGRAM;  //To change body of implemented methods use File | Settings | File Templates.
		}

		@Override
		public User getAuthor() {
			return new User(new InstagramUserDataProvider(comment.getFrom()));
		}

		@Override
		public String getId() {
			return Long.toString(comment.getId());
		}

		@Override
		public String getText() {
			return comment.getText();
		}

		@Override
		public long getDate() {
			return comment.getCreatedTime();
		}
	}
}
