package com.click4design.socialfeed.network.api;

import android.os.Bundle;
import android.text.TextUtils;
import com.click4design.socialfeed.data.model.*;
import com.click4design.socialfeed.data.model.User;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.account.TwitterAccount;
import twitter4j.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 22.09.13
 * Time: 2:50
 */
public class TwitterApiClientAdapter implements ApiClientInterface {
    private static final String CONSUMER_KEY = "MPf9Oe5B71KxV5KzaNDacg";
    private static final String CONSUMER_SECRET = "OHt4FwED5WGD1FqijG3iiSdcnFvuKQIXezd0ZxJYRpg";
	private enum TimeLine {
		HOME, FAVORITE, USER
	}

    Twitter twitterClient;

    public TwitterApiClientAdapter(TwitterAccount account) {
        twitterClient = new TwitterFactory().getInstance();
        twitterClient.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
        twitterClient.setOAuthAccessToken(account.getAccessToken());
    }

    @Override
    public Feed<Post> getPhotoFeed() throws TwitterException {
        return null;
    }

    @Override
    public Feed<Post> getPhotoFeed(Bundle params) throws TwitterException {
        return null;
    }

    @Override
    public Feed<Post> getFeed() throws TwitterException {
        return getFeed(new Bundle());
    }

    @Override
    public Feed<Post> getFeed(Bundle params) throws TwitterException {
        return getTimeLine(params, TimeLine.HOME);
    }

	@Override
	public Feed<Post> getFavoritesFeed(Bundle params) throws Exception {
		return getTimeLine(params, TimeLine.FAVORITE);
	}

	@Override
	public void sendPost(Bundle params) throws Exception {
		String text = params.getString("text");
		String photoUri = params.getString("photo_uri");

		StatusUpdate status = new StatusUpdate(text);
		if(!TextUtils.isEmpty(photoUri)) {
			status.setMedia(new File(photoUri));
		}

		twitterClient.updateStatus(status);
	}

	@Override
	public void fave(Post post) throws Exception {
		twitterClient.favorites().createFavorite(Long.parseLong(post.getId()));
	}

	@Override
	public void unFave(Post post) throws Exception {
		twitterClient.favorites().destroyFavorite(Long.parseLong(post.getId()));
	}

	@Override
	public Feed<Post> getMyFeed(Bundle params) throws Exception {
		return getTimeLine(params, TimeLine.USER);
	}

	private Feed<Post> getTimeLine(Bundle params, TimeLine timeLine) throws TwitterException{
		int count = params.getInt("count", -1);
		long maxId = params.getLong("max_id", -1);

		Paging paging = new Paging();

		if(count != -1) {
			paging.setCount(count);
		}

		if(maxId != -1) {
			paging.setMaxId(maxId);
		}

		ResponseList<Status> statusList;
		switch (timeLine) {
			case USER:
				statusList = twitterClient.getUserTimeline(paging);
				break;
			case FAVORITE:
				statusList = twitterClient.getFavorites(paging);
				break;
			default:
				statusList = twitterClient.getHomeTimeline(paging);
		}

		Feed<Post> feed = new Feed<Post>();
		for (Status status: statusList) {
			feed.addItem(new Post(new TwitterStatusDataProvider(status)));
		}

		Pagination pagination = new Pagination();
		if(!feed.isEmpty()) {
			pagination.setParams("max_id", ((statusList.get(statusList.size() - 1).getId()) - 1));
			pagination.setHasNext(true);
		} else {
			pagination.setHasNext(false);
		}

		pagination.setParams("count", count);
		feed.setPagination(pagination);

		return feed;
	}

	@Override
	public Feed<Comment> getComments(Bundle params) throws Exception {
		long postId  = Long.parseLong(params.getString("post_id"));
		String username  = params.getString("username");

		Feed<Comment> feed = new Feed<Comment>();

		Query query = new Query();
		query.sinceId(postId).query("@"+username).count(100);

		QueryResult result = twitterClient.search().search(query);
		List<Status> tweets = result.getTweets();
		for(Status status :tweets) {
			if(status.getInReplyToStatusId() == postId) {
				feed.addItem(new Comment(new TwitterCommentDataProvider(status)));
			}
		}

		if(params.containsKey("sort") && params.getString("sort").equals("desc")) {
			feed.sort();
		}

		Pagination pagination = new Pagination();
		pagination.setHasNext(false);
		feed.setPagination(pagination);

		return feed;
	}

	@Override
	public Comment addComment(Bundle params) throws Exception {
		String text = params.getString("text");
		String username = params.getString("username");
		long postId = Long.parseLong(params.getString("post_id"));

		StatusUpdate update = new StatusUpdate(String.format("@%s %s", username, text));
		update.setInReplyToStatusId(postId);

		Status status = twitterClient.updateStatus(update);

		return new Comment(new TwitterCommentDataProvider(status));
	}

	private class TwitterPhotoDataProvider implements Photo.PhotoDataProvider {
		private MediaEntity mediaEntity;

		public TwitterPhotoDataProvider(MediaEntity mediaEntity) {
			this.mediaEntity = mediaEntity;
		}

		@Override
		public String getId() {
			return Long.toString(mediaEntity.getId());
		}

		@Override
		public String getPreviewUrl() {
			return mediaEntity.getMediaURL();
		}

		@Override
		public String getUrl() {
			return mediaEntity.getMediaURL();
		}
	}

	private class TwitterUserDataProvider implements User.UserDataProvider {
		private twitter4j.User user;

		public TwitterUserDataProvider(twitter4j.User user) {
			this.user = user;
		}

		@Override
		public String getId() {
			return Long.toString(user.getId());
		}

		@Override
		public String getUsername() {
			return user.getScreenName();
		}

		@Override
		public String getFullName() {
			return user.getName();
		}

		@Override
		public String getAvatarUrl() {
			return user.getProfileImageURL();
		}
	}

	private class TwitterStatusDataProvider implements Post.PostDataProvider {
		private Status status;

		public TwitterStatusDataProvider(Status status) {
			this.status = status;
		}

		@Override
		public SocialNetworkProviderInterface getProvider() {
			return SocialNetworkProvider.TWITTER;
		}

		@Override
		public String getId() {
			return Long.toString(status.getId());
		}

		@Override
		public String getText() {
			return status.getText();
		}

		@Override
		public long getDate() {
			return status.getCreatedAt().getTime()/1000;
		}

		@Override
		public int getCommentsCount() {
			return -1;
		}

		@Override
		public int getFavesCount() {
			return status.getFavoriteCount();
		}

		@Override
		public int getRepostsCount() {
			return status.getRetweetCount();
		}

		@Override
		public List<Photo> getPhotos() {
			List<Photo> photos = new ArrayList<Photo>();

			for(MediaEntity mediaEntity : status.getMediaEntities()) {
				if(mediaEntity.getType().equals("photo")) {
					photos.add(new Photo(new TwitterPhotoDataProvider(mediaEntity)));
				}
			}

			return photos;
		}

		@Override
		public boolean canFave() {
			return true;
		}

		@Override
		public boolean canComment() {
			return true;
		}

		@Override
		public boolean canRepost() {
			return true;
		}

		@Override
		public boolean isFaved() {
			return status.isFavorited();
		}

		@Override
		public User getAuthor() {
			return new User(new TwitterUserDataProvider(status.getUser()));
		}

		@Override
		public boolean isRepost() {
			return false;
		}

		@Override
		public Post getRepostedPost() {
			return null;
		}
	}

	private class TwitterCommentDataProvider implements Comment.CommentDataProvider {
		private Status status;

		public TwitterCommentDataProvider(Status status) {
			this.status = status;
		}

		@Override
		public SocialNetworkProviderInterface getProvider() {
			return SocialNetworkProvider.TWITTER;
		}

		@Override
		public User getAuthor() {
			return new User(new TwitterUserDataProvider(status.getUser()));
		}

		@Override
		public String getId() {
			return Long.toString(status.getId());
		}

		@Override
		public String getText() {
			return status.getText();
		}

		@Override
		public long getDate() {
			return status.getCreatedAt().getTime()/1000;
		}
	}
}
