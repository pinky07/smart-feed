package com.click4design.socialfeed.network.error.adapter;

import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.SocialNetworkError;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.error.ErrorType;
import com.click4design.socialfeed.network.error.type.TwitterErrorCode;
import twitter4j.TwitterException;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 21:42
 */
public class TwitterExceptionAdapter extends CommonSocialNetworkException {
    private static final long serialVersionUID = -4002572875032292377L;
	protected final static int EXTERNAL_RATE_LIMIT_ERROR_CODE = 88;
    protected final static int EXTERNAL_UNAUTHORIZED_ERROR_CODE = -1;

    public TwitterExceptionAdapter(TwitterException exception) {
        super(exception);
		String errMessage = exception.getMessage();
        if(errMessage.equals("No authentication challenges found") || errMessage.equals("Received authentication challenge is null")) {
            setCode(ErrorType.UNAUTHORIZED);
        } else if(exception.getErrorCode() == EXTERNAL_RATE_LIMIT_ERROR_CODE) {
			setCode(ErrorType.REQUEST_LIMIT);
		} else if(exception.getMessage().equals("Unable to resolve host \"api.twitter.com\": No address associated with hostname")) {
			setCode(ErrorType.CONNECTION_ERROR);
		} else {
            setCode(ErrorType.UNKNOWN);
        }
        setMessage(exception.getErrorMessage());
        setProvider(SocialNetworkProvider.TWITTER);
    }

    /**
     * Получение кода ошибки по внешнему коду ошибки сервиса
     * @param code
     * @return
     */
    private int getErrorCodeForExternalCode(int code) {
        switch (code) {
            case EXTERNAL_UNAUTHORIZED_ERROR_CODE:
                return ErrorType.UNAUTHORIZED;
            default:
                return ErrorType.UNKNOWN;
        }
    }
}
