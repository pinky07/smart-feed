package com.click4design.socialfeed.network.error.adapter;

import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.SocialNetworkError;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.error.ErrorType;
import com.perm.kate.api.KException;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 21:27
 */
public class VKExceptionAdapter extends CommonSocialNetworkException {
    private static final long serialVersionUID = 2692745264000095116L;

    protected final static int EXTERNAL_UNAUTHORIZED_ERROR_CODE = 5;
    protected final static int EXTERNAL_NEED_CAPTCHA_ERROR_CODE = 999; //todo

    public VKExceptionAdapter(KException exception) {
        super(exception);

        setCode(getErrorCodeForExternalCode(exception.error_code));
        setMessage(exception.getMessage());
        setProvider(SocialNetworkProvider.VK);
    }

    /**
     * Получение кода ошибки по внешнему коду ошибки сервиса
     * @param code
     * @return
     */
    private int getErrorCodeForExternalCode(int code) {
        switch (code) {
            case EXTERNAL_UNAUTHORIZED_ERROR_CODE:
                return ErrorType.UNAUTHORIZED;
            case EXTERNAL_NEED_CAPTCHA_ERROR_CODE:
                return ErrorType.CAPTCHA;
            default:
                return ErrorType.UNKNOWN;
        }
    }
}
