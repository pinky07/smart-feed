package com.click4design.socialfeed.network.error.adapter;

import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.SocialNetworkError;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.error.ErrorType;
import com.click4design.socialfeed.network.error.type.InstagramErrorCode;
import org.json.JSONObject;
import retrofit.RetrofitError;

/**
 * Author: Khamidullin Kamil
 * Date: 09.02.14
 * Time: 14:09
 */
public class InstagramExceptionAdapter extends CommonSocialNetworkException {

    private static final long serialVersionUID = -7636174695874990398L;
    private final static String EXTERNAL_UNAUTHORIZED_ERROR_TYPE = "OAuthParameterException";

    public InstagramExceptionAdapter(RetrofitError exception) {
        super(exception);

        if(exception.isNetworkError()) {
            setCode(ErrorType.CONNECTION_ERROR);
        } else {
            ExternalErrorModel errorModel = (ExternalErrorModel)exception.getBodyAs(ExternalErrorModel.class);
            if(errorModel != null) {
                setCode(getErrorCodeByErrorModel(errorModel));
                setMessage(errorModel.getMeta().getErrorMessage());
            }
        }

        setProvider(SocialNetworkProvider.INSTAGRAM);
    }

    private int getErrorCodeByErrorModel(ExternalErrorModel errorModel) {
        String errorType = errorModel.getMeta().getErrorType();
        if(errorType.equals(EXTERNAL_UNAUTHORIZED_ERROR_TYPE)) {
            return ErrorType.UNAUTHORIZED;
        } else {
            return ErrorType.UNKNOWN;
        }
    }

    protected static class ExternalErrorModel {
        private Meta meta;

        private static class Meta {
            private String error_type;
            private int code;
            private String error_message;

            public String getErrorType() {
                return error_type;
            }

            public void setErrorType(String error_type) {
                this.error_type = error_type;
            }

            public int getCode() {
                return code;
            }

            public void setCode(int code) {
                this.code = code;
            }

            public String getErrorMessage() {
                return error_message;
            }

            public void setErrorMessage(String error_message) {
                this.error_message = error_message;
            }
        }

        public Meta getMeta() {
            return meta;
        }

        public void setMeta(Meta meta) {
            this.meta = meta;
        }
    }
}
