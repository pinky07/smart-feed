package com.click4design.socialfeed.network.error.handler;

import android.R;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.text.TextUtils;
import com.click4design.core.ui.ActivityManager;
import com.click4design.socialfeed.data.CommonRequestManager;
import com.click4design.socialfeed.data.CommonResponseHandler;
import com.click4design.socialfeed.data.exception.CommonSocialNetworkException;
import com.click4design.socialfeed.data.model.Post;
import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.account.Account;
import com.click4design.socialfeed.network.account.AccountStorage;
import com.click4design.socialfeed.network.auth.AuthDialog;
import com.click4design.socialfeed.network.auth.AuthListener;
import com.click4design.socialfeed.network.error.ErrorType;
import com.foxykeep.datadroid.requestmanager.Request;
import com.google.inject.Inject;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.AlertDialog;
import roboguice.RoboGuice;
import roboguice.inject.RoboInjector;

import java.util.Collections;


/**
 * Author: Khamidullin Kamil
 * Date: 09.02.14
 * Time: 22:59
 */
public class ExceptionHandler {
    @Inject
    protected CommonRequestManager requestManager;

    @Inject
    AccountStorage accountStorage;

    public ExceptionHandler(Context context) {
        final RoboInjector injector = RoboGuice.getInjector(context);
        injector.injectMembersWithoutViews(this);
    }

    public Activity getTopActivity() {
        return ActivityManager.getTopActivity();
    }

    public void handleException(CommonSocialNetworkException exception, Request request, CommonResponseHandler handler) {
        SocialNetworkProviderInterface provider = (SocialNetworkProviderInterface)request.getParcelable("provider");
        switch (exception.getCode()) {
            case ErrorType.UNAUTHORIZED:
                showAuthDialog(exception, provider, request, handler);
				break;
			case ErrorType.REQUEST_LIMIT:
				showRequestLimitDialog(exception, provider, request, handler);
				break;
            default:
                showUnknownErrorDialog(exception, provider, request, handler);
				break;
        }
    }

	private void showRequestLimitDialog(final CommonSocialNetworkException exception, final SocialNetworkProviderInterface provider, final Request request, final CommonResponseHandler handler) {
		Activity activity = getTopActivity();
		if(activity != null) {
			new AlertDialog.Builder(activity)
					.setMessage(String.format("Превышен лимит запросов к %s, повторите попытку через 3 минуты", provider.getCode()))
					.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							handler.dispatchError(exception, request);
						}
					})
					.setOnCancelListener(new DialogInterface.OnCancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {
							handler.dispatchError(exception, request);
						}
					})
					.create()
					.show();
		}
	}

    private void showAuthDialog(final CommonSocialNetworkException exception, final SocialNetworkProviderInterface provider, final Request request, final CommonResponseHandler handler) {
        Activity activity = getTopActivity();
        if(activity != null) {
            new AlertDialog.Builder(activity)
                    .setTitle("Ошибка авторизации")
                    .setMessage(String.format("Авторизоватся в %s сейчас?", provider.getCode()))
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AuthDialog authFragment = provider.createAuthFragment();
                            authFragment.show(getTopActivity().getSupportFragmentManager(), "frag", new AuthListener() {
                                @Override
                                public void onAuthenticated(Account account, SocialNetworkProviderInterface provider) {
                                    accountStorage.save(provider, account);
                                    requestManager.execute(request, handler);
                                }
                            });
                        }
                    })
                    .setNegativeButton(R.string.no, null)
                    .create()
                    .show();
        }
    }

    private void showUnknownErrorDialog(final CommonSocialNetworkException exception, final SocialNetworkProviderInterface provider, final Request request, final CommonResponseHandler handler) {
        Activity activity = getTopActivity();
        if(activity != null) {
			String message = exception.getLocalizedMessage();
			if(TextUtils.isEmpty(message)) {
				message = String.format("Ошибка на стороне сервера %s", provider.getCode());
			}

            new AlertDialog.Builder(activity)
                    .setMessage(message)
                    .setPositiveButton("Повторить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestManager.execute(request, handler);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            handler.dispatchError(exception, request);
                        }
                    })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {
							handler.dispatchError(exception, request);
						}
					})
                    .create()
                    .show();
        }
    }
}
