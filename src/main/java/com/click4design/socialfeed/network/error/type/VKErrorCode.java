package com.click4design.socialfeed.network.error.type;

/**
 * Author: Khamidullin Kamil
 * Date: 08.02.14
 * Time: 19:50
 */
public class VKErrorCode {
    public static final int UNAUTHORIZED = 5;
    public static final int CAPTCHA_ERROR = 2;
}

