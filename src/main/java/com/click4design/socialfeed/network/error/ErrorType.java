package com.click4design.socialfeed.network.error;

/**
 * Author: Khamidullin Kamil
 * Date: 09.02.14
 * Time: 21:38
 */
public class ErrorType {
    public static final int UNAUTHORIZED = 1;
    public static final int CAPTCHA = 2;
    public static final int UNKNOWN = 3;
    public static final int CONNECTION_ERROR = 4;
	public static final int REQUEST_LIMIT = 5;
}
