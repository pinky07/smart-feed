package com.click4design.socialfeed.network;

import android.os.Parcel;
import android.os.Parcelable;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.network.account.*;
import com.click4design.socialfeed.network.api.ApiClientInterface;
import com.click4design.socialfeed.network.auth.*;
import com.click4design.socialfeed.network.dependency.ImplDependency;


/**
 * Author: Khamidullin Kamil
 * Date: 30.06.13
 * Time: 0:20
 */
public enum SocialNetworkProvider implements SocialNetworkProviderInterface {

    TWITTER(
            TwitterAccount.class,
            TwitterAuthDialogFragment.class,
            "twitter",
            R.drawable.tw,
            false
    ),

    INSTAGRAM(
            InstagramAccount.class,
            InstagramAuthDialogFragment.class,
            "instagram",
            R.drawable.ig,
            true
    ),

    FACEBOOK(
            FacebookAccount.class,
            FacebookAuthDialogFragment.class,
            "facebook",
            R.drawable.fb,
            true
    ),

    VK(
            VKAccount.class,
            VKAuthDialogFragment.class,
            "vk",
            R.drawable.vk,
            true
    );

    protected boolean acceptPhoto;
    protected String code;
    protected int iconRes;
    protected Class<? extends Account> accountClass;
    protected Class<? extends AuthDialog> authFragmentClass;

    SocialNetworkProvider(Class<? extends Account> cls, Class<? extends AuthDialog> authFragment, String code, int iconRes, boolean acceptPhoto) {
        this.code               = code;
        this.iconRes            = iconRes;
        this.accountClass       = cls;
        this.authFragmentClass  = authFragment;
        this.acceptPhoto        = acceptPhoto;
    }

    public String getCode() {
        return this.code;
    }

    @Override
    public int getIconRes() {
        return this.iconRes;
    }

    @Override
    public boolean isAcceptPhoto() {
        return this.acceptPhoto;
    }


    public Class<? extends Account> getAccountClass() {
        return this.accountClass;
    }

    @Override
    public AuthDialog createAuthFragment() {
        return AuthDialog.newInstance(authFragmentClass, this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(ordinal());
    }

    public static final Parcelable.Creator<SocialNetworkProvider> CREATOR
            = new Parcelable.Creator<SocialNetworkProvider>() {
        public SocialNetworkProvider createFromParcel(Parcel in) {
            return SocialNetworkProvider.values()[in.readInt()];
        }

        public SocialNetworkProvider[] newArray(int size) {
            return new SocialNetworkProvider[size];
        }
    };
}
