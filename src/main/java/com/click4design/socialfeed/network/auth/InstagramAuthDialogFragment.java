package com.click4design.socialfeed.network.auth;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.*;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.click4design.socialfeed.common.Utils;
import com.click4design.socialfeed.network.account.Account;
import com.click4design.socialfeed.network.account.InstagramAccount;
import com.neovisionaries.android.twitter.TwitterOAuthView;
import com.wareninja.android.opensource.oauth2login.common.DialogError;

/**
 * Author: Khamidullin Kamil
 * Date: 16.11.13
 * Time: 18:51
 */
public class InstagramAuthDialogFragment extends AuthDialog {

    public static final String REDIRECT_URI = "https://click4design.ru/authenticated";
    public static final String AUTH_URI = "https://instagram.com/oauth/authorize/?client_id=%s&redirect_uri=%s&response_type=token%s";
    public static final String INSTAGRAM_URI = "https://instagram.com";
    public static final String INSTAGRAM_RESET_PASSWORD = "https://instagram.com";
    private final String mClientId ="06277e9c8a1042ccbdbcd2a7847c47ab";
    private final String mPermissions = "&scope=likes+comments";
    //WebView mView;

    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout ll = new LinearLayout(getActivity());
        ll.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mView =  new WebView(getActivity());
        ll.addView(mView);

        return ll;
    }*/

    /*@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAuthWebView.setVerticalScrollBarEnabled(false);
        mAuthWebView.setHorizontalScrollBarEnabled(false);
        mAuthWebView.setWebViewClient(new IgWebViewClient());
        mAuthWebView.getSettings().setJavaScriptEnabled(true);
        mAuthWebView.getSettings().setSavePassword(false);
        final String url = String.format(AUTH_URI, mClientId, REDIRECT_URI,
                mPermissions);
        mAuthWebView.loadUrl(url);
    } */

    @Override
    protected String getAuthUrl() {
        return String.format(AUTH_URI, mClientId, REDIRECT_URI,
                mPermissions);
    }

    @Override
    protected String getRedirectUrl() {
        return REDIRECT_URI;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected Account handleUrl(String url) throws AuthException {
        final Bundle values = Utils.parseUrl(url);
        if (values.containsKey("error")) {
            throw new AuthException(values.getString("error"));
        } else {
            InstagramAccount account = new InstagramAccount();
            account.setToken(values.getString("access_token"));
            return account;
        }
    }

    /*private class IgWebViewClient extends WebViewClient {

        private static final String INSTAGRAM_WEB_VIEW = "Instagram-WebView";
        private boolean mFinished;

        @Override
        public boolean shouldOverrideUrlLoading(final WebView view,
                                                final String url) {
            boolean returnValue = true;
            if (url.startsWith(REDIRECT_URI)) {
                // We got out token !
                final Bundle values = Utils.parseUrl(url);
                returnValue = false;
                if (values.containsKey("error")) {
                    //todo show error
                } else {
                    InstagramAccount account = new InstagramAccount();
                    account.setToken(values.getString("access_token"));
                    dispatchAuthListener(account);
                }
                mFinished = true;

            } else {
                returnValue = super.shouldOverrideUrlLoading(view, url);
            }
            return returnValue;
        }

        @Override
        public void onReceivedError(final WebView view, final int errorCode,
                                    final String description, final String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            //mListener.onError(new DialogError(description, errorCode,
                    //failingUrl));
            //InstagramAuthDialog.this.dismiss();
        }*/

       // @Override
        //public void onPageStarted(final WebView view, final String url,
        //                          final Bitmap favicon) {
            /*Util.logd(InstagramAuthDialog.this.getContext(),
                    INSTAGRAM_WEB_VIEW, "Webview loading URL: " + url);
            super.onPageStarted(view, url, favicon);
            if (!mFinished) {
                mSpinner.show();
            } */

        //}

        //@Override
        //public void onPageFinished(final WebView view, final String url) {
         //   super.onPageFinished(view, url);
        //    mProgress.setVisibility(View.GONE);
            /*try {
                // In some cases we dismiss the originating parent activity if
                // so
                // then don't dismiss this dialog otherwise you'll get an error.
                Util.logd(InstagramAuthDialog.this.getContext(),
                        INSTAGRAM_WEB_VIEW, "Webview finished URL: " + url);
                if (isShowing()) {
                    mSpinner.dismiss();
                }
                mWebView.setVisibility(View.VISIBLE);
                mCrossImage.setVisibility(View.VISIBLE);
            } catch (final Exception error) {
                // Ignore any error, just print it.
                Log.e(INSTAGRAM_WEB_VIEW, error.getMessage());
            } */
        //}
    //}
}
