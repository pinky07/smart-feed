package com.click4design.socialfeed.network.auth;

import java.io.IOException;

/**
 * Author: Khamidullin Kamil
 * Date: 04.01.14
 * Time: 3:29
 */
public class AuthException extends IOException {
    public AuthException(String detailMessage) {
        super(detailMessage);
    }
}
