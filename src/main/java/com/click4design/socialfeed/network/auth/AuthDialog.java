package com.click4design.socialfeed.network.auth;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.click4design.roboholo.RoboHoloDialogFragment;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.account.Account;
import org.holoeverywhere.LayoutInflater;
import roboguice.inject.InjectView;

/**
 * Author: Khamidullin Kamil
 * Date: 30.06.13
 * Time: 15:55
 */
abstract public class AuthDialog extends RoboHoloDialogFragment {
    protected AuthListener mAuthListener;
    protected SocialNetworkProviderInterface mProvider;
    protected final static String EXTRA_PROVIDER = "provider";

    @InjectView(R.id.auth_view)
    WebView mAuthWebView;

    @InjectView(R.id.progress_bar)
    ProgressBar mProgress;

    public static AuthDialog newInstance(Class<? extends AuthDialog> authFragment, SocialNetworkProviderInterface provider) {
        try {
            AuthDialog fragment = authFragment.newInstance();
            Bundle args         = new Bundle();
            args.putSerializable(EXTRA_PROVIDER, provider);
            fragment.setArguments(args);

            return fragment;
        } catch (Exception ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_auth, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        mAuthWebView.setVerticalScrollBarEnabled(false);
        mAuthWebView.setHorizontalScrollBarEnabled(false);
        mAuthWebView.setWebViewClient(new AuthWebViewClient());
        mAuthWebView.getSettings().setJavaScriptEnabled(true);
        mAuthWebView.getSettings().setSavePassword(false);
        mAuthWebView.loadUrl(getAuthUrl());

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProvider = (SocialNetworkProviderInterface)getArguments().getSerializable(EXTRA_PROVIDER);
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
    }

    abstract protected String getAuthUrl();

    abstract protected String getRedirectUrl();

    abstract protected Account handleUrl(String url) throws AuthException;

    final public void setAuthListener(AuthListener listener) {
        mAuthListener = listener;
    }

    final public void show(FragmentManager manager, String tag, AuthListener listener) {
        mAuthListener = listener;
        super.show(manager, tag);
    }

    protected void dispatchAuthListener(Account account) {
        if(mAuthListener != null)
            mAuthListener.onAuthenticated(account, mProvider);

        mAuthWebView.destroy();
        dismiss();
    }

    class AuthWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (url.startsWith(getRedirectUrl())) {
                try {
                    dispatchAuthListener(handleUrl(url));
                } catch (AuthException aex) {
                    //pass
                }
            } else {
                super.onPageStarted(view, url, favicon);
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            showAuthWebView();
        }
    }

    protected void showAuthWebView() {
        mProgress.setVisibility(View.GONE);
        mAuthWebView.setVisibility(View.VISIBLE);
        mAuthWebView.requestFocus(View.FOCUS_DOWN);
    }
}
