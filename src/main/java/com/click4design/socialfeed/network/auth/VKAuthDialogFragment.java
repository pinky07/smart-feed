package com.click4design.socialfeed.network.auth;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import com.click4design.socialfeed.network.account.Account;
import com.click4design.socialfeed.network.account.VKAccount;
import com.perm.kate.api.Auth;

/**
 * Author: Khamidullin Kamil
 * Date: 30.06.13
 * Time: 16:51
 */
public class VKAuthDialogFragment extends AuthDialog {
    private static final String TAG = "Kate.LoginActivity";
    private static final String API_ID = "3890987";
    //WebView mView;

    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout ll = new LinearLayout(getActivity());
        ll.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mView =  new WebView(getActivity());
        ll.addView(mView);

        return ll;
    } */

    /*@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CookieSyncManager.createInstance(getActivity());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        String url=Auth.getUrl(API_ID, Auth.getSettings());
        mAuthWebView.getSettings().setJavaScriptEnabled(true);
        mAuthWebView.clearCache(true);
        mAuthWebView.setWebViewClient(new VkontakteWebViewClient());
        mAuthWebView.loadUrl(url);
    }

    class VkontakteWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            parseUrl(url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            mProgress.setVisibility(View.GONE);
        }
    }

    private void parseUrl(String url) {
        try {
            if(url==null)
                return;
            Log.i(TAG, "url=" + url);
            if(url.startsWith(Auth.redirect_url))
            {
                if(!url.contains("error=")){
                    String[] auth= Auth.parseRedirectUrl(url);
                    VKAccount account = new VKAccount();
                    account.setToken(auth[0]);
                    account.setUserId(auth[1]);

                    dispatchAuthListener(account);
                }
                //finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    } */

    @Override
    protected String getAuthUrl() {
        return Auth.getUrl(API_ID, Auth.getSettings());
    }

    @Override
    protected String getRedirectUrl() {
        return Auth.redirect_url;
    }

    @Override
    protected Account handleUrl(String url) throws AuthException {
        if(!url.contains("error=")){
            try {
                String[] auth= Auth.parseRedirectUrl(url);
                VKAccount account = new VKAccount();
                account.setToken(auth[0]);
                account.setUserId(auth[1]);

                return account;
            } catch (Exception ex) {
                throw new AuthException(ex.getMessage());
            }

        } else {
            throw new AuthException("Ошибка");
        }
    }
}
