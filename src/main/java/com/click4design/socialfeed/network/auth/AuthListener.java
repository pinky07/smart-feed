package com.click4design.socialfeed.network.auth;

import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.account.Account;

/**
 * Author: Khamidullin Kamil
 * Date: 30.06.13
 * Time: 16:19
 */
public interface AuthListener {
    public void onAuthenticated(Account account, SocialNetworkProviderInterface provider);
}
