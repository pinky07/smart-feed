package com.click4design.socialfeed.network.auth;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.click4design.socialfeed.R;
import com.click4design.socialfeed.network.account.Account;
import com.click4design.socialfeed.network.account.TwitterAccount;
import com.neovisionaries.android.twitter.TwitterOAuthView;
import roboguice.RoboGuice;
import twitter4j.auth.AccessToken;
import android.widget.LinearLayout.LayoutParams;

/**
 * Author: Khamidullin Kamil
 * Date: 30.06.13
 * Time: 16:07
 */
public class TwitterAuthDialogFragment extends AuthDialog implements TwitterOAuthView.Listener {
    private boolean oauthStarted = false;

    private static final String CONSUMER_KEY = "MPf9Oe5B71KxV5KzaNDacg";
    private static final String CONSUMER_SECRET = "OHt4FwED5WGD1FqijG3iiSdcnFvuKQIXezd0ZxJYRpg";
    private static final String CALLBACK_URL = "http://click4design.ru";
    private static final boolean DUMMY_CALLBACK_URL = true;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewGroup parent = (ViewGroup) view;
        //View old = view.findViewById(R.id.auth_view);
        int index = parent.indexOfChild(mAuthWebView);
        parent.removeView(mAuthWebView);
        mAuthWebView = new TwitterOAuthView(getActivity());
        mAuthWebView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mAuthWebView.setId(R.id.auth_view);
        mAuthWebView.setVisibility(View.GONE);
        parent.addView(mAuthWebView, index);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!oauthStarted) {
            oauthStarted = true;
            ((TwitterOAuthView)mAuthWebView).start(CONSUMER_KEY, CONSUMER_SECRET, CALLBACK_URL, DUMMY_CALLBACK_URL, this);
        }
    }

    @Override
    public void onSuccess(TwitterOAuthView view, AccessToken accessToken) {
        TwitterAccount account = new TwitterAccount();
        account.setToken(accessToken.getToken());
        account.setTokenSecret(accessToken.getTokenSecret());
        dispatchAuthListener(account);
    }

    @Override
    public void onFailure(TwitterOAuthView view, TwitterOAuthView.Result result) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onPageLoaded() {
        showAuthWebView();
    }

    @Override
    protected String getAuthUrl() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected String getRedirectUrl() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected Account handleUrl(String url) throws AuthException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
