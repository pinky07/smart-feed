package com.click4design.socialfeed.network.dependency;

import com.click4design.socialfeed.network.api.ApiClientInterface;
import com.click4design.socialfeed.network.auth.AuthDialog;
import com.click4design.socialfeed.network.resource.Resources;

/**
 * Author: Khamidullin Kamil
 * Date: 20.02.14
 * Time: 22:52
 */
public interface ImplDependency {
	public Class<? extends ApiClientInterface> client();
	public Class<? extends Resources> assets();
	public Class<? extends AuthDialog> authDialog();
}
