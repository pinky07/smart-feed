package com.click4design.socialfeed.network;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 19.02.14
 * Time: 22:43
 */
public interface SocialNetworksManager {
	public List<SocialNetworkProviderInterface> getSocialNetworks();

	public List<SocialNetworkProviderInterface> getAuthorizedSocialNetworks();

	public List<SocialNetworkProviderInterface> getNotAuthorizedSocialNetworks();
}
