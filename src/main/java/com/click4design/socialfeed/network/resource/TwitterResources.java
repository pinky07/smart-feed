package com.click4design.socialfeed.network.resource;

import com.click4design.socialfeed.R;

/**
 * Author: Khamidullin Kamil
 * Date: 20.02.14
 * Time: 23:20
 */
public class TwitterResources implements Resources {
	@Override
	public int getFaveIcon() {
		return R.drawable.ic_rating_important;
	}

	@Override
	public int getFavedIcon() {
		return R.drawable.ic_faved;
	}

	@Override
	public int getCommentIcon() {
		return R.drawable.ic_comment;
	}

	@Override
	public int getRepostIcon() {
		return R.drawable.ic_retweet;
	}

	@Override
	public int getSocialNetworkIcon() {
		return R.drawable.tw;
	}

	@Override
	public int getSocialNetworkTitle() {
		return 0;
	}
}
