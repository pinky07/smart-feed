package com.click4design.socialfeed.network.resource;

/**
 * Author: Khamidullin Kamil
 * Date: 20.02.14
 * Time: 22:33
 */
public interface Resources {
	public int getFaveIcon();

	public int getFavedIcon();

	public int getCommentIcon();

	public int getRepostIcon();

	public int getSocialNetworkIcon();

	public int getSocialNetworkTitle();
}
