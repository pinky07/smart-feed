package com.click4design.socialfeed.network.resource;

import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;
import com.click4design.socialfeed.network.account.InstagramAccount;
import com.click4design.socialfeed.network.account.TwitterAccount;
import com.click4design.socialfeed.network.account.VKAccount;
import com.click4design.socialfeed.network.api.ApiClientInterface;
import com.click4design.socialfeed.network.api.InstagramClientAdapter;
import com.click4design.socialfeed.network.api.TwitterApiClientAdapter;
import com.click4design.socialfeed.network.api.VKApiClientAdapter;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: Khamidullin Kamil
 * Date: 20.02.14
 * Time: 22:41
 */
public class DefaultResourcesManager implements ResourcesManager{
	protected Map<SocialNetworkProviderInterface, Resources> resources = new HashMap<SocialNetworkProviderInterface, Resources>();

	@Override
	public Resources provide(SocialNetworkProviderInterface provider) {
		if(!resources.containsKey(provider)) {
			Resources client = create(provider);
			if(client != null) {
				resources.put(provider, create(provider));
			} else {
				return null;
			}
		}

		return resources.get(provider);
	}

	private Resources create(SocialNetworkProviderInterface provider) {
		switch ((SocialNetworkProvider)provider) {
			case TWITTER:
				return new TwitterResources();
			case VK:
				return new VKResources();
			case INSTAGRAM:
				return new InstagramResources();
			default:
				return null;
		}
	}
}
