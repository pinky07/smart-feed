package com.click4design.socialfeed.network.resource;

import com.click4design.socialfeed.network.SocialNetworkProvider;
import com.click4design.socialfeed.network.SocialNetworkProviderInterface;

/**
 * Author: Khamidullin Kamil
 * Date: 20.02.14
 * Time: 22:34
 */
public interface ResourcesManager {
	public Resources provide(SocialNetworkProviderInterface provider);
}
