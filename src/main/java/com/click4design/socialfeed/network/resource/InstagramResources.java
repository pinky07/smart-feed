package com.click4design.socialfeed.network.resource;

import com.click4design.socialfeed.R;

/**
 * Author: Khamidullin Kamil
 * Date: 20.02.14
 * Time: 23:20
 */
public class InstagramResources implements Resources {
	@Override
	public int getFaveIcon() {
		return R.drawable.ic_like;
	}

	@Override
	public int getFavedIcon() {
		return R.drawable.ic_liked;
	}

	@Override
	public int getCommentIcon() {
		return R.drawable.ic_comment;
	}

	@Override
	public int getRepostIcon() {
		return R.drawable.ic_retweet;
	}

	@Override
	public int getSocialNetworkIcon() {
		return R.drawable.ig;
	}

	@Override
	public int getSocialNetworkTitle() {
		return 0;  //To change body of implemented methods use File | Settings | File Templates.
	}
}
