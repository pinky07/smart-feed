package com.click4design.socialfeed.network;

import android.os.Parcelable;
import com.click4design.socialfeed.network.account.Account;
import com.click4design.socialfeed.network.auth.AuthDialog;
import com.click4design.socialfeed.network.dependency.ImplDependency;

import java.io.Serializable;

/**
 * Author: Khamidullin Kamil
 * Date: 30.06.13
 * Time: 19:32
 */
public interface SocialNetworkProviderInterface extends Serializable, Parcelable {

    public String getCode();

    public int getIconRes();

    public boolean isAcceptPhoto();

    public Class<? extends Account> getAccountClass();

    abstract public AuthDialog createAuthFragment();
}
