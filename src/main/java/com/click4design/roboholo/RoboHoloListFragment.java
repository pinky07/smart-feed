package com.click4design.roboholo;

import android.os.Bundle;
import android.view.View;
import org.holoeverywhere.app.ListFragment;
import roboguice.RoboGuice;

/**
 * Author: Khamidullin Kamil
 * Date: 11.01.14
 * Time: 13:46
 */
public class RoboHoloListFragment extends ListFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectMembersWithoutViews(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectViewMembers(this);
    }
}
