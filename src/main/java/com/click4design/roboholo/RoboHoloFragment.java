package com.click4design.roboholo;

import android.os.Bundle;
import android.view.View;
import org.holoeverywhere.app.Fragment;
import roboguice.RoboGuice;

/**
 * Author: Khamidullin Kamil
 * Date: 11.01.14
 * Time: 13:48
 */
public abstract class RoboHoloFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectMembersWithoutViews(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RoboGuice.getInjector(getActivity()).injectViewMembers(this);
    }
}
