package com.perm.kate.api;

import java.util.ArrayList;
import java.util.List;

public class CommentList {
    public int count;
    public ArrayList<Comment> comments=new ArrayList<Comment>();

	public Long[] getProfilesIds() {
		List<Long> authorIds = new ArrayList<Long>();
		for(Comment comment : comments) {
			if(comment.from_id > 0) {
				authorIds.add(comment.from_id);
			}

		}

		return authorIds.toArray(new Long[authorIds.size()]);
	}

	public Long[] getGroupsIds() {
		List<Long> authorIds = new ArrayList<Long>();
		for(Comment comment : comments) {
			if(comment.from_id < 0) {
				authorIds.add(comment.from_id*(-1));
			}
		}

		return authorIds.toArray(new Long[authorIds.size()]);
	}
}
