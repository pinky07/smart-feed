package com.perm.kate.api;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Author: Khamidullin Kamil
 * Date: 22.02.14
 * Time: 9:28
 */
public class WallFeed {
	public ArrayList<WallMessage> items=new ArrayList<WallMessage>();
	public ArrayList<User> profiles;
	public ArrayList<Group> groups;
	public int count = 0;

	public static WallFeed parse(JSONObject root) throws JSONException {
		JSONObject response = root.getJSONObject("response");
		JSONArray array = response.optJSONArray("wall");
		JSONArray profiles_array = response.optJSONArray("profiles");
		JSONArray groups_array = response.optJSONArray("groups");
		WallFeed wallFeed = new WallFeed();
		if (array != null) {
			int category_count = array.length();
			wallFeed.count = array.getInt(0);
			for(int i = 1; i < category_count; ++i) {
				JSONObject o = (JSONObject)array.get(i);
				WallMessage wm = WallMessage.parse(o);
				wallFeed.items.add(wm);
			}
		}

		if (profiles_array != null) {
			wallFeed.profiles = new ArrayList<User>();
			for(int i = 0; i < profiles_array.length(); i++) {
				JSONObject jprofile = (JSONObject)profiles_array.get(i);
				User m = User.parseFromNews(jprofile);
				wallFeed.profiles.add(m);
			}
		}
		if (groups_array != null)
			wallFeed.groups = Group.parseGroups(groups_array);


		return wallFeed;
	}
}
