package com.getinch.retrogram.model;

import java.util.List;

public class Comments {

    private Integer count;
    private List<Comment> data;
	private Pagination pagination;

    public int getCount() {
        return count;
    }

    public List<Comment> getComments() {
        return data;
    }

}
